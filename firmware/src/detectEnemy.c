#include "detectEnemy.h"
#include "locationFunction/solveLocation.h"
#include "LIDARSensors.h"
#include <stdbool.h>
#include <stdio.h>

#define MINIMUM_CONFIDENCE_VALUE 50
#define ENEMY_DETECTION_LENGTH 50
int samples[ENEMY_DETECTION_LENGTH];
int counterIndex=0;
int sensorDetected[9]={0,0,0,0,0,0,0,0,0};

double x,y;


void initEnemySolver(void)
{
    /* Initialize the application.
    You do not need to do this more than one time. */
    enemyDetection_initialize();
}

double getXPos(void)
{
    return x;
}

double getYPos(void)
{
    return y;
}

int detectEnemy(void)
{    
    double locationSet[3];
    double enemyDetectionInputs[10];

    //Solve Location of Robot
    int i=0;
    solveLocation(getSensorDistancesArrayCenterOfRobot(), (double*)locationSet);

    //Construct enemy detection
    for(i=0;i<8;i++)
    {
        enemyDetectionInputs[i]=getSensorDistanceCenterOfRobot(i);
    }                
    x=locationSet[1] * (cos(locationSet[0]*(3.14159/180)));
    y=locationSet[1] * (sin(locationSet[0]*(3.14159/180)));


//    printf("X: %f, Y: %f\r\n", x,y);
//    printf("Heading: %f\r\n",locationSet[2]);
    enemyDetectionInputs[8]=x;
    enemyDetectionInputs[9]=y;
    //Store this sample of enemy location
    samples[counterIndex] = locateEnemy((double*)enemyDetectionInputs)-1;
    
    //Move pointer to next location
    counterIndex++;
    if(counterIndex>=ENEMY_DETECTION_LENGTH)
    {
        counterIndex=0;
    }
    
    //Wipe the sum of the sensor counters
    for(i=0;i<9;i++)
    {
        sensorDetected[i]=0;
    }
    
    //Sum up the samples per sensor
    for(i=0;i<ENEMY_DETECTION_LENGTH;i++)
    {
        sensorDetected[samples[i]+1]++;               
    }
    int maxConfidence=0;
    int locationEnemy=0;
    //Find the most confident sensor that enemy is present on
    for(i=0;i<9;i++)
    {
        if(sensorDetected[i] > maxConfidence)
        {
            maxConfidence = sensorDetected[i];
            locationEnemy = i;
        }
    }
    return locationEnemy-1;    
}

int locateEnemy(double * inputs)
{
    int i=0;
    double maxConf = MINIMUM_CONFIDENCE_VALUE;
    int locationEnemy=0;
    double enemyHere[9];
    //printf("B4: %d,", millis());
    enemyDetection(inputs,(double*)enemyHere);
    for(i=0;i<9;i++)
    {
        if(enemyHere[i] > maxConf)
        {
            maxConf = enemyHere[i];
            locationEnemy=i;
        }
        //printf("index %d: %f\r\n",i,enemyHere[i]);
    }
    //printf("\r\n");
    //printf("After: %d \n",millis());
    return locationEnemy;
}

void closeEnemySolver(void)
{
    /* Terminate the application.
    You do not need to do this more than one time. */
    enemyDetection_terminate();
}