#include "AutonomousMode.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"
#include "runBackAndForthFindEnemyBoth.h"
#include "spinWeaponUp.h"
#include "enemyAssumedToBeInFront.h"
#include "runWalls.h"
#include "detectEnemy.h"
#include "doTurn.h"

#define WEAPON_SPEED_SETTING 20

#define ATTACK_SPEED 40
#define ATTACK_SPEED_H 40
#define ATTACK_SPEED_L 25

#define MAX_SPEED_ATTACK 50
#define MIN_SPEED_ATTACK -50

#define CLOCKWISE true
#define COUNTERCLOCKWISE false

#define DEBUG

timers_t detectEnemyTimer;
timers_t enemyTimeout;
timers_t detectCentralEnemyTimer;
timers_t checkDetectTimer;
timers_t attackCycleTimeout;
timers_t macroLightFoundYouTimer;
timers_t macroLightAttackTimer;
timers_t debugTimer;
timers_t holdEnemyMemoryTimer;

bool cw_ccw=CLOCKWISE;
bool blinkLight=true;
int enemySensor=-1;
int numberOfShimmies=0;
bool oneTimeTimerReset=true;
bool timeoutActive=false;
uint8_t attackSpeed = ATTACK_SPEED_H;

int boundMotorCommandsAttack(int val);

void setupAutonomous(void)
{
    setTimerInterval(&detectCentralEnemyTimer,2000);
    setTimerInterval(&detectEnemyTimer,10);
    setTimerInterval(&enemyTimeout,2000);
    setTimerInterval(&checkDetectTimer,500);
    setTimerInterval(&attackCycleTimeout,5000);   
    setTimerInterval(&macroLightFoundYouTimer,100);
    setTimerInterval(&macroLightAttackTimer,50);
    setTimerInterval(&debugTimer,100);
    setTimerInterval(&holdEnemyMemoryTimer,500);
}

void runAutonomous(void)
{
    spinWeaponUp(WEAPON_SPEED_SETTING);
   
    while(checkMacroExists())
    {
        MACRO_LIGHT=1;
        runWallsFindEnemy(WEAPON_SPEED_SETTING);
        
        bool enemyFound=false;
        resetTimer(&attackCycleTimeout);
        while(!timerDone(&attackCycleTimeout) && checkMacroExists())
        {
            if(timerDone(&macroLightFoundYouTimer))
            {
                MACRO_LIGHT=blinkLight;
                blinkLight=!blinkLight;
            }

            resetTimer(&enemyTimeout);
            numberOfShimmies=0;
            
            while(!timerDone(&enemyTimeout) && checkMacroExists())
            {
                if(timerDone(&macroLightFoundYouTimer))
                {
                    MACRO_LIGHT=blinkLight;
                    blinkLight=!blinkLight;
                }

                updateCommunications();
                updateRCInputs();            
                //Update the gyroscope values
                updateIMU();

                
                uint8_t speed = (int)((getChannel6()+255)/5.0);
                if(speed>WEAPON_SPEED_SETTING)
                {
                    if(speed!=getWeaponMotorSpeed())
                        setWeaponMotorSpeed(speed);
                }
                else
                {
                    if(WEAPON_SPEED_SETTING!=getWeaponMotorSpeed())
                        setWeaponMotorSpeed(WEAPON_SPEED_SETTING);
                }
#ifdef DEBUG   
                if(timerDone(&debugTimer))
                {
                    printf("Weapon Speed: %d\r\n", getWeaponMotorSpeed());
                }
#endif
                
                enemySensor=getEnemyLocation();
                if(timerDone(&checkDetectTimer))
                {
                    if(enemySensor==0)
                    {
                        enemyFound=true;
                        break;
                    }
                    else
                    {
                        if(cw_ccw == CLOCKWISE)
                        {
                            doTurn(15+numberOfShimmies);
                            cw_ccw=COUNTERCLOCKWISE;
                        }
                        else
                        {
                            doTurn(-15-numberOfShimmies);
                            cw_ccw=CLOCKWISE;
                            numberOfShimmies+=2;
                        }
                    }
                    resetTimer(&checkDetectTimer);
                }
            }

            //ATTACK
            resetTimer(&holdEnemyMemoryTimer);
            cw_ccw=!cw_ccw;
            
            setGyroRelativeAngle();
            if(enemyFound)
            {
                while((enemySensor==0 || !timerDone(&holdEnemyMemoryTimer)) && checkMacroExists())        
                {
                    if(timerDone(&macroLightAttackTimer))
                    {
                        MACRO_LIGHT=blinkLight;
                        blinkLight=!blinkLight;
                    }
                    updateCommunications();
                    updateRCInputs();            
                    //Update the gyroscope values
                    updateIMU();
                    
                    uint8_t speed = (int)((getChannel6()+255)/5.0);
                    if(speed>WEAPON_SPEED_SETTING)
                    {
                        if(speed!=getWeaponMotorSpeed())
                            setWeaponMotorSpeed(speed);
                    }
                    else
                    {
                        if(WEAPON_SPEED_SETTING!=getWeaponMotorSpeed())
                            setWeaponMotorSpeed(WEAPON_SPEED_SETTING);
                    }
                    
#ifdef DEBUG   
                if(timerDone(&debugTimer))
                {
                    printf("Weapon Speed: %d\r\n", getWeaponMotorSpeed());
                }
#endif
                    
                    enemySensor=getEnemyLocation();

                    int _lm =  ATTACK_SPEED;
                    int _rm =  ATTACK_SPEED;

                    motorGyroFeedbackStraight(&_lm, &_rm);
                    //Send to the motor system to be sent by the communications library
                    setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsAttack(_lm)));
                    setRightMotorSpeed(boundMotorCommands(boundMotorCommandsAttack(_rm)));
                }
                enemyFound=false;
            }
        }
    }
}


void runAutonomous2(void)
{
    spinWeaponUp(WEAPON_SPEED_SETTING);
   
     while(checkMacroExists())
     {
         MACRO_LIGHT=1;
        runWallsFindEnemy(WEAPON_SPEED_SETTING);
        
        bool enemyFound=false;
        
        resetTimer(&detectCentralEnemyTimer);
        while(!timerDone(&detectCentralEnemyTimer) && checkMacroExists())
        {
            if(timerDone(&macroLightFoundYouTimer))
            {
                MACRO_LIGHT=blinkLight;
                blinkLight=!blinkLight;
            }

            resetTimer(&enemyTimeout);
            numberOfShimmies=0;
            
            while(!timerDone(&enemyTimeout) && checkMacroExists())
            {
                if(timerDone(&macroLightFoundYouTimer))
                {
                    MACRO_LIGHT=blinkLight;
                    blinkLight=!blinkLight;
                }

                updateCommunications();
                updateRCInputs();            
                //Update the gyroscope values
                updateIMU();
                
//                if(timerDone(&debugTimer))
//                {
//                    printf("Looking, see: %d\r\n", enemySensor);
//                }
                 uint8_t speed = (int)((getChannel6()+255)/5.0);
                if(speed>WEAPON_SPEED_SETTING)
                {
                    setWeaponMotorSpeed(speed);
                }
                else
                {
                    setWeaponMotorSpeed(WEAPON_SPEED_SETTING);
                }
                
                enemySensor=getEnemyLocation();
                if(timerDone(&checkDetectTimer))
                {
                    if(enemySensor==0)
                    {
                        enemyFound=true;
                        break;
                    }
                    else
                    {
                        if(cw_ccw == CLOCKWISE)
                        {
                            doTurn(15+numberOfShimmies);
                            cw_ccw=COUNTERCLOCKWISE;
                        }
                        else
                        {
                            doTurn(-15-numberOfShimmies);
                            cw_ccw=CLOCKWISE;
                            numberOfShimmies+=2;
                        }
                    }
                    resetTimer(&checkDetectTimer);
                }
            }
            //ATTACK
            cw_ccw=!cw_ccw;
        }
        if(enemyFound)
        {
            attackSpeed = ATTACK_SPEED_H;
            timeoutActive=false;
            oneTimeTimerReset=true;
            
            setGyroRelativeAngle();
            resetTimer(&attackCycleTimeout);
            while((enemyFound || !timerDone(&attackCycleTimeout)) && checkMacroExists())        
            {

                if(timerDone(&macroLightAttackTimer))
                {
                    MACRO_LIGHT=blinkLight;
                    blinkLight=!blinkLight;
                }
                updateCommunications();
                updateRCInputs();            

                //Update the gyroscope values
                updateIMU();

                uint8_t speed = (int)((getChannel6()+255)/5.0);
                if(speed>WEAPON_SPEED_SETTING)
                {
                    setWeaponMotorSpeed(speed);
                }
                else
                {
                    setWeaponMotorSpeed(WEAPON_SPEED_SETTING);
                }
                
                enemySensor=getEnemyLocation();
                if(enemySensor!=0)
                {
                    if(enemySensor==-1)
                    {
                        //enemyFound=false;
                        attackSpeed=ATTACK_SPEED_L;
                    }
                    else if(enemySensor == 1)
                    {
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()-45);
                        timeoutActive=true;
                        if(oneTimeTimerReset)
                        {
                            oneTimeTimerReset=false;
                            resetTimer(&attackCycleTimeout);
                        }
                    }
                    else if(enemySensor == 7)
                    {
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()+45);
                        timeoutActive=true;
                        if(oneTimeTimerReset)
                        {
                            oneTimeTimerReset=false;
                            resetTimer(&attackCycleTimeout);
                        }                     
                    }
                   
                    if(timeoutActive)
                    {
                        if(timerDone(&attackCycleTimeout))
                        {
                            break;
                        }
                    }
                }
                else
                {                    
                    attackSpeed=ATTACK_SPEED_H;
                    enemyFound=true;
                    resetTimer(&attackCycleTimeout);
                    
                }
                
                
                int _lm =  attackSpeed;
                int _rm =  attackSpeed;

                motorGyroFeedbackStraight(&_lm, &_rm);

                //Send to the motor system to be sent by the communications library
                setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsAttack(_lm)));
                setRightMotorSpeed(boundMotorCommands(boundMotorCommandsAttack(_rm)));
            }
            enemyFound=false;
        }
        
    }
}

int boundMotorCommandsAttack(int val)
{
    if(val>MAX_SPEED_ATTACK)
    {
        val=MAX_SPEED_ATTACK;
    }
    else if(val<MIN_SPEED_ATTACK)
    {
        val=MIN_SPEED_ATTACK;
    }
    return val;
}