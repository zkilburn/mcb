/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _MICROS_TIMER_H    /* Guard against multiple inclusion */
#define _MICROS_TIMER_H

#include "MillisTimer.h"
#include <stdint.h>
#include "system_definitions.h"
#include "app.h"

    typedef struct{
        unsigned long timerInterval;
        unsigned long lastMicros;
    }timers_micros_t;

bool timerDoneMicros(timers_micros_t * t);
void setTimerIntervalMicros(timers_micros_t * t, unsigned long interval);
void resetTimerMicros(timers_micros_t * t);

#endif /* _MICROS_TIMER_H */

/* *****************************************************************************
 End of File
 */
