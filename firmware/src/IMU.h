/* 
 * File:   IMU.h
 * Author: Zac
 *
 * Created on February 5, 2018, 4:31 PM
 */

#ifndef IMU_H
#define	IMU_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include "app.h"
    
    //MS value 
#define IMU_UPDATE_TIMER_VALUE 2
    
#define IMU_ADDRESS_WRITE   0xD4
#define IMU_ADDRESS_READ    0xD5
    
#define X_AXIS_GYRO         0
#define Y_AXIS_GYRO         1
#define Z_AXIS_GYRO         2
    //Accel
#define CTRL1_XL            0x10    
#define CTRL1_XL_SETTING    0x66    //104Hz @ full scale at +-16g / AA - 100Hz
    //Gyro
#define CTRL2_G             0x11
#define CTRL2_G_SETTING     0x64    //104Hz @ full scale at +-500dps
    //104 Hz - 0x44
    //208 Hz - 0x54
    //416 Hz - 0x64
    //833 Hz - 0x74
    //1.66Khz - 0x84
#define CTRL3_C             0x12
#define CTRL4_C             0x13
    
#define OUTX_L_G            0x22
#define OUTY_L_G            0x24
#define OUTZ_L_G            0x26
    
#define OUTX_L_XL           0x28
#define OUTY_L_XL           0x2A
#define OUTZ_L_XL           0x2C
    
//======================== Send State Machine States ===========================
typedef enum{
    START_BIT_CONFIG = 0,
    ADDR_BYTE_CONFIG,
    COMMAND_BYTE_CONFIG,
    DATA_BYTE_CONFIG,
    STOP_BIT_CONFIG
}IMU_STATES_CONFIG_t;

typedef enum{
    START_BIT_READ = 0,
    ADDR_BYTE_WRITE,
    REGISTER_BYTE_READ,
    RESTART_BIT,
    ADDR_BYTE_READ,
    CLOCK_BYTE1_READ,
    GRAB_BYTE_L_READ,
    ACK_BYTE_L_READ,
    CLOCK_BYTE2_READ,
    GRAB_BYTE_H_READ,
    NACK_BYTE_H_READ,
    STOP_BIT_READ
}IMU_STATES_READ_t;

typedef enum{
    START_BIT_READ_3 = 0,
    ADDR_BYTE_WRITE_3,
    REGISTER_BYTE_READ_3,
    RESTART_BIT_3,
    ADDR_BYTE_READ_3,
    CLOCK_BYTE1_X_READ_3,
    GRAB_BYTE_X_L_READ_3,
    ACK_BYTE_X_L_READ_3,
    CLOCK_BYTE2_X_READ_3,
    GRAB_BYTE_X_H_READ_3,
    ACK_BYTE_X_H_READ_3,
    CLOCK_BYTE1_Y_READ_3,
    GRAB_BYTE_Y_L_READ_3,
    ACK_BYTE_Y_L_READ_3,
    CLOCK_BYTE2_Y_READ_3,
    GRAB_BYTE_Y_H_READ_3,
    ACK_BYTE_Y_H_READ_3,
    CLOCK_BYTE1_Z_READ_3,
    GRAB_BYTE_Z_L_READ_3,
    ACK_BYTE_Z_L_READ_3,
    CLOCK_BYTE2_Z_READ_3,
    GRAB_BYTE_Z_H_READ_3,           
    NACK_BYTE_Z_H_READ_3,
    STOP_BIT_READ_3
}IMU_STATES_READ_3_t;

typedef enum{
    START_BIT_READ_6 = 0,
    ADDR_BYTE_WRITE_6,
    REGISTER_BYTE_READ_6,
    RESTART_BIT_6,
    ADDR_BYTE_READ_6,
    CLOCK_BYTE1_X_G_READ_6,
    GRAB_BYTE_X_G_L_READ_6,
    ACK_BYTE_X_G_L_READ_6,
    CLOCK_BYTE2_X_G_READ_6,
    GRAB_BYTE_X_G_H_READ_6,
    ACK_BYTE_X_G_H_READ_6,
    CLOCK_BYTE1_Y_G_READ_6,
    GRAB_BYTE_Y_G_L_READ_6,
    ACK_BYTE_Y_G_L_READ_6,
    CLOCK_BYTE2_Y_G_READ_6,
    GRAB_BYTE_Y_G_H_READ_6,
    ACK_BYTE_Y_G_H_READ_6,
    CLOCK_BYTE1_Z_G_READ_6,
    GRAB_BYTE_Z_G_L_READ_6,
    ACK_BYTE_Z_G_L_READ_6,
    CLOCK_BYTE2_Z_G_READ_6,
    GRAB_BYTE_Z_G_H_READ_6,  
    ACK_BYTE_Z_G_H_READ_6,
            
    CLOCK_BYTE1_X_XL_READ_6,
    GRAB_BYTE_X_XL_L_READ_6,
    ACK_BYTE_X_XL_L_READ_6,
    CLOCK_BYTE2_X_XL_READ_6,
    GRAB_BYTE_X_XL_H_READ_6,
    ACK_BYTE_X_XL_H_READ_6,
    CLOCK_BYTE1_Y_XL_READ_6,
    GRAB_BYTE_Y_XL_L_READ_6,
    ACK_BYTE_Y_XL_L_READ_6,
    CLOCK_BYTE2_Y_XL_READ_6,
    GRAB_BYTE_Y_XL_H_READ_6,
    ACK_BYTE_Y_XL_H_READ_6,
    CLOCK_BYTE1_Z_XL_READ_6,
    GRAB_BYTE_Z_XL_L_READ_6,
    ACK_BYTE_Z_XL_L_READ_6,
    CLOCK_BYTE2_Z_XL_READ_6,
    GRAB_BYTE_Z_XL_H_READ_6,             
    NACK_BYTE_Z_XL_H_READ_6,
            
    STOP_BIT_READ_6
}IMU_STATES_READ_6_t;

typedef enum{
    GYRO_REGISTER = 0,
    XL_REGISTER,
   
    NUMBER_OF_CONFIG_STEPS=XL_REGISTER
}CONFIG_STATES_t;

typedef union
{ // this union is used to join and disassemble integers
    uint8_t parts[2];
    int16_t integer;

}join_t;
void initIMU(void);
bool isInverted(void);

void zeroIMUAxisGyro(void);
void zeroIMUAxisAll(void);

void updateZAxis(void);
void updateGyro(void);
void updateXL(void);
void updateIMU(void);

void resetZAngle(void);
void resetXAngle(void);

void readIMU(int axis);
void readIMU3AxisG(void);
void readIMU6Axis(void);

uint16_t getXDPS(void);
uint16_t getYDPS(void);
uint16_t getZDPS(void);

double getXAxisAngle(void);
double getYAxisAngle(void);
double getZAxisAngle(void);

double getXAxisAccel(void);
double getYAxisAccel(void);
double getZAxisAccel(void);

#ifdef	__cplusplus
}
#endif

#endif	/* IMU_H */

