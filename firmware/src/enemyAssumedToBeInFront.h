
#ifndef _ENEMY_ASSUMED_TO_BE_IN_FRONT_H    /* Guard against multiple inclusion */
#define _ENEMY_ASSUMED_TO_BE_IN_FRONT_H

#include <stdbool.h>

bool enemyAssumedToBeInFront(void);

#endif /* _ENEMY_ASSUMED_TO_BE_IN_FRONT_H */
