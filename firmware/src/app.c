// *****************************************************************************
// *****************************************************************************
// Section: Included Files 
// *****************************************************************************
// *****************************************************************************

#include "app.h"
#include "locationFunction/solveLocation.h"
#include "macros.h"
#include "motorControl.h"
#include "I2CFunctions.h"
#include "simulinkDebugger.h"
#include "NeuralNets/calculateSensorOne/solveSensorOne.h"
#include "detectEnemy.h"
#include "AutonomousMode.h"
#include "LIDARSensors.h"
#include "printfUART.h"
#include "sevenSegDigit.h"
#include "spinWeaponUp.h"
// *****************************************************************************
// *****************************************************************************
// Section: Configuration Defines, Alter code operation
// *****************************************************************************
// *****************************************************************************


//--------------IMU CONFIG-------------------
//#define Z_AXIS_GYRO_ONLY
//#define GYRO_ONLY
#define IMU_FULL


//--------------DEBUG CONFIG-------------------
//#define PRINT_GYRO_ANGLES
//#define PRINT_XL
//#define PRINT_LIDAR_DISTANCES
//#define PRINT_MOTOR_VALUES
#define PRINT_RC_INPUTS
//#define PRINT_ENEMY_SENSOR_LOCATION

//--------------COMMS CONFIG-------------------
#define COMMS_ACTIVE
//#define COMMS_TEST
#define COMMS_TEST_TIMER_LENGTH_MS 10

//-------------TEST CONFIG-------------------
//#define TEST_WEAPON_COMMAND_SIMPLE
//#define TEST_LOCOMOTION_COMMS
//#define TEST_MOTOR_COMMANDS_SIMPLE_RIGHT
//#define TEST_MOTOR_COMMANDS_SIMPLE_LEFT
//#define TEST_MOTOR_COMMANDS_SENSOR_FEEDBACK
//#define TEST_SENSOR_SYSTEM
//#define TEST_LETS_DO_TURN_90
//#define TEST_LETS_APPROACH_A_WALL_50
//#define TEST_RC_INPUTS
//#define TEST_MACRO_LIGHT


//# TEST_LETS_FIND_PERPENDICULAR
//#define TEST_LETS_RUN_WALLS
//#define TEST_LETS_RUN_BACK_AND_FORTH
//#define TEST_LETS_RUN_BACK_AND_FORTH_ENEMY


#define NUMBER_OF_SENSORS_TO_TEST 8
static int testAddressToSend= MDB_R_ADDRESS;

// *****************************************************************************
// *****************************************************************************
// Section: Global Data Definitions
// *****************************************************************************
// *****************************************************************************

// *****************************************************************************
/* Application Data

  Summary:
    Holds application data

  Description:
    This structure holds the application's data.

  Remarks:
    This structure should be initialized by the APP_Initialize function.
    
    Application strings and buffers are be defined outside this structure.
*/

APP_DATA appData;

// *****************************************************************************
// *****************************************************************************
// Section: Application Callback Functions
// *****************************************************************************
// *****************************************************************************

/* TODO:  Add any necessary callback functions.
*/

// *****************************************************************************
// *****************************************************************************
// Section: Application Local Functions
// *****************************************************************************
// *****************************************************************************

void initializeICHandles(void);
void initializeInterruptCallbacks(void);


// *****************************************************************************
// *****************************************************************************
// Section: Application Initialization and State Machine Functions
// *****************************************************************************
// *****************************************************************************


/******************************************************************************
  Function:
    void APP_Tasks ( void )

  Remarks:
    See prototype in app.h.
 */
int enemySensor=0;

void APP_Tasks ( void )
{
  static timers_t taskTimer;
  static timers_t secondTimer;
  static timers_t tenMSTimer;
    /* Check the application's current state. */
    switch ( appData.state )
    {
        /* Application's initial state. */
        case APP_STATE_INIT:
        {
            
            setupSpinUp();
            //Init custom timer system (millis timers)
            setTimerInterval(&taskTimer,100);  
            setTimerInterval(&secondTimer,10); 
            setTimerInterval(&tenMSTimer,10);
            writeDigitSevenSeg(14,true);
            
            //Finish app setup
            bool appInitialized = true;
            if (appInitialized)
            {
                appData.state = APP_STATE_SERVICE_TASKS;                
                writeDigitSevenSeg(15,true);
            }
          
            break;
        }

        case APP_STATE_SERVICE_TASKS:
        {                  
             
            //Blink the decimal at 1 second
            blinkDecimal(1000);
#ifndef TEST_RC_INPUTS

            //printf("ICTimer\r\n"); 
            //Gather other RC input channels
            if(updateRCInputs())
            {
                setWeaponMotorSpeed((int)((getChannel6()+255)/5.0));
                //Capture joystick information from the input capture
                appData.verticalJoystick    =   getChannel2();
                appData.horizontalJoystick  =   getChannel4();

                //Mix joystick info and store motor commands
                mixJoysticks(appData.verticalJoystick,appData.horizontalJoystick);
            }
                
#endif
            
            //Every 10ms            
            if(timerDone(&tenMSTimer))
            { 
                WDTCONbits.WDTCLRKEY=0x5743;
                //printf("Macros\r\n"); 
                updateMacros();
            }
            
            //Every 100ms
            if(timerDone(&taskTimer))
            {
                //printf("Debug\r\n"); 
                debugStuff();               
            }
            
            if(timerDone(&secondTimer))
            {               
                //printf("OneSecond\r\n");    
                LATEbits.LATE7 ^=1;
                //writeDigitSevenSegNoDec(8);  
                
                enemySensor=detectEnemy();
                
                //double sensorOnePrediction = solveSensorOne((double *)getSensorDistancesSubarray7(0));
                //printf("Robot Angle: %f\n",robotAngle); 
                //printf("Sensor 1 prediction: %f\r\n",sensorOnePrediction); 
            }
            
            //---------------------IMU Update system-----------------------
            #ifdef IMU_FULL
                updateIMU();
            #elif defined GYRO_ONLY
                //All gyro-axis update 
                updateGyro();            
            #elif defined Z_AXIS_GYRO_ONLY
                //Z-axis only update
                //updateZAxis();
            #endif
            //---------------------Communications system-----------------------
            #ifdef COMMS_ACTIVE
                #ifndef COMMS_TEST
                    updateCommunications();
                #endif 

                #ifdef COMMS_TEST
                    static timers_t boardCommTestTimer;
                    static bool firstTime=true;
                    if(firstTime)
                    {
                        firstTime=false;
                        setTimerInterval(&boardCommTestTimer,COMMS_TEST_TIMER_LENGTH_MS);
                    }
                    if(timerDone(&boardCommTestTimer))
                    {
      
#ifdef TEST_SENSOR_SYSTEM
                        if(testBoardComms(testAddressToSend))
                        {
                            testAddressToSend++;
                            if(testAddressToSend>=PSB_1_ADDRESS+NUMBER_OF_SENSORS_TO_TEST)
                            {
                                testAddressToSend= PSB_1_ADDRESS;
                            }
                        }
#elif defined(TEST_LOCOMOTION_COMMS)
                        if(testBoardComms(testAddressToSend))
                        {
                            
                            if(testAddressToSend==MDB_R_ADDRESS)
                            {
                                testAddressToSend= MDB_L_ADDRESS;
                            }
                            else
                            {
                                testAddressToSend = MDB_R_ADDRESS;
                            }
                        }
#else
                        testBoardComms(testAddressToSend);        
#endif
                    }
                #endif  
            #endif
            break;
        }
        /* The default state should never be executed. */
        default:
        {
            /* TODO: Handle error in application's state machine. */
            break;
        }
    }
}
/*******************************************************************************
  Function:
    void APP_Initialize ( void )

  Remarks:
    See prototype in app.h.
 */
void APP_Initialize ( void )
{
    //MISC PIN SETS (PROB UNNECESSARY)
//    TRISBbits.TRISB2= 1;
//    LATBbits.LATB4  = 1;
//    LATBbits.LATB5  = 0;
    /* Place the App state machine in its initial state. */
    appData.state = APP_STATE_INIT;
    //TIMERS
    SYS_INT_SourceEnable(INT_SOURCE_TIMER_1);   
    //SYS_INT_SourceEnable(INT_SOURCE_TIMER_2);   
    PLIB_TMR_Start(TMR_ID_1); 
    PLIB_TMR_Start(TMR_ID_2);  
    PLIB_TMR_Start(TMR_ID_4);  
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //I2C  
    appData.I2CHandle   = DRV_I2C_Open(sysObj.drvI2C0,DRV_IO_INTENT_READWRITE);
    
    //Setup smart I2C handling timeouts
    setupI2CFunctions();
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //Run seven seg for mutex init
    initSevenSeg();    
    writeDigitSevenSeg(0,true);
    
    //UART
    initializeUARTHandles();
       
    WDTCONbits.WDTCLRKEY=0x5743; 
    //Setup printf redirect 
    setupPrintf(appData.USART1Handle); 
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //UART Buffer system init
    initUARTBufferSystem(appData.USART0Handle,appData.USART1Handle,appData.USART2Handle,appData.USART3Handle);  
    writeDigitSevenSeg(10,true);
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //Initialize Input Capture Handles (RC Receiver)
    initializeICHandles(); 
    writeDigitSevenSeg(11,true);
    
    WDTCONbits.WDTCLRKEY=0x5743;
     //Setup the FastTransfer communications system
    setupCommunications();            
    writeDigitSevenSeg(12,true);
//    while(1)
//    {
//        sendDataSimulink();
//    }
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //Initialize IMU
#if (defined Z_AXIS_GYRO_ONLY) || (defined GYRO_ONLY) || (defined IMU_FULL)
    initIMU();     
    writeDigitSevenSeg(13,true);
#endif
    
    WDTCONbits.WDTCLRKEY=0x5743;
    //Solver neural net bootup
    initLocationSolver();
    initEnemySolver();
    //initSensorOneSolver();
    setupAutonomous();
    //WDTCONbits.ON=0;
}

APP_STATES getApplicationState(void)
{
    return appData.state;
}

void initializeICHandles(void)
{
    DRV_IC0_Start();
    DRV_IC1_Start();
    //setupTimerHandleRC();
}

timers_t RCDebugTimer;
bool firstTime=true;
void debugStuff(void)
{
    
    //resetI2CSystem();
    #ifdef PRINT_MOTOR_VALUES
        printf("lm: %d, rm: %d, weapon: %d\r\n",getLeftMotorSpeed(),getRightMotorSpeed(),getWeaponMotorSpeed());
    #endif
    #ifdef TEST_MOTOR_COMMANDS_SIMPLE_RIGHT
        testAddressToSend = MDB_R_ADDRESS;
    #endif

    #ifdef TEST_MOTOR_COMMANDS_SIMPLE_LEFT
        testAddressToSend = MDB_L_ADDRESS;
    #endif

    #ifdef TEST_WEAPON_COMMAND_SIMPLE        
        testAddressToSend = WMCB_ADDRESS;     
    #endif

    #ifdef TEST_SENSOR_SYSTEM
        //testAddressToSend = PSB_1_ADDRESS;
    #endif

    #if defined(PRINT_GYRO_ANGLES) && defined(PRINT_XL_ANGLES)
        printf("Ang %d, %d, %d ",getXAxisAngle(),getYAxisAngle(),getZAxisAngle());
        printf("XL %4.3f, %4.3f, %4.3f\n",getXAxisAccel(),getYAxisAccel(),getZAxisAccel());
    #elif defined PRINT_GYRO_ANGLES
        printf("Angles %4.3f, %4.3f, %4.3f\r\n",getXAxisAngle(),getYAxisAngle(),getZAxisAngle());
    #elif defined PRINT_XL
        printf("XL %4.3f, %4.3f, %4.3f\r\n\r\n",getXAxisAccel(),getYAxisAccel(),getZAxisAccel());
    #endif

    #ifdef PRINT_LIDAR_DISTANCES
        double * sensorD;
        sensorD=(double *)getSensorDistancesArray();
         printf("Sensor Array: ");
            int i;
            for(i=0;i<8;i++)
            {
                printf("%4.3f",sensorD[i]);
                printf(" ");

            }
            printf("\r\n\r\n");
    #endif
#ifdef PRINT_ENEMY_SENSOR_LOCATION
//Printout enemy location
            
                printf("X: %f, Y: %f\r\n", getXPos(),getYPos());
                if(enemySensor>=0)
                {
                    printf("Detected Enemy: %d\r\n\r\n", enemySensor+1);
                }
                else
                {
                    printf("No Enemy\r\n\r\n");
                }
#endif
    
#ifdef TEST_LETS_DO_TURN_90
            doTurn(90);
            while(1);
#endif
#ifdef TEST_LETS_APPROACH_A_WALL_50
            doApproach(50);
            while(1);
#endif   
#ifdef TEST_LETS_FIND_PERPENDICULAR
            printf("Finding Perp\r\n");
            setMacroCommand(PERPENDICULAR);
            findPerpendicular(2);
            
            printf("Done Finding Perp\r\n");
            while(1);
#endif
#ifdef TEST_LETS_RUN_WALLS
            setMacroCommand(PERPENDICULAR);
      runWalls();      
      while(1);
#endif
#ifdef TEST_LETS_RUN_BACK_AND_FORTH
            setMacroCommand(PERPENDICULAR);
      runBackAndForth();      
      while(1);
#endif
#ifdef TEST_LETS_RUN_BACK_AND_FORTH_ENEMY
            setMacroCommand(PERPENDICULAR);
      runBackAndForthFindEnemy();      
      while(1);
#endif
            
#ifdef TEST_RC_INPUTS
            if(firstTime)
            {
                setTimerInterval(&RCDebugTimer,1000);
                firstTime=false;
            }
            
            if(timerDone(&RCDebugTimer))
            {
                //Gather other RC input channels
                if(updateRCInputs())
                {
                    printf("RCInputs Active\r\n"); 
                    printf("\r\n");
                }
                else
                {                    
                    printf("\r\n"); 
                    printf("\r\n");
                }
            }
            

#endif
#ifdef PRINT_RC_INPUTS
            printf("Chan 1: %d\r\n",getChannel1());            
            printf("Chan 2: %d\r\n",getChannel2());
            printf("Chan 3: %d\r\n",getChannel3());         
            printf("Chan 4: %d\r\n",getChannel4());          
            printf("Chan 5: %d\r\n",getChannel5());       
            printf("Chan 6: %d\r\n\r\n",getChannel6());
#endif
            
#ifdef TEST_MACRO_LIGHT
            TRISBbits.TRISB7 = 0;
            static bool toggleLEDDD=true;
            if(toggleLEDDD)
            {
                LATBbits.LATB7 =0;
                toggleLEDDD=0;
                
            }
            else
            {
                LATBbits.LATB7 =1;
                toggleLEDDD=1;
                
            }
#endif
}


 

/*******************************************************************************
 End of File
 */
