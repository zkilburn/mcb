
#ifndef _AUTONOMOUS_MODE_H    /* Guard against multiple inclusion */
#define _AUTONOMOUS_MODE_H

void setupAutonomous(void);
void runAutonomous(void);
void runAutonomous2(void);



#endif /* _AUTONOMOUS_MODE_H */

/* *****************************************************************************
 End of File
 */
