/* ************************************************************************** */
/** Do Turn Macro System

  @Company
    Zac Kilburn

  @File Name
    doTurn.c

  @Summary
    Brief description of the file.

  @Description
    Turning system for autonomous purposes.
 */
#include "doTurn.h"
#include "PID.h"

#include "holdRotationWeaponStopped.h"
#include "motorControl.h"
#include "MillisTimer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "helperFunctions.h"

//-------------------- TURN ------------------------
#define MIN_SPEED_TURN -35 //30
#define MAX_SPEED_TURN 35  //30

#define KP_TURN 5
#define KI_TURN 0.01//.1
#define KD_TURN .5

timers_t debugTHIS;
timers_t decisionTiming, sampleTiming;

int boundMotorCommandsTurn(int val);


void doTurn(double angle)
{
    //Prep data values
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    resetXAngle();      //Relative turning (zero before we turn)
    
    resetPIDController(angle, KP_TURN, KI_TURN, KD_TURN);
    
    printf("Turn\r\n");
    //Do method until turn is complete or told to stop
    while(!isAtAngleStable(angle,getXAxisAngle(),2) && (getMacroCommand()!=0))
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
        //Do sample processing and input to PID system
        if(timerDone(&sampleTiming))
        {
            updatePIDOutput(getXAxisAngle());
        }
        
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {
            //Condition the output from the PID so its appropriate to drive the motors
            float outputToMotor = getPIDOutput();
            int _lm =  -outputToMotor;
            int _rm =  outputToMotor;
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsTurn(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsTurn(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            //printf("Chan 1: %d\r\n",getChannel1());    
            //printf("angle: %f, lm: %d, rm: %d\r\n",getXAxisAngle(),getLeftMotorSpeed(),getRightMotorSpeed());
            //printf("MacroCommand %d\r\n", getMacroCommand());
            //printf("E: %3f, P: %3f, I: %3f, D: %3f, O: %3f\n", getErrorComponent(),getProportionalComponent(), getIntegralComponent(), getDerivativeComponent(), getPIDOutput());
       }
#endif
        
    }
    printf("TurnDone\r\n");
    
    completeMacro();
}

bool isAtAngleStable(double target, double currentAngle, double acceptableAngleRange)
{
    static timers_t stableEndTimer;
    static bool setup = false;
    if(!setup)
    {
        setTimerInterval(&stableEndTimer,100);
        setup=true;
    }
    
    if(isAboutDouble(target,currentAngle,acceptableAngleRange) && timerDone(&stableEndTimer))
    {
        return true;
    }
    else if(!isAboutDouble(target,currentAngle,acceptableAngleRange))
    {
        resetTimer(&stableEndTimer);
        return false;
    }
    else
    {
        return false;
    }
    
}

int boundMotorCommandsTurn(int val)
{
    if(val>MAX_SPEED_TURN)
    {
        val=MAX_SPEED_TURN;
    }
    else if(val<MIN_SPEED_TURN)
    {
        val=MIN_SPEED_TURN;
    }
    return val;
}