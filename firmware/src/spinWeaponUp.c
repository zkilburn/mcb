#include "spinWeaponUp.h"
#include "PID.h"

#include "holdRotationWeaponStopped.h"
#include "motorControl.h"
#include "MillisTimer.h"
#include "driveAssistance.h"
#include "macros.h"
//#include "IMU.h"
#define DEBUG_PID_UART
//#define WEAPON_SPEED_SETTING 25
#define HOLD_KP 4
#define HOLD_KI 0
#define HOLD_KD 0.5

timers_t spinUpTimer;
timers_t rampTimer;
timers_t debugTHIS;
int motorSpeed;
bool firstTime=true;
void setupSpinUp(void)
{ 
    setupHoldPosition();
    setTimerInterval(&rampTimer,100);
    setTimerInterval(&spinUpTimer,3000); 
    setTimerInterval(&debugTHIS,100);  
}

void testMovementSpinning(void)
{
    spinWeaponUp(15);
    
    setLeftMotorSpeed(20);
    setRightMotorSpeed(20);
    resetTimer(&spinUpTimer);
    while(!timerDone(&spinUpTimer) && (getMacroCommand()!=0))
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
                
        #ifndef IGNORE_MACRO_KILL
                //Update the RC input to cancel
                updateRCInputs();
        #endif

        
    }
   completeMacro();
}

void spinWeaponUp(uint8_t weaponSpeed)
{   
    //Set angle to 0
    resetXAngle();  
    setGyroRelativeAngle();  
    
    //Hold angle at 0
    resetPIDController(0, HOLD_KP, HOLD_KI, HOLD_KD);
    
    //setWeaponMotorSpeed(WEAPON_SPEED_SETTING);
    resetTimer(&spinUpTimer);
    motorSpeed=0;
    while(!timerDone(&spinUpTimer) && (getMacroCommand()!=0))
    {
        if(timerDone(&rampTimer) && motorSpeed!=weaponSpeed)
        {
            motorSpeed++;
            setWeaponMotorSpeed(motorSpeed);
            resetTimer(&spinUpTimer);
        }
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
                
        #ifndef IGNORE_MACRO_KILL
                //Update the RC input to cancel
                updateRCInputs();
        #endif

        holdPosition();

#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {            
            
            printf("lm: %d, rm: %d\r\n", getLeftMotorSpeed(), getRightMotorSpeed());
            printf("gyro: %f\r\n\r\n",getXAxisAngle());
        }
#endif
        
    }
    
   completeMacro();
    
}