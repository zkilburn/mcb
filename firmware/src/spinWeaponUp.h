
#ifndef _SPIN_WEAPON_UP_H    /* Guard against multiple inclusion */
#define _SPIN_WEAPON_UP_H

#include <stdbool.h>
#include <stdint.h>


void spinWeaponUp(uint8_t weaponSpeed);

void setupSpinUp(void);
void testMovementSpinning(void);

#endif /* _SPIN_WEAPON_UP_H */
