/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _I2C_FUNCTIONS    /* Guard against multiple inclusion */
#define _I2C_FUNCTIONS

#include <xc.h>
#include <stdbool.h>


void setupI2CFunctions(void);
bool WaitReceiverByteAck(void);
bool WaitRxByteAvailable(void);
bool WaitTransmitterWriteCompleted(void);
void resetI2CSystem(void);
void I2cStart(void);
void I2cStop (void);
bool getMutex(void);
void setMutex(bool mu);


#endif /* _I2C_FUNCTIONS */

/* *****************************************************************************
 End of File
 */
