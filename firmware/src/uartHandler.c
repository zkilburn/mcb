
#include "uartHandler.h"
#include "app.h"

volatile uartCircBuff_t recBuf0, sendBuf0, recBuf1,sendBuf1, recBuf2,sendBuf3;
DRV_HANDLE internalUART0Handle,internalUART1Handle,internalUART2Handle,internalUART3Handle;
volatile bool transmitStall0 = true, transmitStall1 = true, transmitStall3 = true;
extern APP_DATA appData;

void wipeBuffer(volatile unsigned char * buf);
void bufPut(volatile uartCircBuff_t * buf, unsigned char c);
unsigned char bufGet(volatile uartCircBuff_t * buf);
unsigned int buff_modulo_inc(const unsigned int value, const unsigned int modulus);

void initUARTBufferSystem(DRV_HANDLE uartHandle0,DRV_HANDLE uartHandle1,DRV_HANDLE uartHandle2,DRV_HANDLE uartHandle3)
{
    recBuf0.head     =   0;
    recBuf0.tail     =   0;
    recBuf0.count    =   0;
    wipeBuffer(recBuf0.buff);    
    sendBuf0.head    =   0;
    sendBuf0.tail    =   0;
    sendBuf0.count   =   0;
    wipeBuffer(sendBuf0.buff);      
    internalUART0Handle = uartHandle0;
    
    recBuf1.head     =   0;
    recBuf1.tail     =   0;
    recBuf1.count    =   0;
    wipeBuffer(recBuf0.buff);    
    sendBuf1.head    =   0;
    sendBuf1.tail    =   0;
    sendBuf1.count   =   0;
    wipeBuffer(sendBuf1.buff);      
    internalUART1Handle = uartHandle1;
    
    
    recBuf2.head     =   0;
    recBuf2.tail     =   0;
    recBuf2.count    =   0;
    wipeBuffer(recBuf2.buff);
    internalUART2Handle = uartHandle2;
    
    sendBuf3.head     =   0;
    sendBuf3.tail     =   0;
    sendBuf3.count    =   0;
    wipeBuffer(sendBuf3.buff);
    internalUART3Handle = uartHandle3;
}

void initializeUARTHandles(void)
{
    /*=================================USART 0 (485 TX)=======================*/ 
    SYS_STATUS usart0Status = DRV_USART_Status(sysObj.drvUsart0);
    if (SYS_STATUS_READY == usart0Status)
    {        
        //Tie the driver to a tx buffer transmitting function (handle buffer tx))
        DRV_USART_ByteTransmitCallbackSet(sysObj.drvUsart0,(void *)UART0_TX_CALLBACK);
        
        //Tie the driver to a rx buffer receiving function (handle buffer rx))
        DRV_USART_ByteReceiveCallbackSet(sysObj.drvUsart0, (void *)UART0_RX_CALLBACK);
        
        //Setup and start the USART Driver
        appData.USART0Handle = DRV_USART_Open(sysObj.drvUsart0,DRV_IO_INTENT_READWRITE);
        
        //Baud Rate setting attempt
        //DRV_USART_BaudSet(appData.USART0Handle,111111);
        
        //Ensure system start
        if (DRV_HANDLE_INVALID == appData.USART0Handle)
        {
            writeDigitSevenSeg(1,true);
            while(1);
        }
    }  
    
    /*=============================  USART 1 (DEBUG 1) =======================*/ 
    SYS_STATUS usart1Status = DRV_USART_Status(sysObj.drvUsart1);
    if (SYS_STATUS_READY == usart1Status)
    {        
        //Tie the driver to a tx buffer transmitting function (handle buffer tx))
        DRV_USART_ByteTransmitCallbackSet(sysObj.drvUsart1,(void *)UART1_TX_CALLBACK);
        
        //Tie the driver to a rx buffer receiving function (handle buffer rx))
        DRV_USART_ByteReceiveCallbackSet(sysObj.drvUsart1, (void *)UART1_RX_CALLBACK);
        
        //Setup and start the USART Driver
        appData.USART1Handle = DRV_USART_Open(sysObj.drvUsart1,DRV_IO_INTENT_READWRITE);
        
        //Baud Rate setting attempt
        //DRV_USART_BaudSet(appData.USART1Handle,115200);
        
        //Ensure system start
        if (DRV_HANDLE_INVALID == appData.USART1Handle)
        {
            writeDigitSevenSeg(2,true);
            while(1);
        }
    } 
    
    /*================================ USART 2 (485 RX)=======================*/ 
    SYS_STATUS usart2Status = DRV_USART_Status(sysObj.drvUsart2);
    if (SYS_STATUS_READY == usart2Status)
    {        
        //NO TX on UART2
        
        //Tie the driver to a rx buffer receiving function (handle buffer rx))
        DRV_USART_ByteReceiveCallbackSet(sysObj.drvUsart2, (void *)UART2_RX_CALLBACK);
        
        //Setup and start the USART Driver
        appData.USART2Handle = DRV_USART_Open(sysObj.drvUsart2,DRV_IO_INTENT_READWRITE);
        
        //Baud Rate setting attempt
        //DRV_USART_BaudSet(appData.USART2Handle,115200);
        
        //Ensure system start
        if (DRV_HANDLE_INVALID == appData.USART2Handle)
        {
            writeDigitSevenSeg(3,true);
            while(1);
        }
    } 
    /*=============================== USART 3 (DEBUG 2)=======================*/ 
    SYS_STATUS usart3Status = DRV_USART_Status(sysObj.drvUsart3);
    if (SYS_STATUS_READY == usart3Status)
    {                
        //Tie the driver to a tx buffer receiving function (handle buffer tx))
        DRV_USART_ByteTransmitCallbackSet(sysObj.drvUsart3,(void *)UART3_TX_CALLBACK);
        
        //NO RX on UART3
        
        //Setup and start the USART Driver
        appData.USART3Handle = DRV_USART_Open(sysObj.drvUsart3,DRV_IO_INTENT_READWRITE);
        
        //Baud Rate setting attempt
        //DRV_USART_BaudSet(appData.USART3Handle,115200);
        
        //Ensure system start
        if (DRV_HANDLE_INVALID == appData.USART3Handle)
        {
            writeDigitSevenSeg(4,true);
            while(1);
        }
    } 
}
    
 /*================================= USART 0 =================================*/
void writeByteUART0(unsigned char ch)
{
    //DUPLICATE THE OUTPUT TO THE DEBUG 2 CHANNEL
    //writeByteUART3(ch);
    
    SYS_INT_SourceDisable( INT_SOURCE_USART_1_TRANSMIT );
    bufPut(&sendBuf0,ch);
    //If the ISR is not delivering bytes to the TX, force it to
    if(transmitStall0 && !DRV_USART_TransmitBufferIsFull(internalUART0Handle))
    {
        DRV_USART_WriteByte(internalUART0Handle,bufGet(&sendBuf0));
    }
    else if(DRV_USART_TransmitBufferIsFull(internalUART0Handle))
    {                
        transmitStall0=false;
    }
    SYS_INT_SourceEnable( INT_SOURCE_USART_1_TRANSMIT );
}

void UART0_TX_CALLBACK(void)
{
    //If there is data stored in the uart buffer waiting to be sent
    if(sendBuf0.count>0)
    {        
        DRV_USART_WriteByte(internalUART0Handle,bufGet(&sendBuf0));
    }    
    else
    {
        transmitStall0 = true;
    }
}

/*UNUSED*/
uint16_t rxBytesAvailable0(void)
{
    return recBuf0.count;
}

/*UNUSED*/
unsigned char peekByteUART0(void)
{
    if(recBuf0.head != recBuf0.tail)
        return recBuf0.buff[recBuf0.tail];
    else
        return 0;
}
    
/*UNUSED*/
unsigned char readByteUART0(void)
{
    if(recBuf0.head != recBuf0.tail)
    {
        //grab the byte we want out
        uint8_t byteBack = bufGet(&recBuf0);       
        return byteBack;
    }
    return 0;
}
/*UNUSED*/

void UART0_RX_CALLBACK(void)
{
    unsigned char readByte= DRV_USART_ReadByte(internalUART0Handle);
    //writeByteUART1(readByte);
    bufPut(&recBuf0,readByte);
}
    
/*================================= USART 1 DEBUG =================================*/
void writeByteUART1(unsigned char ch)
{
    SYS_INT_SourceDisable( INT_SOURCE_USART_2_TRANSMIT );
    bufPut(&sendBuf1,ch);
    //If the ISR is not delivering bytes to the TX, force it to
    if(transmitStall1 && !DRV_USART_TransmitBufferIsFull(internalUART1Handle))
    {
        DRV_USART_WriteByte(internalUART1Handle,bufGet(&sendBuf1));
    }
    else if(DRV_USART_TransmitBufferIsFull(internalUART1Handle))
    {                
        transmitStall1=false;
    }
    SYS_INT_SourceEnable( INT_SOURCE_USART_2_TRANSMIT );
}

void UART1_TX_CALLBACK(void)
{
    //If there is data stored in the uart buffer waiting to be sent
    if(sendBuf1.count>0)
        DRV_USART_WriteByte(internalUART1Handle,bufGet(&sendBuf1));
    else
        transmitStall1 = true;
}

uint16_t rxBytesAvailable1(void)
{
    return recBuf1.count;
}

unsigned char peekByteUART1(void)
{
    if(recBuf1.head != recBuf1.tail)
        return recBuf1.buff[recBuf1.tail];
    else
        return 0;
}
    
unsigned char readByteUART1(void)
{
    if(recBuf1.head != recBuf1.tail)
    {
        //grab the byte we want out
        uint8_t byteBack = bufGet(&recBuf1);       
        return byteBack;
    }
    return 0;
}

void UART1_RX_CALLBACK(void)
{
    //unsigned char readByte=DRV_USART_ReadByte(internalUART1Handle);
    bufPut(&recBuf1,DRV_USART_ReadByte(internalUART1Handle));
   //writeByteUART1(readByte);
}


uint16_t rxBytesAvailable2(void)
{
    return recBuf2.count;
}

unsigned char peekByteUART2(void)
{
    if(recBuf2.head != recBuf2.tail)
        return recBuf2.buff[recBuf2.tail];
    else
        return 0;
}
    
unsigned char readByteUART2(void)
{
    if(recBuf2.head != recBuf2.tail)
    {
        //grab the byte we want out
        uint8_t byteBack = bufGet(&recBuf2);       
        return byteBack;
    }
    return 0;
}

void UART2_RX_CALLBACK(void)
{
    unsigned char readByte=DRV_USART_ReadByte(internalUART2Handle);
    bufPut(&recBuf2,readByte);//DRV_USART_ReadByte(internalUART2Handle));
    
    //DUPLICATE THE RX 485 TO THE DEBUG 2 CHANNEL
    //writeByteUART3(readByte);
    //DUPLICATE THE RX 485 TO THE DEBUG 1 CHANNEL
    //writeByteUART1(readByte);
}

void wipeBuffer2(void)
{
    wipeBuffer(recBuf2.buff);
}

//---------------------USART 3 DEBUG NUM 2--------------------
void writeByteUART3(unsigned char ch)
{
    SYS_INT_SourceDisable( INT_SOURCE_USART_3_TRANSMIT );
    bufPut(&sendBuf3,ch);
    //If the ISR is not delivering bytes to the TX, force it to
    if(transmitStall3 && !DRV_USART_TransmitBufferIsFull(internalUART3Handle))
    {
        DRV_USART_WriteByte(internalUART3Handle,bufGet(&sendBuf3));
    }
    else if(DRV_USART_TransmitBufferIsFull(internalUART3Handle))
    {                
        transmitStall3=false;
    }
    SYS_INT_SourceEnable( INT_SOURCE_USART_3_TRANSMIT );
}

void UART3_TX_CALLBACK(void)
{
    //If there is data stored in the uart buffer waiting to be sent
    if(sendBuf3.count>0)
    {        
        DRV_USART_WriteByte(internalUART3Handle,bufGet(&sendBuf3));
    }    
    else
    {
        transmitStall3 = true;
    }
}

/*=========================Generic UART Functions=============================*/
void wipeBuffer(volatile unsigned char * buf)
{
    int i=0;
    for(i=0;i<UART_BUFFER_LENGTH;i++)
    {
        buf[i]=0;
    }
}

void bufPut(volatile uartCircBuff_t * buf, unsigned char c)
{
     if (buf->count < UART_BUFFER_LENGTH)
    {
        buf->buff[buf->head] = c;
        buf->head = buff_modulo_inc(buf->head, UART_BUFFER_LENGTH);
        ++buf->count;
    } 
     else
    {
        buf->buff[buf->head] = c;
        buf->head = buff_modulo_inc(buf->head, UART_BUFFER_LENGTH);
        buf->tail = buff_modulo_inc(buf->tail, UART_BUFFER_LENGTH);

    }
}

unsigned char bufGet(volatile uartCircBuff_t * buf)
{
    unsigned char c;
    if (buf->count > 0)
    {
        c = buf->buff[buf->tail];
        buf->buff[buf->tail]=0;
        buf->tail = buff_modulo_inc(buf->tail, UART_BUFFER_LENGTH);
        --buf->count;
    } 
    else
    {
        c = 0;
    }
    return (c);
}

unsigned int buff_modulo_inc(const unsigned int value, const unsigned int modulus)
{
    unsigned int my_value = value + 1;
    if (my_value >= modulus)  // WAS >= SHOULD BE JUST >
    {
        my_value = 0;
    }
    return (my_value);
}