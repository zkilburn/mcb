#include "MillisTimer.h"

//Each interrupt is 100us so we count to 10x and thats a millisecond
#define MILLIS_INTERVAL 10

unsigned long globalTimeMillis;
unsigned short millisCounter=0;



unsigned long millis(void)
{
    return globalTimeMillis;
}

bool timerDone(timers_t * t)
{
    if(abs(millis()-t->lastMillis)>t->timerInterval)
    {
        t->lastMillis=millis();
        return true;
    }
    else
    {
        return false;
    }
}

void setTimerInterval(timers_t * t, unsigned long interval)
{
    t->timerInterval= interval;
    resetTimer(t);
}

void resetTimer(timers_t * t)
{
    t->lastMillis=millis();
}

void globalTimerTracker( )
{
    //Wait a number of interrupts to count a millisecond
//    if(millisCounter>MILLIS_INTERVAL)
//    {
        globalTimeMillis++;
//        millisCounter = 0;
//    }
//    else
//    {
//        millisCounter++;
//    }
}