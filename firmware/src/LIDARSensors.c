#include "LIDARSensors.h"

/* ===========================  Datas from PSU ============================== */
uint16_t rawSensorDistances[8]  ={0,0,0,0,0,0,0,0};
double      sensorDistances[8]  ={0,0,0,0,0,0,0,0};//{483.6599,929.33,859.0,856.6,791.8,450.5,416.4,523.2};//{21.2836,22.0676,17.0268,17.6540,21.2836,22.0676,25.5403,26.4811}; //Result should be 20
                                    //{21.2836,22.0676,17.0268,17.6540,21.2836,22.0676,25.5403,26.4811};
                                    //{1166,305,139.4,94.5,128.5,429.2,583.3,1125.7};
                                    //{483.6599,929.33,859.0,856.6,791.8,450.5,416.4,523.2};

void setSensorDistance(uint8_t sensorNumber, double sensorValue)
{
    sensorDistances[sensorNumber]=sensorValue;
}

double getSensorDistance(uint8_t sensorNumber)
{
    return sensorDistances[sensorNumber];
}
double getSensorDistanceCenterOfRobot(uint8_t sensorNumber)
{
    return sensorDistances[sensorNumber]+38;
}

double * getSensorDistancesArray(void)
{
    return sensorDistances;
}

    double sensorDistancesOffset[8];
double * getSensorDistancesArrayCenterOfRobot(void)
{
    int i=0;
    for(i=0;i<8;i++)
    {
        sensorDistancesOffset[i]=sensorDistances[i]+38;
    }
    return (double*)sensorDistancesOffset;
}

double distancesSubarray[7];

double * getSensorDistancesSubarray7(uint8_t indexToSkip)
{
    int i=0,j=0;
    for(i=0;i<8;i++)
    {
        if(i==indexToSkip)//if this is the index to skip
        {
            //Do not load the index
            //Do not increment the subarray index pointer
        } 
        else
        {
            distancesSubarray[j]=sensorDistances[i];
            j++;
        }
        
    }
    return distancesSubarray;
}
