
#include "sevenSegDigit.h"
#include <stdio.h>
#include "../../../../framework/driver/i2c/drv_i2c.h"
#include "I2CFunctions.h"

//============================ Method Prototypes ===============================
void writeValueSevenSeg(char num, bool decimalPoint);
void I2cStart(void);
void I2cStop (void);
//========================== Variable Declarations =============================
timers_t blinkyTimer;
char lastCharOnScreen;
bool decBlinkState=false;
timers_t mutexTimer;
//======================== Send State Machine States ===========================
typedef enum{
    START_BIT = 0,
    ADDR_BYTE,
    COMMAND_BYTE,
    DATA_BYTE,
    STOP_BIT
}LED_DIGIT_STATES_t;
//==============================================================================
//DRV_HANDLE I2CHandleSevenSeg;  //Maybe should have local copy


//==============================================================================
void initSevenSeg(void)
{
    setTimerInterval(&mutexTimer,1);
    LED_DIGIT_STATES_t states=START_BIT;
    bool writeIncomplete=true;
    //Make sure another thread is not using the I2C
    if(!getMutex())
    {
        //Take the mutex before use
        setMutex(true);
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT:
                    I2cStart();
                    states++;
                    break;
                case ADDR_BYTE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,LED_DIGIT_ADDR_WRITE);
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    states++;
                    break;
                case COMMAND_BYTE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,LED_DIGIT_WRITE_CONFIG);  
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));  
                    states++;
                    break;
                case DATA_BYTE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,LED_DIGIT_ENABLE_OUTPUT); 
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1)); 
                    states++;
                    break;
                case STOP_BIT:
                    I2cStop();
                    states=START_BIT;
                    writeIncomplete=false;
                    break;
            }
        }
        //Return the mutex when finished
        setMutex(false);
    }
    //Setup blinky timer for use
    setTimerInterval(&blinkyTimer,500);
}
//==============================================================================
void blinkDecimal(unsigned long ms)
{
    static unsigned long blinkInterval=0;
    if(ms != blinkInterval)
    {
        setTimerInterval(&blinkyTimer,ms);
        blinkInterval=ms;
    }
    
    if(timerDone(&blinkyTimer))
    {
        //printf("Blinky\r\n");
       decimalBlink(); 
    }
}

void decimalBlink(void)
{
    if(decBlinkState) //IF DECIMAL POINT WAS ON
        {
            //Turn it off
            writeValueSevenSeg(lastCharOnScreen,false);
            decBlinkState=false;
        }
        else
        {
            //Turn it on
            writeValueSevenSeg(lastCharOnScreen,true);
            decBlinkState=true;
        }
}
//==============================================================================
void writeValueSevenSeg(char num, bool decimalPoint)
{
    //Make sure another thread is not using the I2C
    if(!getMutex())
    {
        //Take the mutex before use
        setMutex(true);
        //Record last char on digit
        lastCharOnScreen=num;
        //Setup state machine
        LED_DIGIT_STATES_t states=START_BIT;
        //Mark that we need to complete state machine
        bool writeIncomplete=true;

        //Continue till write is complete
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT:
                    I2cStart();
                    states++;
                    break;
                case ADDR_BYTE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,LED_DIGIT_ADDR_WRITE);
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    states++;
                    break;
                case COMMAND_BYTE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,LED_DIGIT_WRITE_DATA);  
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));   
                    states++;
                    break;
                case DATA_BYTE:            
                    if(decimalPoint)
                        PLIB_I2C_TransmitterByteSend(I2C_ID_1,num & SEG_DP);  
                    else
                        PLIB_I2C_TransmitterByteSend(I2C_ID_1,num);                      
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    states++;
                    break;
                case STOP_BIT:
                    I2cStop();
                    states=START_BIT;
                    writeIncomplete=false;
                    break;
            }
        }  
        //Return the mutex when finished
        setMutex(false);
    }
}

void writeDigitSevenSegNoDec(char num)
{
    writeDigitSevenSeg(num,decBlinkState);
}
//==============================================================================
void writeDigitSevenSeg(char num, bool decimalPoint)
{
    //Determine which number is requested
    switch(num)
    {
        case 0:
            writeValueSevenSeg(DIGIT0,decimalPoint);
            break;
        case 1:
            writeValueSevenSeg(DIGIT1,decimalPoint);            
            break;
        case 2:
            writeValueSevenSeg(DIGIT2,decimalPoint);
            break;
        case 3:
            writeValueSevenSeg(DIGIT3,decimalPoint);
            break;
        case 4:
            writeValueSevenSeg(DIGIT4,decimalPoint);
            break;
        case 5:
            writeValueSevenSeg(DIGIT5,decimalPoint);
            break;
        case 6:
            writeValueSevenSeg(DIGIT6,decimalPoint);
            break;
        case 7:
            writeValueSevenSeg(DIGIT7,decimalPoint);
            break;
        case 8:
            writeValueSevenSeg(DIGIT8,decimalPoint);
            break;
        case 9:
            writeValueSevenSeg(DIGIT9,decimalPoint);
            break;
        case 10: //A
            writeValueSevenSeg(DIGITA,decimalPoint);
            break;
        case 11: //B
            writeValueSevenSeg(DIGITB,decimalPoint);
            break;
        case 12: //C
            writeValueSevenSeg(DIGITC,decimalPoint);
            break;
        case 13: //D
            writeValueSevenSeg(DIGITD,decimalPoint);
            break;
        case 14: //E
            writeValueSevenSeg(DIGITE,decimalPoint);
            break;
        case 15: //F
            writeValueSevenSeg(DIGITF,decimalPoint);
            break;            
    }
}

void testSevenSegment(void)
{
    
    static int counter=0;
    static bool dP = false;
    //Write the seven segment display
    writeDigitSevenSeg(counter,dP);
    if(counter < MAX_DIGITS_SEVEN_SEG)
    {
        counter++;
    }
    else
    {
        counter=0;
        if(dP)
        {
            dP=false;
        }
        else
        {
            dP=true;
        }
    }
}

