#include "TimerlessDelay.h"
#include <xc.h>


void hardDelay_ms(uint16_t d)
{
    uint32_t i;
    for(i=0;i<((uint32_t)d*200000);i++)
    {
         asm ("nop");
        WDTCONbits.WDTCLRKEY=0x5743;
    }
}

void hardDelay_cycles(uint16_t d)
{
    uint32_t i;
    for(i=0;i<((uint32_t)d);i++)
    {
         asm ("nop");
    }
}
