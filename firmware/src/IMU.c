#include "IMU.h"
#include "MillisTimer.h"
#include "sevenSegDigit.h"
#include <stdio.h>
#include "I2CFunctions.h"
#include "../../../../framework/driver/i2c/drv_i2c.h"
#include "helperFunctions.h"

#define NUMBER_SAMPLES_CALIBRATE    100.0
#define CORRECTION_FACTOR_GYRO      1.3//1.0715
#define SENSATIVITY_GYRO            1000
#define SCALING_GYRO                (SENSATIVITY_GYRO/65535.0)*CORRECTION_FACTOR_GYRO

#define CORRECTION_FACTOR_XL        1
#define SENSATIVITY_XL              32
#define SCALING_ACCEL               (SENSATIVITY_XL/65535.0)*CORRECTION_FACTOR_XL


//Variables to hold the values read from the gyro
join_t combineX_gyro, combineY_gyro, combineZ_gyro;

//Variables to hold the values read from the gyro
join_t combineX_accel, combineY_accel, combineZ_accel;

//Offset and deadzone variables gyro
int16_t offsetG_X=0, highG_x=-65535/2, lowG_x=65535/2;
int16_t offsetG_Y=0, highG_y=-65535/2, lowG_y=65535/2;
int16_t offsetG_Z=0, highG_z=-65535/2, lowG_z=65535/2;

//Offset and deadzone variables accel
int16_t offsetXL_X=0, highXL_x=-65535/2, lowXL_x=65535/2;
int16_t offsetXL_Y=0, highXL_y=-65535/2, lowXL_y=65535/2;
int16_t offsetXL_Z=0, highXL_z=-65535/2, lowXL_z=65535/2;

int16_t gyroXDPS, gyroYDPS, gyroZDPS;

unsigned long lastMillis=0;
timers_t IMU_UpdateTimer;
double xAngle,yAngle,zAngle;
double x_XL_raw, y_XL_raw, z_XL_raw;
double x_XL, y_XL, z_XL;

timers_t invertedTimer;
bool wasInverted=false;
bool isInverted(void)
{    
    if(x_XL>0.5 && !wasInverted) 
    {
        resetTimer(&invertedTimer);        
        return false; 
    }
    else if(!wasInverted)
    {
        if(timerDone(&invertedTimer))
        {
            printf("switch inverted");
            wasInverted=true;
            return true;
        }
        return false;
    }
    else if(x_XL<0.5 && wasInverted)
    {
        resetTimer(&invertedTimer);
        return true;
    }
    else if(wasInverted)
    {
        if(timerDone(&invertedTimer))
        {
            printf("switch non-inverted");
            wasInverted=false;
            return false;
        }
        return true;
    }
    else
    {
        return wasInverted;
    }
//    
//    if(x_XL>0)
//    {
//        printf("inverted\r\n");        
//        return false;
//    }
//    else
//    {
//        printf("non-inverted\r\n");
//        return true;
//    }
}

void updateGyro(void)
{
     static bool firstTime=true;
    //First time through
    if(firstTime)
    {
        firstTime=false;
        lastMillis=millis();
    }
    //I2c Is available to use now
    if(getMutex()==false)
    {
        if(timerDone(&IMU_UpdateTimer))
        {
            
            //printf("Gyro\r\n"); 
            if(lastMillis!=millis())
            {
                //Read from the device
                readIMU3AxisG();

                //Process offsets
                gyroXDPS = combineX_gyro.integer - offsetG_X;
                gyroYDPS = combineY_gyro.integer - offsetG_Y;
                gyroZDPS = combineZ_gyro.integer - offsetG_Z;

                //Handle Deadzones
                if(isWithinInt(gyroXDPS,lowG_x*1.2,highG_x*1.2))          gyroXDPS=0; 
                if(isWithinInt(gyroYDPS,lowG_y,highG_y))          gyroYDPS=0; 
                if(isWithinInt(gyroZDPS,lowG_z,highG_z))          gyroZDPS=0;           

                xAngle += (gyroXDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                yAngle += (gyroYDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                zAngle += (gyroZDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                lastMillis = millis();
            }
        }
    }
}

void updateZAxis(void)
{
     static bool firstTime=true;
    //First time through
    if(firstTime)
    {
        firstTime=false;
        lastMillis=millis();
    }
    
    //I2c Is available to use now
    if(getMutex()==false)
    {
        if(timerDone(&IMU_UpdateTimer))
        {
            if(lastMillis!=millis())
            {
                readIMU(Z_AXIS_GYRO);
                gyroZDPS = combineZ_gyro.integer - offsetG_Z;
                //printf("Offsetted value: %d\r\n",gyroRead);
                if(isWithinInt(gyroZDPS,lowG_z,highG_z))
                    gyroZDPS=0;           

                zAngle += (gyroZDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                lastMillis = millis();
            }
        }   
    }
}

void updateXL(void)
{
    
}

void updateIMU(void)
{
    static bool firstTime=true;
    //First time through
    if(firstTime)
    {
        firstTime=false;
        lastMillis=millis();
    }
    //I2c Is available to use now
    if(getMutex()==false)
    {
        if(timerDone(&IMU_UpdateTimer))
        {
            if(lastMillis!=millis())
            {
                //Read from the device
                readIMU6Axis();

                //Process offsets
                gyroXDPS = combineX_gyro.integer - offsetG_X;
                gyroYDPS = combineY_gyro.integer - offsetG_Y;
                gyroZDPS = combineZ_gyro.integer - offsetG_Z;

                //Handle Deadzones
                if(isWithinInt(gyroXDPS,lowG_x*1.2,highG_x*1.2))          gyroXDPS=0; 
                if(isWithinInt(gyroYDPS,lowG_y,highG_y))          gyroYDPS=0; 
                if(isWithinInt(gyroZDPS,lowG_z,highG_z))          gyroZDPS=0;           

                xAngle += (gyroXDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                yAngle += (gyroYDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));
                zAngle += (gyroZDPS*SCALING_GYRO * ((millis()-lastMillis)/1000.0));

                //printf("Raw: %d\n",combineX_accel.integer);
                x_XL_raw = ((double)combineX_accel.integer) * SCALING_ACCEL;
                y_XL_raw = ((double)combineY_accel.integer) * SCALING_ACCEL;
                z_XL_raw = ((double)combineZ_accel.integer) * SCALING_ACCEL;  
                
                x_XL = ((x_XL*63) + x_XL_raw)/64.0;
                y_XL = ((y_XL*63) + y_XL_raw)/64.0;
                z_XL = ((z_XL*63) + z_XL_raw)/64.0;

                //printf("double: %f\n",x_XL);

                lastMillis = millis();
            }
        }
    }
}

void resetXAngle(void)
{
    xAngle = 0;
}

void resetZAngle(void)
{
    zAngle = 0;
}

uint16_t getXDPS(void)
{
    return gyroXDPS;
}

uint16_t getYDPS(void)
{
    return gyroYDPS;
}

uint16_t getZDPS(void)
{
    return gyroZDPS;
}

double getXAxisAngle(void)
{
    return xAngle;
}

double getYAxisAngle(void)
{
    return yAngle;
}

double getZAxisAngle(void)
{
    return zAngle;
}

double getXAxisAccel(void)
{
    return x_XL_raw;
}

double getYAxisAccel(void)
{
    return y_XL_raw;
}

double getZAxisAccel(void)
{
    return z_XL_raw;
}

void zeroIMUAxisGyro(void)
{
    double sumG_X=0,sumG_Y=0,sumG_Z=0;
    int i=0;
    
    //Wait for power on bootup
    for(i=0;i<NUMBER_SAMPLES_CALIBRATE;i++)
    {
        while(!timerDone(&IMU_UpdateTimer));
        //blinkDecimal(100);  
        
    WDTCONbits.WDTCLRKEY=0x5743;
    }
    
    //Calibrate using a number of samples defined above
    for(i=0;i<NUMBER_SAMPLES_CALIBRATE;i++)
    {
        
    WDTCONbits.WDTCLRKEY=0x5743;
        //Wait for 10ms
        while(!timerDone(&IMU_UpdateTimer));
        
        //Read the values
        readIMU3AxisG();
        
        //Sum all values up for an average
        sumG_X+=combineX_gyro.integer; 
        sumG_Y+=combineY_gyro.integer; 
        sumG_Z+=combineZ_gyro.integer; 
        
        //Keep track of the highest values
        if(combineX_gyro.integer>highG_x)      highG_x=combineX_gyro.integer;
        if(combineY_gyro.integer>highG_y)      highG_y=combineY_gyro.integer;
        if(combineZ_gyro.integer>highG_z)      highG_z=combineZ_gyro.integer;
        
        //Keep track of the lowest values
        if(combineX_gyro.integer<lowG_x)       lowG_x=combineX_gyro.integer;
        if(combineY_gyro.integer<lowG_y)       lowG_y=combineY_gyro.integer;
        if(combineZ_gyro.integer<lowG_z)       lowG_z=combineZ_gyro.integer;
        
        blinkDecimal(100);  
    }
    
    //CALIBRATE X
    offsetG_X=(int16_t)(sumG_X/NUMBER_SAMPLES_CALIBRATE);
    highG_x -= offsetG_X;
    lowG_x  -= offsetG_X;
    //printf("TotalX:  %f\n",sumX);
    printf("OffsetX: %d\n",offsetG_X);
    printf("HighX: %d\n",highG_x);
    printf("LowX: %d\r\n",lowG_x);
    
    //CALIBRATE Y
    offsetG_Y=(int16_t)(sumG_Y/NUMBER_SAMPLES_CALIBRATE);
    highG_y -= offsetG_Y;
    lowG_y  -= offsetG_Y;
    //printf("TotalY:  %f\n",sumY);
    printf("OffsetY: %d\n",offsetG_Y);
    printf("HighY: %d\n",highG_y);
    printf("LowY: %d\r\n",lowG_y);
    
    //CALIBRATE Z
    offsetG_Z=(int16_t)(sumG_Z/NUMBER_SAMPLES_CALIBRATE);
    highG_z -= offsetG_Z;
    lowG_z  -= offsetG_Z;
    //printf("TotalZ:  %f\n",sumZ);
    printf("OffsetZ: %d\n",offsetG_Z);
    printf("HighZ: %d\n",highG_z);
    printf("LowZ: %d\r\n",lowG_z);
    
    //Lemme read the damn output 
    for(i=0;i<300;i++)
    {
        while(!timerDone(&IMU_UpdateTimer));
        //blinkDecimal(100);
        WDTCONbits.WDTCLRKEY=0x5743;
    }
}


void zeroIMUAxisAll(void)
{
    double sumG_X=0,sumG_Y=0,sumG_Z=0;
    double sumXL_X=0,sumXL_Y=0,sumXL_Z=0;
    int i=0;
    
    //Wait for power on bootup
    for(i=0;i<NUMBER_SAMPLES_CALIBRATE;i++)
    {
        while(!timerDone(&IMU_UpdateTimer));
        //blinkDecimal(100);  
    }
    
    //Calibrate using a number of samples defined above
    for(i=0;i<NUMBER_SAMPLES_CALIBRATE;i++)
    {
        WDTCONbits.WDTCLRKEY=0x5743;
        //Wait for 10ms
        while(!timerDone(&IMU_UpdateTimer));
        
        //Read the values
        readIMU6Axis();
        
        //Sum all values up for an average GYRO
        sumG_X+=combineX_gyro.integer; 
        sumG_Y+=combineY_gyro.integer; 
        sumG_Z+=combineZ_gyro.integer; 
        
        //Sum all values up for an average XL
        sumXL_X+=combineX_accel.integer; 
        sumXL_Y+=combineY_accel.integer; 
        sumXL_Z+=combineZ_accel.integer; 
        
        //Keep track of the highest values GYRO
        if(combineX_gyro.integer>highG_x)      highG_x=combineX_gyro.integer;
        if(combineY_gyro.integer>highG_y)      highG_y=combineY_gyro.integer;
        if(combineZ_gyro.integer>highG_z)      highG_z=combineZ_gyro.integer;
        
        //Keep track of the highest values XL
        if(combineX_accel.integer>highXL_x)      highXL_x=combineX_accel.integer;
        if(combineY_accel.integer>highXL_y)      highXL_y=combineY_accel.integer;
        if(combineZ_accel.integer>highXL_z)      highXL_z=combineZ_accel.integer;
        
        //Keep track of the lowest values GYRO
        if(combineX_gyro.integer<lowG_x)       lowG_x=combineX_gyro.integer;
        if(combineY_gyro.integer<lowG_y)       lowG_y=combineY_gyro.integer;
        if(combineZ_gyro.integer<lowG_z)       lowG_z=combineZ_gyro.integer;
        
        //Keep track of the lowest values XL
        if(combineX_accel.integer<lowXL_x)       lowXL_x=combineX_accel.integer;
        if(combineY_accel.integer<lowXL_y)       lowXL_y=combineY_accel.integer;
        if(combineZ_accel.integer<lowXL_z)       lowXL_z=combineZ_accel.integer;
        
        blinkDecimal(100);  
    }
    
    //CALIBRATE X GYRO
    offsetG_X=(int16_t)(sumG_X/NUMBER_SAMPLES_CALIBRATE);
    highG_x -= offsetG_X;
    lowG_x  -= offsetG_X;
    //printf("TotalX:  %f\n",sumX);
    printf("OffsetX: %d\n",offsetG_X);
    printf("HighX: %d\n",highG_x);
    printf("LowX: %d\r\n",lowG_x);
    
    //CALIBRATE Y GYRO
    offsetG_Y=(int16_t)(sumG_Y/NUMBER_SAMPLES_CALIBRATE);
    highG_y -= offsetG_Y;
    lowG_y  -= offsetG_Y;
    //printf("TotalY:  %f\n",sumY);
    printf("OffsetY: %d\n",offsetG_Y);
    printf("HighY: %d\n",highG_y);
    printf("LowY: %d\r\n",lowG_y);
    
    //CALIBRATE Z GYRO
    offsetG_Z=(int16_t)(sumG_Z/NUMBER_SAMPLES_CALIBRATE);
    highG_z -= offsetG_Z;
    lowG_z  -= offsetG_Z;
    //printf("TotalZ:  %f\n",sumZ);
    printf("OffsetZ: %d\n",offsetG_Z);
    printf("HighZ: %d\n",highG_z);
    printf("LowZ: %d\r\n",lowG_z);
    
//    //CALIBRATE X XL
//    offsetG_X=(int16_t)(sumG_X/NUMBER_SAMPLES_CALIBRATE);
//    highG_x -= offsetG_X;
//    lowG_x  -= offsetG_X;
//    //printf("TotalX:  %f\n",sumX);
//    printf("OffsetX: %d\n",offsetG_X);
//    printf("HighX: %d\n",highG_x);
//    printf("LowX: %d\r\n",lowG_x);
//    
//    //CALIBRATE Y XL
//    offsetG_Y=(int16_t)(sumG_Y/NUMBER_SAMPLES_CALIBRATE);
//    highG_y -= offsetG_Y;
//    lowG_y  -= offsetG_Y;
//    //printf("TotalY:  %f\n",sumY);
//    printf("OffsetY: %d\n",offsetG_Y);
//    printf("HighY: %d\n",highG_y);
//    printf("LowY: %d\r\n",lowG_y);
//    
//    //CALIBRATE Z XL
//    offsetG_Z=(int16_t)(sumG_Z/NUMBER_SAMPLES_CALIBRATE);
//    highG_z -= offsetG_Z;
//    lowG_z  -= offsetG_Z;
//    //printf("TotalZ:  %f\n",sumZ);
//    printf("OffsetZ: %d\n",offsetG_Z);
//    printf("HighZ: %d\n",highG_z);
//    printf("LowZ: %d\r\n",lowG_z);
    
    //Lemme read the damn output 
//    for(i=0;i<300;i++)
//    {
//        while(!timerDone(&IMU_UpdateTimer));
//        //blinkDecimal(100);
//    }
}

void initIMU(void)
{ 
    //Setup timer
    setTimerInterval(&IMU_UpdateTimer,IMU_UPDATE_TIMER_VALUE);
    setTimerInterval(&invertedTimer,250);
    IMU_STATES_CONFIG_t states = START_BIT_CONFIG;
    bool configIncomplete=true;
    int configStep=0;
    unsigned char controlRegister,dataRegister;
     //Make sure another thread is not using the I2C
    if(getMutex()==false)
    {
        setMutex(true);
        while(configIncomplete)
        {
            switch(configStep)
            {
                case GYRO_REGISTER:        
                    controlRegister     =   CTRL2_G;
                    dataRegister        =   CTRL2_G_SETTING;
                    break;
                case XL_REGISTER:                    
                    controlRegister     =   CTRL1_XL;
                    dataRegister        =   CTRL1_XL_SETTING;
                    break;
            }
            bool writeIncomplete=true;
            while(writeIncomplete)
            {
                //Wait for idle bus
                while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
                switch(states)
                {
                    case START_BIT_CONFIG:                   
                        PLIB_I2C_MasterStart(I2C_ID_1);
                        states++;
                        break;
                    case ADDR_BYTE_CONFIG:            
                        PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_WRITE);
                        //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                        if(WaitTransmitterWriteCompleted())                    
                        {                        
                            states++;                    
                        }                    
                        else                    
                        {        
                            resetI2CSystem();                                        
                            states=START_BIT_CONFIG;                                            
                        }  
                        break;
                    case COMMAND_BYTE_CONFIG:            
                        PLIB_I2C_TransmitterByteSend(I2C_ID_1,controlRegister);
                        //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                        if(WaitTransmitterWriteCompleted())                    
                        {                        
                            states++;                    
                        }                    
                        else                    
                        {        
                            resetI2CSystem();                                        
                            states=START_BIT_CONFIG;                                            
                        }  
                        break;
                    case DATA_BYTE_CONFIG:            
                        PLIB_I2C_TransmitterByteSend(I2C_ID_1,dataRegister); 
                        //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                        if(WaitTransmitterWriteCompleted())                    
                        {                        
                            states++;                    
                        }                    
                        else                    
                        {        
                            resetI2CSystem();                                        
                            states=START_BIT_CONFIG;                                            
                        }  
                        break;
                    case STOP_BIT_CONFIG:
                        PLIB_I2C_MasterStop(I2C_ID_1);
                        states=START_BIT_CONFIG;
                        writeIncomplete=false;
                        break;
                }
            }

            configStep++;
            if(configStep>NUMBER_OF_CONFIG_STEPS)
            {
                configIncomplete=false;
            }
        }
        setMutex(false);
    }
    
    //Calibrate zero for axis
    resetTimer(&IMU_UpdateTimer);
    zeroIMUAxisGyro();
    resetTimer(&IMU_UpdateTimer);
}

void readIMU(int axis)
{
    join_t combineStuff;
    unsigned char registerToRead=OUTX_L_G;
    switch(axis)
    {
        case X_AXIS_GYRO:
            registerToRead=OUTX_L_G;
            break;
        case Y_AXIS_GYRO:
            registerToRead=OUTY_L_G;
            break;
        case Z_AXIS_GYRO:
            registerToRead=OUTZ_L_G;
            break;
    }
    
    static IMU_STATES_READ_t states = START_BIT_READ;
    
    bool writeIncomplete=true;
    if(getMutex()==false)
    {
        setMutex(true);
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT_READ:                   
                    PLIB_I2C_MasterStart(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_WRITE:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_WRITE);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }  
                    break;
                case REGISTER_BYTE_READ:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,registerToRead); 
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }  
                    break;
                case RESTART_BIT:
                    PLIB_I2C_MasterStartRepeat(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_READ:
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_READ);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }  
                    break;
                case CLOCK_BYTE1_READ:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }
                    break;
                case GRAB_BYTE_L_READ:            
                    combineStuff.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    
                    //printf("Low: %d",readStuff.parts[1]);
                    //while(!PLIB_I2C_MasterReceiverReadyToAcknowledge(I2C_ID_1));
                    states++;
                    break;
                case ACK_BYTE_L_READ:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }
                    break;
                case CLOCK_BYTE2_READ:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ;                                            
                    }
                    break;
                case GRAB_BYTE_H_READ:         
                    combineStuff.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    //printf(" High: %d\r\n",readStuff.parts[0]);
                    //while(!PLIB_I2C_MasterReceiverReadyToAcknowledge(I2C_ID_1));
                    states++;                
                    break;
                case NACK_BYTE_H_READ:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,false);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ;                                            
                    }
                    break;
                case STOP_BIT_READ:
                    PLIB_I2C_MasterStop(I2C_ID_1);
                    states=START_BIT_READ;
                    writeIncomplete=false;
                    break;
            }
        }
        setMutex(false);
        //printf("Raw: %d\r\n",readStuff.integer);
        switch(axis)
        {
            case X_AXIS_GYRO:
                combineX_gyro.integer=combineStuff.integer;
                break;
            case Y_AXIS_GYRO:
                combineY_gyro.integer=combineStuff.integer;
                break;
            case Z_AXIS_GYRO:
                combineZ_gyro.integer=combineStuff.integer;
                break;
        }
    }
}


void readIMU3AxisG(void)
{    
    static IMU_STATES_READ_3_t states = START_BIT_READ_3;
    
    bool writeIncomplete=true;
    if(getMutex()==false)
    {
        setMutex(true);
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT_READ_3:                   
                    PLIB_I2C_MasterStart(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_WRITE_3:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_WRITE);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }  
                    break;
                case REGISTER_BYTE_READ_3:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,OUTX_L_G);  
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }  
                    break;
                case RESTART_BIT_3:
                    PLIB_I2C_MasterStartRepeat(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_READ_3:
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_READ);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }  
                    break;
                    
                    
                case CLOCK_BYTE1_X_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_X_L_READ_3:            
                    combineX_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_X_L_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case CLOCK_BYTE2_X_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_X_H_READ_3:         
                    combineX_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_X_H_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break; 
                    
                case CLOCK_BYTE1_Y_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_Y_L_READ_3:            
                    combineY_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Y_L_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case CLOCK_BYTE2_Y_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_Y_H_READ_3:         
                    combineY_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_Y_H_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                    
                case CLOCK_BYTE1_Z_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_Z_L_READ_3:            
                    combineZ_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Z_L_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case CLOCK_BYTE2_Z_READ_3:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                case GRAB_BYTE_Z_H_READ_3:         
                    combineZ_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case NACK_BYTE_Z_H_READ_3:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,false);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_3;                                            
                    }
                    break;
                    
                case STOP_BIT_READ_3:
                    PLIB_I2C_MasterStop(I2C_ID_1);
                    states=START_BIT_READ_3;
                    writeIncomplete=false;
                    break;
            }
        }
        setMutex(false);
    }
}

void readIMU6Axis(void)
{    
    static IMU_STATES_READ_6_t states = START_BIT_READ_6;
    
    bool writeIncomplete=true;
    if(getMutex()==false)
    {
        setMutex(true);
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT_READ_6:                   
                    PLIB_I2C_MasterStart(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_WRITE_6:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_WRITE);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }  
                    break;
                case REGISTER_BYTE_READ_6:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,OUTX_L_G);  
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }  
                    break;
                case RESTART_BIT_6:
                    PLIB_I2C_MasterStartRepeat(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_READ_6:
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,IMU_ADDRESS_READ);
                    //while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    if(WaitTransmitterWriteCompleted())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }               
                    break;
                    
                    
                case CLOCK_BYTE1_X_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_X_G_L_READ_6:            
                    combineX_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_X_G_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_X_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_X_G_H_READ_6:         
                    combineX_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_X_G_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_6;                                            
                    }
                    break; 
                    
                case CLOCK_BYTE1_Y_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Y_G_L_READ_6:            
                    combineY_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Y_G_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {     
                        resetI2CSystem();                                           
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_Y_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Y_G_H_READ_6:         
                    combineY_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_Y_G_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {     
                        resetI2CSystem();                                           
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                    
                case CLOCK_BYTE1_Z_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Z_G_L_READ_6:            
                    combineZ_gyro.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Z_G_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {      
                        resetI2CSystem();                                          
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_Z_G_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Z_G_H_READ_6:         
                    combineZ_gyro.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;  
                case ACK_BYTE_Z_G_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {      
                        resetI2CSystem();                                          
                        states=START_BIT_READ_6;                                            
                    }
                    break; 
                    
                    
                case CLOCK_BYTE1_X_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_X_XL_L_READ_6:            
                    combineX_accel.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_X_XL_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {      
                        resetI2CSystem();                                          
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_X_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_X_XL_H_READ_6:         
                    combineX_accel.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_X_XL_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {       
                        resetI2CSystem();                                         
                        states=START_BIT_READ_6;                                            
                    }
                    break; 
                    
                case CLOCK_BYTE1_Y_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Y_XL_L_READ_6:            
                    combineY_accel.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Y_XL_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {         
                        resetI2CSystem();                                       
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_Y_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Y_XL_H_READ_6:         
                    combineY_accel.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                case ACK_BYTE_Y_XL_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                    
                case CLOCK_BYTE1_Z_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Z_XL_L_READ_6:            
                    combineZ_accel.parts[0]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;
                    break;
                case ACK_BYTE_Z_XL_L_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,true);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {    
                        resetI2CSystem();
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case CLOCK_BYTE2_Z_XL_READ_6:            
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_READ_6;                                            
                    }
                    break;
                case GRAB_BYTE_Z_XL_H_READ_6:         
                    combineZ_accel.parts[1]=PLIB_I2C_ReceivedByteGet(I2C_ID_1); 
                    states++;                
                    break;
                    
                    
                    
                case NACK_BYTE_Z_XL_H_READ_6:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,false);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())
                    {
                        states++;
                    }
                    else
                    {             
                        resetI2CSystem();           
                        states=START_BIT_READ_6;                        
                    }
                    break;
                    
                case STOP_BIT_READ_6:
                    PLIB_I2C_MasterStop(I2C_ID_1);
                    states=START_BIT_READ_6;
                    writeIncomplete=false;
                    break;
            }
        }
        setMutex(false);
    }
}