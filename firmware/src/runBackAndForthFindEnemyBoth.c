#include "runBackAndForthFindEnemyBoth.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"
#include "doTurn.h"

#define DEBUG_PID_UART

//-------------------BACK AND FORTH------------------------
#define FORWARD                         true
#define BACKWARD                        false
#define ENEMY_LOCATION_SENSATIVITY      25
#define BACK_AND_FORTH_SPEED            20
#define MAX_SPEED_BAF                   25
#define MIN_SPEED_BAF                   -25

timers_t debugTHIS;
timers_t decisionTiming, sampleTiming;

int boundMotorCommandsBAF(int val);

void runBackAndForth(void)
{
     //Prep data values
    double angleOffset=0;
    bool direction = FORWARD;
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    
    printf("BAF\r\n");
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(getSensorDistance(FORWARD_SENSOR)>25 && getSensorDistance(2)>25 && (getMacroCommand()!=0))
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
      
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {     
            int _lm;
            int _rm;
            if(direction==FORWARD)
            {
                _lm =  20;
                _rm =  20;
            }
            else
            {
                _lm =  -20;
                _rm =  -20;                
            }
            
            //Using the two 45 degree sensors for wall orientation
            if(getSensorDistance(1)<getSensorDistance(3)-25)
            {
                if(angleOffset<10)
                {
                    angleOffset++;
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
                }
            }
            else if(getSensorDistance(1)>getSensorDistance(3)+25)
            {
                if(angleOffset>-10)
                {
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
                    angleOffset--;
                }
            }
            else
            {          
//                angleOffset=0;
//                setGyroRelativeAngle();
            }
              
            
             if(((getSensorDistance(FORWARD_SENSOR)<75)&&(direction==FORWARD))||((getSensorDistance(BACKWARD_SENSOR)<75)&&(direction==BACKWARD)))
            {
//                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
//                sendData(MDB_L_ADDRESS);  
//                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
//                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                angleOffset=0;
                //Reverse Direction
                direction = !direction;
                //Set the relative angle
                resetXAngle();
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            if(direction==FORWARD)
                printf("Moving forward: %f\r\n",getSensorDistance(FORWARD_SENSOR));
            else                
                printf("Moving backward: %f\r\n",getSensorDistance(BACKWARD_SENSOR));
        }
#endif
        
    }

    completeMacro(); 
}

void runBackAndForthFindEnemy(void)
{
     //Prep data values
    double angleOffset=0;
    double deltaSensor=0, lastSensor = getSensorDistance(6);
    bool direction = FORWARD;
    bool foundEnemy = false;
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    //resetPIDController(angle, KP_TURN, KI_TURN, KD_TURN);
    
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(!foundEnemy && (getMacroCommand()!=0))//&& getSensorDistance(0)>15 && getSensorDistance(2)>15 
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
        
        //Check to see if the change in sensor value is beyond the threshold,
        //meaning we saw a non steady slope (not a wall)
        if(timerDone(&sampleTiming))
        {
            deltaSensor = lastSensor - getSensorDistance(6);
            if(abs(deltaSensor)>ENEMY_LOCATION_SENSATIVITY)
            {
                doTurn(90);
                foundEnemy = true;
            }
        }
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {     
            int _lm;
            int _rm;
            if(direction==FORWARD)
            {
                _lm =  BACK_AND_FORTH_SPEED;
                _rm =  BACK_AND_FORTH_SPEED;
            }
            else
            {
                _lm =  -BACK_AND_FORTH_SPEED;
                _rm =  -BACK_AND_FORTH_SPEED;                
            }
   
            
            //Using the two 45 degree sensors for wall orientation
            if(getSensorDistance(1)<getSensorDistance(3)-25)
            {
                if(angleOffset<10)
                {
                    angleOffset++;
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
                }
            }
            else if(getSensorDistance(1)>getSensorDistance(3)+25)
            {
                if(angleOffset>-10)
                {
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
                    angleOffset--;
                }
            }
            else
            {          
//                angleOffset=0;
//                setGyroRelativeAngle();
            }
               
            
             if(((getSensorDistance(0)<30)&&(direction==FORWARD))||((getSensorDistance(4)<30)&&(direction==BACKWARD)))
            {
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_L_ADDRESS);  
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                angleOffset=0;
                //Reverse Direction
                direction = !direction;
                //Set the relative angle
                resetXAngle();
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
        }
#endif
        
    }

   completeMacro();
}


void runBackAndForthFindEnemyBoth(void)
{
     //Prep data values
    double angleOffset=0;
    
    //SENSOR 2
    double deltaSensor1=0, lastSensor1 = getSensorDistance(6);
    //SENSOR 6
    double deltaSensor2=0, lastSensor2 = getSensorDistance(2);
    
    
    
    bool direction = FORWARD;
    bool foundEnemy = false;
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    //resetPIDController(angle, KP_TURN, KI_TURN, KD_TURN);
    printf("Delta1: %f\r\n", deltaSensor1);
    printf("Delta2: %f\r\n", deltaSensor2);
    printf("Last1: %f\r\n", lastSensor1);
    printf("Last2: %f\r\n", lastSensor2);
    printf("This1: %f\r\n", getSensorDistance(6));
    printf("This2: %f\r\n", getSensorDistance(2));
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(!foundEnemy && (getMacroCommand()!=0))//&& getSensorDistance(0)>15 && getSensorDistance(2)>15 
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
        
        //Check to see if the change in sensor value is beyond the threshold,
        //meaning we saw a non steady slope (not a wall)
        if(timerDone(&sampleTiming))
        {
            deltaSensor1    = lastSensor1 - getSensorDistance(6);
            deltaSensor2    = lastSensor2 - getSensorDistance(2);
            lastSensor1     = getSensorDistance(6);
            lastSensor2    = getSensorDistance(2);
            if(abs(deltaSensor1)>ENEMY_LOCATION_SENSATIVITY)
            {
                
    //printf("Delta1: %f\r\n", deltaSensor1);
                doTurn(95);
//                if(isabout(getSensorDistance(6),lastSensor1,15))
//                {
//                    return true;
//                }
                foundEnemy = true;
            }
            
            if(abs(deltaSensor2)>ENEMY_LOCATION_SENSATIVITY)
            {
                
    //printf("Delta2: %f\r\n", deltaSensor2);
                doTurn(-95);
                foundEnemy = true;
            }
            
              if((getSensorDistance(1)+getSensorDistance(3))< (getSensorDistance(7)+getSensorDistance(5)))
            {
                //Using the two 45 degree sensors for wall orientation
                if(getSensorDistance(1)<getSensorDistance(3)+15)
                {
                    if(angleOffset<10)
                    {
                        angleOffset++;
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
                    }
                }
                else if(getSensorDistance(1)>getSensorDistance(3)-15)
                {
                    if(angleOffset>-10)
                    {
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
                        angleOffset--;
                    }
                }
                else
                {          
    //                angleOffset=0;
    //                setGyroRelativeAngle();
                }
            }
            else
            {
                //Using the two 45 degree sensors for wall orientation
                if(getSensorDistance(5)<getSensorDistance(7)+15)
                {
                    if(angleOffset>-10)
                    {
                        angleOffset++;
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
                    }
                }
                else if(getSensorDistance(5)>getSensorDistance(7)-15)
                {
                    if(angleOffset<10)
                    {
                        setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
                        angleOffset--;
                    }
                }
                else
                {          
    //                angleOffset=0;
    //                setGyroRelativeAngle();
                }
            }
        }
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {     
            int _lm;
            int _rm;
            if(direction==FORWARD)
            {
                _lm =  20;
                _rm =  20;
            }
            else
            {
                _lm =  -20;
                _rm =  -20;                
            }
            
            //Using ONE sensor facing the wall for orientation (BAD)
//            if(getSensorDistance(2)<50)
//            {
//                if(angleOffset<10)
//                {
//                    angleOffset++;
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
//                }
//            }
//            else if(getSensorDistance(2)>100)
//            {
//                if(angleOffset>-10)
//                {
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
//                    angleOffset--;
//                }
//            }
//            else
//            {          
////                angleOffset=0;
////                setGyroRelativeAngle();
//            }
            
          
               
            
             if(((getSensorDistance(0)<65)&&(direction==FORWARD))||((getSensorDistance(4)<65)&&(direction==BACKWARD)))
            {
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_L_ADDRESS);  
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                angleOffset=0;
                //Reverse Direction
                direction = !direction;
                //Set the relative angle
                resetXAngle();
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsBAF(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("Delta1: %f\r\n", deltaSensor1);
            printf("Delta2: %f\r\n", deltaSensor2);
            printf("Last1: %f\r\n", lastSensor1);
            printf("Last2: %f\r\n", lastSensor2);
            printf("This1: %f\r\n", getSensorDistance(6));
            printf("This2: %f\r\n", getSensorDistance(2));
           // printf("This2: %f\r\n", getSensorDistance(2));
            
        }
#endif
        
    }

  
   completeMacro();
}

int boundMotorCommandsBAF(int val)
{
    if(val>MAX_SPEED_BAF)
    {
        val=MAX_SPEED_BAF;
    }
    else if(val<MIN_SPEED_BAF)
    {
        val=MIN_SPEED_BAF;
    }
    return val;
}