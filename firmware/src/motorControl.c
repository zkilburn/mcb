#include "motorControl.h"
#include "IMU.h"
#include "FastTransfer.h"
#include "driveAssistance.h"

#define HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED 0.3
#define HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING 0.2
#define HORIZONTAL_COMPENSATION_BBC_MODE_STOPPED 0.3
#define HORIZONTAL_COMPENSATION_BBC_MODE_DRIVING 0.1
#define VERTICAL_COMPENSATION_NORMAL_MODE   1
#define VERTICAL_COMPENSATION_BBC_MODE   0.3

#define USE_GYRO_FEEDBACK_CORRECTION

bool wasHorizontal=false;
int boundMotorCommands(int command);
int boundWeaponMotorCommands(int command);
int leftSpeed=STOPPED_MOTOR_COMMAND, rightSpeed=STOPPED_MOTOR_COMMAND, weaponSpeed = STOPPED_MOTOR_COMMAND;

bool firstTime=true;
timers_t debugTimer;
//Mix the joystick inputs into a motor speed for left and right
void mixJoysticks(int vert, int horizon)
{
    if(firstTime)
    {
        setTimerInterval(&debugTimer,100);
        firstTime=false;
        setGyroRelativeAngle();
    }
    int lm=0, rm=0;
    switch(getOperationalMode())
    {
        case NORMAL_MODE:
#ifdef USE_GYRO_FEEDBACK_CORRECTION
            if(!isInverted())
            {
                if(vert==0)
                {
                    if(horizon==0)
                    {                        
                        motorGyroFeedbackStraight(&lm,&rm);
                    }
                    else
                    {
                        lm = (int)vert*VERTICAL_COMPENSATION_NORMAL_MODE+horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;
                        rm = (int)vert*VERTICAL_COMPENSATION_NORMAL_MODE-horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;      
                        setGyroRelativeAngle();
                    }
                }
                else
                {                   
                    lm = (int)vert*VERTICAL_COMPENSATION_NORMAL_MODE+(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
                    rm = (int)vert*VERTICAL_COMPENSATION_NORMAL_MODE-(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
                    if(horizon==0)
                    {
                        if(wasHorizontal)
                        {
                            wasHorizontal=false;
                            setGyroRelativeAngle();
                        }
                        motorGyroFeedbackStraight(&lm,&rm);
                    }
                    else
                    {
                        wasHorizontal=true;
                        setGyroRelativeAngle();
                    }

                }
            }
            else
            {
                 if(vert==0)
                {
                    if(horizon==0)
                    {                        
                        motorGyroFeedbackStraight(&lm,&rm);
                    }
                    else
                    {
                        lm = vert*VERTICAL_COMPENSATION_NORMAL_MODE-horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;
                        rm = vert*VERTICAL_COMPENSATION_NORMAL_MODE+horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;      
                        setGyroRelativeAngle();
                    }
                }
                else
                {                   
                    lm = vert*VERTICAL_COMPENSATION_NORMAL_MODE-(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
                    rm = vert*VERTICAL_COMPENSATION_NORMAL_MODE+(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
                    if(horizon==0)
                    {
                        if(wasHorizontal)
                        {
                            wasHorizontal=false;
                            setGyroRelativeAngle();
                        }
                        motorGyroFeedbackStraight(&lm,&rm);
                    }
                    else
                    {
                        wasHorizontal=true;
                        setGyroRelativeAngle();
                    }

                }
            }
#else
            if(vert==0)
            {
                lm = vert*VERTICAL_COMPENSATION_NORMAL_MODE+horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;
                rm = vert*VERTICAL_COMPENSATION_NORMAL_MODE-horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_STOPPED;            
            }
            else
            {                
                lm = vert*VERTICAL_COMPENSATION_NORMAL_MODE+(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
                rm = vert*VERTICAL_COMPENSATION_NORMAL_MODE-(horizon*HORIZONTAL_COMPENSATION_NORMAL_MODE_DRIVING* (255.0-abs(vert))/255.0);
            }
#endif
            break;
        case BABY_CAKES_MODE:
        if(!isInverted())     
        {
            if(vert==0)
            {
                lm = vert*VERTICAL_COMPENSATION_BBC_MODE+horizon*HORIZONTAL_COMPENSATION_BBC_MODE_STOPPED;
                rm = vert*VERTICAL_COMPENSATION_BBC_MODE-horizon*HORIZONTAL_COMPENSATION_BBC_MODE_STOPPED;
            }
            else
            {                
                lm = vert*VERTICAL_COMPENSATION_BBC_MODE+(horizon*HORIZONTAL_COMPENSATION_BBC_MODE_DRIVING* (255.0-abs(vert))/255.0);
                rm = vert*VERTICAL_COMPENSATION_BBC_MODE-(horizon*HORIZONTAL_COMPENSATION_BBC_MODE_DRIVING* (255.0-abs(vert))/255.0);
            }
        }
        else
        {
            if(vert==0)
            {
                lm = vert*VERTICAL_COMPENSATION_BBC_MODE-horizon*HORIZONTAL_COMPENSATION_BBC_MODE_STOPPED;
                rm = vert*VERTICAL_COMPENSATION_BBC_MODE+horizon*HORIZONTAL_COMPENSATION_BBC_MODE_STOPPED;
            }
            else
            {                
                lm = vert*VERTICAL_COMPENSATION_BBC_MODE-(horizon*HORIZONTAL_COMPENSATION_BBC_MODE_DRIVING* (255.0-abs(vert))/255.0);
                rm = vert*VERTICAL_COMPENSATION_BBC_MODE+(horizon*HORIZONTAL_COMPENSATION_BBC_MODE_DRIVING* (255.0-abs(vert))/255.0);
            }
            
        
        }
            break;
    }
//    if(lm!=rm)
//        //if(timerDone(&debugTimer))
//        {
//        printf("v: %3d, h: %3d, lm: %3d, rm: %3d\r\n",vert,horizon,lm,rm);
//        }
    setLeftMotorSpeed(boundMotorCommands(lm));
    setRightMotorSpeed(boundMotorCommands(rm));
}

//Bound the motor commands to the max and min allowable values
int boundMotorCommands(int command)
{
    //Maximums
    if(command>MAX_POS_MOTOR_COMMAND)
    {
        command=MAX_POS_MOTOR_COMMAND;
    }
    if(command<MAX_NEG_MOTOR_COMMAND)
    {
        command=MAX_NEG_MOTOR_COMMAND;
    }
    
    //Minimums
//    if(command!=0)
//    {
//        if(abs(command)<30)
//        {
//            if(command<0)
//            {
//                command=-30;
//            }
//            else
//            {
//                command=30;
//            }
//        }
//    }
    return command;
}

int boundWeaponMotorCommands(int command)
{
      //Maximums
    if(command>MAX_WEAPON_COMMAND)
    {
        command=MAX_WEAPON_COMMAND;
    }
    if(command<MIN_WEAPON_COMMAND)
    {
        command=MIN_WEAPON_COMMAND;
    }
    
    return command;
}

//Set the motor speed variable (for external code usage)
void setRightMotorSpeed(int rm)
{
    if(!isInverted())
        rightSpeed=rm;
    else
        rightSpeed=-rm;
    
}

//Get the motor speed variable (for external code usage)
int getRightMotorSpeed(void)
{
   return rightSpeed; 
}

//Set the motor speed variable (for external code usage)
void setLeftMotorSpeed(int lm)
{
    if(!isInverted())
        leftSpeed=lm;
    else
        leftSpeed=-lm;
}

//Get the motor speed variable (for external code usage)
int getLeftMotorSpeed(void)
{
    return leftSpeed;
}

//Set the motor speed variable (for external code usage)
void setWeaponMotorSpeed(int wm)
{
    weaponSpeed=wm;
}

//Get the motor speed variable (for external code usage)
int getWeaponMotorSpeed(void)
{
    return weaponSpeed;
}
timers_t commDelay;
void stopAllMotors(void)
{
    setTimerInterval(&commDelay,5);
    setLeftMotorSpeed(0);
    setRightMotorSpeed(0); 
    
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_L_ADDRESS);
    while(!timerDone(&commDelay));
    
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_R_ADDRESS);  
    while(!timerDone(&commDelay));
}

void testRightMotorControl(void)
{
    static int ramper=0;
    static bool rampingUp=true;
    setRightMotorSpeed(ramper);
    if(rampingUp && ramper<MAX_POS_MOTOR_COMMAND)
    {
        ramper++;
    }
    else if(rampingUp)
    {
        rampingUp = false;
    }
    else if(!rampingUp && ramper > MAX_NEG_MOTOR_COMMAND)
    {
        ramper--;
    }
    else
    {
        rampingUp=true;
    }
}

void testLeftMotorControl(void)
{
    static int ramper=0;
    static bool rampingUp=true;
    setLeftMotorSpeed(ramper);
    if(rampingUp && ramper<MAX_POS_MOTOR_COMMAND)
    {
        ramper++;
    }
    else if(rampingUp)
    {
        rampingUp = false;
    }
    else if(!rampingUp && ramper > MAX_NEG_MOTOR_COMMAND)
    {
        ramper--;
    }
    else
    {
        rampingUp=true;
    }
}


void testWeaponMotorControl(void)
{
    static int ramper=0;
    static bool rampingUp=true;
    setWeaponMotorSpeed(ramper);
    if(rampingUp && ramper<MAX_WEAPON_COMMAND)
    {
        ramper++;
    }
    else if(rampingUp)
    {
        rampingUp = false;
    }
    else if(!rampingUp && ramper > MIN_WEAPON_COMMAND)
    {
        ramper--;
    }
    else
    {
        rampingUp=true;
    }
}