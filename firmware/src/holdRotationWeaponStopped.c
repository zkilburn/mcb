#include "holdRotationWeaponStopped.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"

timers_t sampleTimingHold, decisionTimingHold;

#define MAX_SPEED_HOLD  30
#define MIN_SPEED_HOLD  -30

int boundMotorCommandsHold(int val);

void setupHoldPosition(void)
{
    setTimerInterval(&sampleTimingHold,5);
    setTimerInterval(&decisionTimingHold,50);
}

void holdPosition(void)
{
      //Do sample processing and input to PID system
        if(timerDone(&sampleTimingHold))
        {
            updatePIDOutput(getXAxisAngle());
        }
        
        //Allow the PID to output a control value
        if(timerDone(&decisionTimingHold))
        {
            //Condition the output from the PID so its appropriate to drive the motors
            float outputToMotor = getPIDOutput();
            int _lm =  -outputToMotor;
            int _rm =  outputToMotor;
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsHold(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsHold(_rm)));
        }   
        
}


int boundMotorCommandsHold(int val)
{
    if(val>MAX_SPEED_HOLD)
    {
        val=MAX_SPEED_HOLD;
    }
    else if(val<MIN_SPEED_HOLD)
    {
        val=MIN_SPEED_HOLD;
    }
    return val;
}