/* 
 * File:   communications.h
 * Author: Zac
 *
 * Created on January 29, 2018, 3:27 PM
 */

#ifndef COMMUNICATIONS_H
#define	COMMUNICATIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
    
#define MCU_ADDRESS             1
#define MDB_L_ADDRESS           42
#define MDB_R_ADDRESS           69
#define WMCB_ADDRESS            3
#define PSB_1_ADDRESS           4
#define PSB_2_ADDRESS           5
#define PSB_3_ADDRESS           6
#define PSB_4_ADDRESS           7
#define PSB_5_ADDRESS           8
#define PSB_6_ADDRESS           9
#define PSB_7_ADDRESS           10
#define PSB_8_ADDRESS           11
#define NUMBER_OF_SLAVE_ADDRESSES     11

    
#define MDB_L_ADDRESS_INDEX         0
#define MDB_R_ADDRESS_INDEX         1
#define WMCB_ADDRESS_INDEX          2
#define PSB_1_ADDRESS_INDEX         3
#define PSB_5_ADDRESS_INDEX         7
    
uint8_t addressList[NUMBER_OF_SLAVE_ADDRESSES] = {
                        MDB_L_ADDRESS,
                        MDB_R_ADDRESS,
                        WMCB_ADDRESS,
                        PSB_1_ADDRESS,  
                        PSB_2_ADDRESS,
                        PSB_3_ADDRESS,
                        PSB_4_ADDRESS,
                        PSB_5_ADDRESS,
                        PSB_6_ADDRESS,
                        PSB_7_ADDRESS,
                        PSB_8_ADDRESS};

#define MAX_RETRY_PER_ADDRESS               0

/* ===========================  Controls for MCB ===========================  */
#define SPEED_SETTING_DATA_ADDRESS          0
/* =========================================================================  */
#define SPEED_SETTING_DATA_WEAPON_ADDRESS   1

/* ===========================  Controls for PSU ===========================  */
#define DISTANCE_DATA_ADDRESS               1
#define SENSOR_MISSED_MEASURE               2
#define SENSOR_GONE_IDLE                    3

void setupCommunications(void);
void updateCommunications(void);    
bool testBoardComms(unsigned char slaveNum);
bool weaponUpToSpeed(void);
int getEnemyLocation(void);
    


#ifdef	__cplusplus
}
#endif

#endif	/* COMMUNICATIONS_H */

