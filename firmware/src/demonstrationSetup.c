/* ************************************************************************** */
/** Demonstration Macro Systems

  @Company
    Zac Kilburn

  @File Name
    demonstrationSetup.c

  @Summary
    Brief description of the file.

  @Description
    Turning and wall approach system for demonstration purposes.
 */
/* ************************************************************************** */

#include "demonstrationSetup.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"

//#define IGNORE_MACRO_KILL

//-------------------WALL APPROACH------------------------
#define MIN_SPEED_APPROACH -17
#define MAX_SPEED_APPROACH 17

#define KP_APPROACH .5
#define KI_APPROACH 0
#define KD_APPROACH 0
//-------------------WALL AVOIDANCE------------------------
#define MIN_SPEED_AVOID -20
#define MAX_SPEED_AVOID 20

#define WALL_AVOID_SPEED                10 
#define MINIMUM_WALL_DISTANCE_DRIVE     25
#define FORWARD_WALL_APPROACH_DIST      100

#define FORWARD_FACING_SENSOR_NUMBER    0
#define LEFT_SIDE_SENSOR_NUMBER         1
#define RIGHT_SIDE_SENSOR_NUMBER        2
#define ADJUST_RIGHT                    5
#define ADJUST_LEFT                     -5
#define DRIVE_STRAIGHT                  0

//-------------------PERPENDICULAR------------------------
#define ANGLE_TO_TURN                   3
#define NOISE_MARGIN                    0.5

//-------------------------------------------------------------

#define DEBUG_PID_UART

timers_t debugTHIS;
timers_t decisionTiming, sampleTiming;

int boundMotorCommandsApproach(int val);
int boundMotorCommandsWallAvoid(int val);

void findPerpendicular(uint8_t sensorToUse)
{
      //Prep data values
    static timers_t updateTimer;
    double previousDistance = getSensorDistance(sensorToUse);
    uint8_t foundMultipleTimes=0;
    //double angleOfCenter,angleCapture;
    //bool senseDistances=true;
    bool foundOneWay = false;
    bool localMinFound =false;
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&updateTimer,50);  
    setTimerInterval(&debugTHIS,100); 
    
    //Reset the angle tracking for IMU correction
    resetXAngle();
    setGyroRelativeAngle();
    
    //Do method until local minimum found or told to stop
    while(!localMinFound && (getMacroCommand()!=0))
    {       
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
        
#endif
        //Do sample processing and input to PID system
        if(timerDone(&updateTimer))
        {
            if(!foundOneWay)
            {
                 setLeftMotorSpeed(6);
                setRightMotorSpeed(-6);
            }
            else
            {
                
                 setLeftMotorSpeed(-6);
                setRightMotorSpeed(6);
            }
//            if(senseDistances)
//            {
                //If the distance has gotten bigger
                if(previousDistance- getSensorDistance(sensorToUse)+NOISE_MARGIN < 0)
                {
                    //Distance is getting bigger
                    if(foundOneWay)
                    {
                        if(foundMultipleTimes>4)
                            localMinFound = true;  
                        else
                        {
                            //angleOfCenter=(getXAxisAngle()+angleCapture)/2;
                            foundMultipleTimes++;
                            foundOneWay=false;
                            previousDistance = getSensorDistance(sensorToUse); 
                        }
                    }
                    else
                    {
                        //angleCapture=getXAxisAngle();
                        previousDistance = getSensorDistance(sensorToUse);  
                        foundOneWay = true;
                    }
                }                  
                else
                {
                   //continue turning
                }              
           if(previousDistance>getSensorDistance(sensorToUse))
            {
                previousDistance = getSensorDistance(sensorToUse);  
            }
                //senseDistances = false;                
                resetTimer(&updateTimer);
//            }
//            else
//            {
//                if(!foundOneWay)
//                    doTurn(ANGLE_TO_TURN);     
//                else
//                    doTurn(-ANGLE_TO_TURN);
//                senseDistances = true;
//                resetTimer(&updateTimer);
//            }
           
        } 
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("Current: %f ,Prev: %f\r\n",
                    getSensorDistance(sensorToUse),
                    previousDistance);
            printf("Macro: %d \r\n", getMacroCommand());
        }
#endif
    }
    
    
    //printf("Perp Done\r\n");
   
    completeMacro();
    //doTurn(getXAxisAngle()-angleOfCenter);
    
}

void driveStraight(void)
{
     //Prep data values
    
    //The critical piece of this algorithm, using a nudge back to center, this 
    //value will shove the robot, IMU will handle variations that this causes.
    int driveAdjust=0;
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);  
    setTimerInterval(&debugTHIS,100); 
    
    //Setup the PID system with tuning and a target
    //resetPIDController(targetDistance, KP_APPROACH, KI_APPROACH, KD_APPROACH);
    
    //Reset the angle tracking for IMU correction
    resetXAngle();
    setGyroRelativeAngle();
    
    //Do method until distance is close to the wall/NON-ZERO or told to stop
    while(((getSensorDistance(FORWARD_FACING_SENSOR_NUMBER)>FORWARD_WALL_APPROACH_DIST)
            ||(getSensorDistance(FORWARD_FACING_SENSOR_NUMBER)==0)) 
            && (getMacroCommand()!=0))
    {       
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
        //Update the RC input to cancel
        updateRCInputs();
        
        //Do sample processing and input to PID system
        if(timerDone(&sampleTiming))
        {
            if(getSensorDistance(LEFT_SIDE_SENSOR_NUMBER) < MINIMUM_WALL_DISTANCE_DRIVE)
            {
                driveAdjust = ADJUST_RIGHT;
            }
            else if(getSensorDistance(RIGHT_SIDE_SENSOR_NUMBER) < MINIMUM_WALL_DISTANCE_DRIVE)
            {
                driveAdjust = ADJUST_LEFT;
            }
            else
            {
                driveAdjust = DRIVE_STRAIGHT;
            }
        }
        
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {
            //Driving straight is the goal
            int _lm = (int) WALL_AVOID_SPEED;
            int _rm = (int) WALL_AVOID_SPEED;
            
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            _lm += driveAdjust;
            _rm -= driveAdjust;
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsWallAvoid(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsWallAvoid(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("SForward: %f ,SL: %f ,SR: %f , lm: %d, rm: %d\n",
                    getSensorDistance(FORWARD_FACING_SENSOR_NUMBER),
                    getSensorDistance(LEFT_SIDE_SENSOR_NUMBER),
                    getSensorDistance(RIGHT_SIDE_SENSOR_NUMBER),
                    getLeftMotorSpeed(),
                    getRightMotorSpeed());
           
            //printf("E: %3f, P: %3f, I: %3f, D: %3f, O: %3f\n", getErrorComponent(),getProportionalComponent(), getIntegralComponent(), getDerivativeComponent(), getPIDOutput());
        }
#endif
    }
    
    //CLEANUP WHEN WE ARE DONE
    setLeftMotorSpeed(0);
    setRightMotorSpeed(0); 
    
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_L_ADDRESS);  
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_R_ADDRESS);  
    
    
}


void doApproach(double targetDistance)
{
     //Prep data values
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,20);
    setTimerInterval(&sampleTiming,2);  
    setTimerInterval(&debugTHIS,100); 
    
    //Setup the PID system with tuning and a target
    resetPIDController(targetDistance, KP_APPROACH, KI_APPROACH, KD_APPROACH);
    
    //Reset the angle tracking for IMU correction
    resetXAngle();
    setGyroRelativeAngle();
    int _rm;
    int _lm;
    //Do method until turn is complete or told to stop
    while(!isAboutDouble(getSensorDistance(0),targetDistance,5) && (getMacroCommand()!=0))
    {               
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
        //Update the RC input to cancel
        updateRCInputs();
        
        //Do sample processing and input to PID system
        if(timerDone(&sampleTiming))
        {
            updatePIDOutput(getSensorDistance(0));
        }
        
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {
            //Condition the output from the PID so its appropriate to drive the motors
            double outputToMotor = getPIDOutput();
            _lm = (int) -outputToMotor;
            _rm = (int) -outputToMotor;
            
           
            //Correct the left and right motor for the gyro error based on percentage throttle
            _lm=boundMotorCommandsApproach(_lm);
            _rm=boundMotorCommandsApproach(_rm);   
            
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommandsApproach(_lm));
            setRightMotorSpeed(boundMotorCommandsApproach(_rm));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("Sensor: %f , lm: %d, rm: %d\r\n",getSensorDistance(0),getLeftMotorSpeed(),getRightMotorSpeed());
            printf("Angle: %f, lraw: %d, rraw: %d\r\r\n",getXAxisAngle(),_lm,_rm);
            //printf("E: %3f, P: %3f, I: %3f, D: %3f, O: %3f\n", getErrorComponent(),getProportionalComponent(), getIntegralComponent(), getDerivativeComponent(), getPIDOutput());
        }
#endif
    }
    
  
    completeMacro();
}

int boundMotorCommandsWallAvoid(int val)
{
    if(val>MAX_SPEED_AVOID)
    {
        val=MAX_SPEED_AVOID;
    }
    else if(val<MIN_SPEED_AVOID)
    {
        val=MIN_SPEED_AVOID;
    }
    return val;
}

int boundMotorCommandsApproach(int val)
{
    if(val>MAX_SPEED_APPROACH)
    {
        val=MAX_SPEED_APPROACH;
    }
    else if(val<MIN_SPEED_APPROACH)
    {
        val=MIN_SPEED_APPROACH;
    }
    return val;
}

