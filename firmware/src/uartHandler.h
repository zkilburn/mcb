/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _UART_HANDLER_H    /* Guard against multiple inclusion */
#define _UART_HANDLER_H


/* ************************************************************************** */
/* ************************************************************************** */
/* Section: Included Files                                                    */
/* ************************************************************************** */
/* ************************************************************************** */

/* This section lists the other files that are included in this file.
 */

/* TODO:  Include other files here if needed. */


/* Provide C++ Compatibility */
#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>
#include "app.h"
    
#define UART_BUFFER_LENGTH  500
    
    typedef struct {
        /* Pointer location of head */
        uint16_t head;
        /* Pointer location of tail */
        uint16_t tail;
        /* Count of how many bytes used*/
        uint16_t count;
        /* Describe structure member. */
        unsigned char buff[UART_BUFFER_LENGTH];

    } uartCircBuff_t;


    void initializeUARTHandles();
    void initUARTBufferSystem(DRV_HANDLE uartHandle0,DRV_HANDLE uartHandle1,DRV_HANDLE uartHandle2,DRV_HANDLE uartHandle3);  
    
    /*=================================USART 0=================================*/
    void writeByteUART0(unsigned char ch);    
    uint16_t rxBytesAvailable0(void);
    unsigned char peekByteUART0(void);
    unsigned char readByteUART0(void);    
    void UART0_TX_CALLBACK(void);
    void UART0_RX_CALLBACK(void);
    
    /*=================================USART 1=================================*/
    void writeByteUART1(unsigned char ch);
    void UART1_TX_CALLBACK(void);
    uint16_t rxBytesAvailable11(void);
    unsigned char peekByteUART1(void);
    unsigned char readByteUART1(void);
    void UART1_RX_CALLBACK(void);
    /*=================================USART 2=================================*/
    uint16_t rxBytesAvailable2(void);
    unsigned char peekByteUART2(void);
    unsigned char readByteUART2(void);
    void UART2_RX_CALLBACK(void);
    void wipeBuffer2(void);
    /*=================================USART 3=================================*/
    void writeByteUART3(unsigned char ch);
    void UART3_TX_CALLBACK(void);


    /* Provide C++ Compatibility */
#ifdef __cplusplus
}
#endif

#endif /* _EXAMPLE_FILE_NAME_H */

/* *****************************************************************************
 End of File
 */
