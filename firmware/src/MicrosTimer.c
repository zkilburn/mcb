/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include "MicrosTimer.h"
#include "../src/system_config/default/system_definitions.h"


unsigned long micros(void)
{
    return PLIB_TMR_Counter32BitGet(TMR_ID_4);
}

bool timerDoneMicros(timers_micros_t * t)
{
    if((abs(micros()-t->lastMicros)/12) > t->timerInterval)
    {
        t->lastMicros=micros();
        return true;
    }
    else
    {
        return false;
    }
}

void setTimerIntervalMicros(timers_micros_t * t, unsigned long interval)
{
    t->timerInterval= interval;
}

void resetTimerMicros(timers_micros_t * t)
{
    t->lastMicros=micros();
}


/* *****************************************************************************
 End of File
 */
