
#ifndef _RUN_BAF_FIND_ENEMY_BOTH_H    /* Guard against multiple inclusion */
#define _RUN_BAF_FIND_ENEMY_BOTH_H

void runBackAndForth(void);
void runBackAndForthFindEnemy(void);
void runBackAndForthFindEnemyBoth(void);



#endif /* _RUN_BAF_FIND_ENEMY_BOTH_H */
