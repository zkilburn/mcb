
#ifndef DEMONSTRATION_SETUP_H    /* Guard against multiple inclusion */
#define DEMONSTRATION_SETUP_H

#include <stdint.h>

void doApproach(double targetDistance);
void findPerpendicular(uint8_t sensorToUse);
void runBackAndForth(void);

#endif /* DEMONSTRATION_SETUP_H */

/******************************************************************************/