/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "I2CFunctions.h"
#include "../../../../framework/driver/i2c/drv_i2c.h"
#include "MillisTimer.h"
#include "MicrosTimer.h"

timers_micros_t timeout;
bool mutexControl = false;

void setupI2CFunctions(void)
{
    setTimerIntervalMicros(&timeout,100);
}

bool WaitReceiverByteAck(void)
{
    resetTimerMicros(&timeout);
    while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1))
    {
        if(timerDoneMicros(&timeout))
        {
            return false;
        }
    }
    return true;
}

bool WaitRxByteAvailable(void)
{    
    resetTimerMicros(&timeout);
    while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1))
    {
        if(timerDoneMicros(&timeout))
        {
            return false;
        }
    }
    return true;
}

bool WaitTransmitterWriteCompleted(void)
{
    resetTimerMicros(&timeout);
    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1))
    {
        if(timerDoneMicros(&timeout))
        {
            return false;
        }
    }
    return true;
}                   

//According to errata, there is a silicon issue
void resetI2CSystem(void)
{    
    PLIB_I2C_Disable(I2C_ID_1);    
    
    //Wait 4 instruction clock cycles by clearing the lines    
     //CLEAR I2C handstate by toggling lines
    TRISDbits.TRISD10 =0;   //SCL
    TRISDbits.TRISD9  =0;   //SDA
    
    LATDbits.LATD9   =0;    //SDA
    LATDbits.LATD10 ^=1;    //SCL
    LATDbits.LATD10 ^=1;    //SCL
    LATDbits.LATD10 ^=1;    //SCL   
    
//    TRISDbits.TRISD9  =1;
//    TRISDbits.TRISD10 =1;

    
    PLIB_I2C_Enable(I2C_ID_1);    
}
//==============================================================================
void I2cStart(void)
{ 
    PLIB_I2C_MasterStart(I2C_ID_1);
}
//==============================================================================
void I2cStop (void)
{
    PLIB_I2C_MasterStop(I2C_ID_1);
}

bool getMutex(void)
{
//    if(mutexControl)
//    {
//        if(timerDone(&mutexTimer))  //Mandatory wait after mutex is used
//        {            
            return mutexControl;
//        }
//    }
//    else
//    {
//        return false;
//    }
}

void setMutex(bool mu)
{
//    if(mu==false)
//    {
//        resetTimer(&mutexTimer);
//    }
    mutexControl=mu;
}
/* *****************************************************************************
 End of File
 */
