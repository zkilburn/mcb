/* 
 * File:   helperFunctions.h
 * Author: Zac
 *
 * Created on February 3, 2018, 1:38 PM
 */

#ifndef HELPERFUNCTIONS_H
#define	HELPERFUNCTIONS_H

#ifdef	__cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <stdint.h>
#include "app.h"
    
    
bool isAboutInt(int value, int referenceValue, int range);
bool isAboutFloat(float value, float referenceValue, float range);
bool isAboutDouble(double value, double referenceValue, double range);
bool isWithinInt(int value, int lowValue, int highValue);
void I2CsweepAddresses(void);


#ifdef	__cplusplus
}
#endif

#endif	/* HELPERFUNCTIONS_H */

