
#ifndef _PID_H    /* Guard against multiple inclusion */
#define _PID_H

void resetPIDController(double _target, double _kp, double _ki, double _kd);
void updatePIDOutput(double sample);
double getPIDOutput(void);
double getProportionalComponent(void);
double getDerivativeComponent(void);
double getIntegralComponent(void);
double getErrorComponent(void);
#endif /* _PID_H */

/* *****************************************************************************
 End of File
 */
