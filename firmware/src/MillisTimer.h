/* 
 * File:   MillisTimer.h
 * Author: Zac
 *
 * Created on January 22, 2018, 9:01 PM
 */

#ifndef MILLISTIMER_H
#define	MILLISTIMER_H

#ifdef	__cplusplus
extern "C" {
#endif
    
#include <stdint.h>
#include "system_definitions.h"
#include "app.h"
    
    typedef struct{
        unsigned long timerInterval;
        unsigned long lastMillis;
    }timers_t;
    
bool timerDone(timers_t * t);
void setTimerInterval(timers_t * t, unsigned long interval);
void resetTimer(timers_t * t);
void globalTimerTracker( );
unsigned long millis(void);
unsigned long micros(void);


#ifdef	__cplusplus
}
#endif

#endif	/* MILLISTIMER_H */

