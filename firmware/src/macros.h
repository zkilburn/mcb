/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _MACROS_H    /* Guard against multiple inclusion */
#define _MACROS_H

#include <stdbool.h>
#include <stdint.h>

#define MACRO_STOP      0
#define TURN            1
#define APPROACH        2
#define PERPENDICULAR   3
#define BAF             4
#define BAF_ENEMY       5
#define WALLS           6
#define AUTONOMOUS      7
#define SPIN_UP         8
#define SPIN_AND_MOVE   9
#define WALLS_ENEMY     10
#define AUTONOMOUS2     11

#define MACRO_LIGHT LATBbits.LATB7
void updateMacros(void);
void setMacroCommand(uint8_t mc);

uint8_t getMacroCommand(void);
bool checkMacroExists(void);


void completeMacro(void);

void checkWDT(void);

#endif /* _MACROS_H */

/* *****************************************************************************
 End of File
 */
