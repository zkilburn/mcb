#include "helperFunctions.h"

bool isAboutInt(int value, int referenceValue, int range)
{
    return ((value+range)>referenceValue) && ((value-range)<referenceValue);
}

bool isAboutFloat(float value, float referenceValue, float range)
{
    return ((value+range)>referenceValue) && ((value-range)<referenceValue);
}
bool isAboutDouble(double value, double referenceValue, double range)
{
    return ((value+range)>referenceValue) && ((value-range)<referenceValue);
}


bool isWithinInt(int value, int lowValue, int highValue)
{
    return ((value>lowValue) && (value<highValue));
}


void I2CsweepAddresses(void)
{
    int state=0,addressToSend=0;
    bool addressesIncomplete=true;
    
    while(addressesIncomplete)
    {
        //Wait for idle bus
        while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
        switch(state)
        {
            case 0:                   
                PLIB_I2C_MasterStart(I2C_ID_1);
                state++;
                break;
            case 1:            
                PLIB_I2C_TransmitterByteSend(I2C_ID_1,addressToSend);
                while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                state++;
                break;
            case 2:
                PLIB_I2C_MasterStop(I2C_ID_1);
                addressToSend++;
                if(addressToSend>=255)
                {
                    addressesIncomplete=false;
                }
                state=0;
                break;
        }        
    }
}
