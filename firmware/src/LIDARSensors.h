/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _LIDAR_SENSORS_H    /* Guard against multiple inclusion */
#define _LIDAR_SENSORS_H

#include <stdint.h>

#define FORWARD_SENSOR                          0
#define FOURTY_FIVE_DEGREE_SENSOR               1
#define NINTY_DEGREE_SENSOR                     2
#define ONE_THIRTY_FIVE_DEGREE_SENSOR           3
#define BACKWARD_SENSOR                         4
#define TWO_TWENTY_FIVE_DEGREE_SENSOR           5
#define TWO_SEVENTY_DEGREE_SENSOR               6
#define THREE_FIFTEEN_DEGREE_SENSOR             7

void setSensorDistance(uint8_t sensorNumber, double sensorValue);
double getSensorDistance(uint8_t sensorNumber);
double getSensorDistanceCenterOfRobot(uint8_t sensorNumber);
double * getSensorDistancesArray(void);
double * getSensorDistancesArrayCenterOfRobot(void);
double * getSensorDistancesSubarray7(uint8_t indexToSkip);

#endif /* _LIDAR_SENSORS_H */

/* *****************************************************************************
 End of File
 */
