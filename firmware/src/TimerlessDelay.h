
#ifndef _TIMERLESS_DELAY_H    /* Guard against multiple inclusion */
#define _TIMERLESS_DELAY_H

#include <stdint.h>

void hardDelay_ms(uint16_t d);
void hardDelay_cycles(uint16_t d);


#endif /* _TIMERLESS_DELAY_H */

