/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: AngleFunction_terminate.c
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 22-Feb-2018 13:33:09
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "AngleFunction.h"
#include "AngleFunction_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void AngleFunction_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for AngleFunction_terminate.c
 *
 * [EOF]
 */
