/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: AngleFunction.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 22-Feb-2018 13:33:09
 */

#ifndef ANGLEFUNCTION_H
#define ANGLEFUNCTION_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "AngleFunction_types.h"

/* Function Declarations */
extern double AngleFunction(const double x1[8]);

#endif

/*
 * File trailer for AngleFunction.h
 *
 * [EOF]
 */
