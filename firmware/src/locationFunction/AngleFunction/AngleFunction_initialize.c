/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: AngleFunction_initialize.c
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 22-Feb-2018 13:33:09
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "AngleFunction.h"
#include "AngleFunction_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void AngleFunction_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for AngleFunction_initialize.c
 *
 * [EOF]
 */
