/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: locationFunction.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:03:34
 */

#ifndef LOCATIONFUNCTION_H
#define LOCATIONFUNCTION_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "locationFunction_types.h"

/* Function Declarations */
extern void locationFunction(const double x1[8], double b_y1[3]);

#endif

/*
 * File trailer for locationFunction.h
 *
 * [EOF]
 */
