/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: locationFunction_initialize.c
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:03:34
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "locationFunction.h"
#include "locationFunction_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void locationFunction_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for locationFunction_initialize.c
 *
 * [EOF]
 */
