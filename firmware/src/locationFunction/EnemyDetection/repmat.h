/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: repmat.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:04:07
 */

#ifndef REPMAT_H
#define REPMAT_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "enemyDetection_types.h"

/* Function Declarations */
extern void b_repmat(double b[250]);
extern void c_repmat(double b[250]);
extern void d_repmat(double b[9]);
extern void repmat(double b[250]);

#endif

/*
 * File trailer for repmat.h
 *
 * [EOF]
 */
