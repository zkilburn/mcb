/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: enemyDetection_initialize.c
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:04:07
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "enemyDetection.h"
#include "enemyDetection_initialize.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void enemyDetection_initialize(void)
{
  rt_InitInfAndNaN(8U);
}

/*
 * File trailer for enemyDetection_initialize.c
 *
 * [EOF]
 */
