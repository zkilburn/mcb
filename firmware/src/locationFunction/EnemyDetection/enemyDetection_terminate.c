/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: enemyDetection_terminate.c
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:04:07
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "enemyDetection.h"
#include "enemyDetection_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void enemyDetection_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for enemyDetection_terminate.c
 *
 * [EOF]
 */
