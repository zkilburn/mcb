/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: enemyDetection_types.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:04:07
 */

#ifndef ENEMYDETECTION_TYPES_H
#define ENEMYDETECTION_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for enemyDetection_types.h
 *
 * [EOF]
 */
