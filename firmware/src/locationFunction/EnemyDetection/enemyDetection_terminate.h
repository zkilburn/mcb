/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: enemyDetection_terminate.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:04:07
 */

#ifndef ENEMYDETECTION_TERMINATE_H
#define ENEMYDETECTION_TERMINATE_H

/* Include Files */
#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "enemyDetection_types.h"

/* Function Declarations */
extern void enemyDetection_terminate(void);

#endif

/*
 * File trailer for enemyDetection_terminate.h
 *
 * [EOF]
 */
