#include <stdio.h>
#include "locationFunction/rt_nonfinite.h"
#include "locationFunction/locationFunction.h"
#include "locationFunction/locationFunction_terminate.h"
#include "locationFunction/locationFunction_initialize.h"

void initLocationSolver(void)
{
    /* Initialize the application.
    You do not need to do this more than one time. */
    locationFunction_initialize();
}
void solveLocation(double * distances, double * locSet)
{
    //printf("B4: %d,", millis());
    locationFunction(distances,  locSet);
    //printf("After: %d \n",millis());
}

void closeLocationSolver(void)
{
    /* Terminate the application.
    You do not need to do this more than one time. */
    locationFunction_terminate();
}