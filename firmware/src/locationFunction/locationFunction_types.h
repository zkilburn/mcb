/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: locationFunction_types.h
 *
 * MATLAB Coder version            : 3.4
 * C/C++ source code generated on  : 27-Apr-2018 01:03:34
 */

#ifndef LOCATIONFUNCTION_TYPES_H
#define LOCATIONFUNCTION_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for locationFunction_types.h
 *
 * [EOF]
 */
