/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SOLVE_LOCATION_H    /* Guard against multiple inclusion */
#define _SOLVE_LOCATION_H

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "locationFunction_types.h"

void initLocationSolver(void);
void solveLocation(double * distances, double * locSet);
void closeLocationSolver(void);

#endif
/* *****************************************************************************
 End of File
 */
