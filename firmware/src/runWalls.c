#include "runWalls.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"
#include "spinWeaponUp.h"
#include "doTurn.h"

#define RUN_WALL_SPEED                  40
#define MAX_OFFSET_SPEED                9
#define FORWARD_WALL_DISTANCE           150

#define TURN_DEGREES_90                 120
#define TURN_DEGREES_45                 50

#define ENEMY_LOCATION_SENSATIVITY      45
#define MAX_SPEED_RUN_WALLS             35
#define MIN_SPEED_RUN_WALLS             -35

#define DEBUG

//#define DEBUG_PID_UART
int _lm =0;
int _rm =0;
timers_t debugTHIS;
timers_t decisionTiming, sampleTiming, correctionTimer;

int boundMotorCommandsRunWalls(int val);

void runWalls(void)
{
    int offsetSpeed=0;
    double deltaSensor=0, lastSensor = getSensorDistance(2);
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    setTimerInterval(&correctionTimer,100);
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    
    spinWeaponUp(15);
    
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(getSensorDistance(0)>40  && (getMacroCommand()!=0))
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
       if(timerDone(&correctionTimer))
        {
//            //Using the two 45 degree sensors for wall orientation
//            if(getSensorDistance(1)<getSensorDistance(3)-15)
//            {
//                if(angleOffset<10)
//                {
//                    angleOffset++;
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
//                }
//            }
//            else if(getSensorDistance(1)>getSensorDistance(3)+15)
//            {
//                if(angleOffset>-10)
//                {
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
//                    angleOffset--;
//                }
//            }
//            else
//            {          
//                angleOffset=0;
//                //setGyroRelativeAngle();
//            }
           
            deltaSensor = lastSensor-getSensorDistance(2);
            lastSensor  = getSensorDistance(2);
            offsetSpeed=deltaSensor*1.25;
            if(offsetSpeed>=MAX_OFFSET_SPEED)
            {
                offsetSpeed=MAX_OFFSET_SPEED;
                
            }
            if(offsetSpeed<=-MAX_OFFSET_SPEED)
            {
                offsetSpeed=-MAX_OFFSET_SPEED;
            }
//           if(deltaSensor>0)
//           {
//               
//                if(angleOffset<5)
//                {
//                    //setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
//                    angleOffset++;
//                    offsetSpeed++;
//                }
//           }
//           else
//           {
//               
//                if(angleOffset>-5)
//                {
//                    angleOffset--;
//                    //setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
//                    offsetSpeed--;
//                }
//            }
         }
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {            
            _lm =  RUN_WALL_SPEED;
            _rm =  RUN_WALL_SPEED;
            
            //Using ONE sensor facing the wall for orientation (BAD)
//            if(getSensorDistance(2)<50)
//            {
//                if(angleOffset<10)
//                {
//                    angleOffset++;
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
//                }
//            }
//            else if(getSensorDistance(2)>100)
//            {
//                if(angleOffset>-10)
//                {
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
//                    angleOffset--;
//                }
//            }
//            else
//            {          
////                angleOffset=0;
////                setGyroRelativeAngle();
//            }
        
       
               
             if(getSensorDistance(0)<FORWARD_WALL_DISTANCE)
            {
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_L_ADDRESS);  
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                offsetSpeed=0;
                //Turn 90 degrees at the corner
                doTurn(90);
                //Set the relative angle
                resetXAngle();
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            //motorGyroFeedbackStraight(&_lm, &_rm);
            if(isInverted())
            {
                if(offsetSpeed<0)
                {
                    _lm-=offsetSpeed;
                }
                else
                {
                    _rm+=offsetSpeed;

                }
            }
            else
            {
               if(offsetSpeed<0)
                {
                    _rm-=offsetSpeed;
                }
                else
                {
                    _lm+=offsetSpeed;

                } 
            }
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsRunWalls(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsRunWalls(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
//            printf("lm: %d, rm: %d\r\n", getLeftMotorSpeed(), getRightMotorSpeed());
//            printf("deltaSensor: %f\r\n", deltaSensor);
//            printf("sensorVal: %f\r\n", getSensorDistance(2));
//            printf("offsetSpeed: %d\r\n\r\n", offsetSpeed);
            printf("forward: %f\r\n", getSensorDistance(0));
        }
#endif
        
    }

    completeMacro();
}

timers_t justTurnedDelay;
void runWallsFindEnemy(uint8_t weaponSpeed)
{
    int offsetSpeed=0;
    bool justTurned=true;
    double deltaSensor=0,       lastSensor = getSensorDistance(2);
    double deltaEnemySensor = 0, lastEnemySensor = getSensorDistance(6);
     //Prep data values
    bool foundEnemy=false;
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,500);
    setTimerInterval(&correctionTimer,100);
    setTimerInterval(&justTurnedDelay,100);
    
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(!foundEnemy && getSensorDistance(0)>40  && (getMacroCommand()!=0))
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
        uint8_t speed = (int)((getChannel6()+255)/5.0);
        if(speed>weaponSpeed)
        {
            if(speed!=getWeaponMotorSpeed())
                setWeaponMotorSpeed(speed);
        }
        else
        {
            if(weaponSpeed!=getWeaponMotorSpeed())
                setWeaponMotorSpeed(weaponSpeed);
        }
#ifdef DEBUG   
        if(timerDone(&debugTHIS))
        {
            printf("Weapon Speed: %d\r\n", getWeaponMotorSpeed());
        }
#endif
        
        if(timerDone(&correctionTimer))
        {
            deltaSensor = lastSensor-getSensorDistance(2);
            lastSensor  = getSensorDistance(2);
            offsetSpeed=deltaSensor*2;
            if(offsetSpeed>=MAX_OFFSET_SPEED)
            {
                offsetSpeed=MAX_OFFSET_SPEED;                
            }
            if(offsetSpeed<=-MAX_OFFSET_SPEED)
            {
                offsetSpeed=-MAX_OFFSET_SPEED;
            }
        }
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {            
            _lm =  RUN_WALL_SPEED;
            _rm =  RUN_WALL_SPEED;
       
            if(justTurned && timerDone(&justTurnedDelay))
            {
                justTurned=false;                
                lastEnemySensor  = getSensorDistance(6);
            }
            else
            {                
                deltaEnemySensor = lastEnemySensor - getSensorDistance(6);
//                if(abs(deltaEnemySensor)>ENEMY_LOCATION_SENSATIVITY && !justTurned)
//                {
//                    doTurn(110);
//                    foundEnemy = true;
//                }
                if(getEnemyLocation()==7)
                {
                    doTurn(TURN_DEGREES_45);
                    foundEnemy=true;
                    return;
                }
                if(getEnemyLocation()==6)
                {
                    doTurn(TURN_DEGREES_90);
                    foundEnemy = true;
                    return;
                    
                }
            }
               
            if(getSensorDistance(0)<FORWARD_WALL_DISTANCE)
            {
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_L_ADDRESS);  
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                offsetSpeed=0;
                //Turn 90 degrees at the corner
                doTurn(90);
                justTurned=true;
                //Set the relative angle
                resetXAngle();
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            //motorGyroFeedbackStraight(&_lm, &_rm);
            if(isInverted())
            {
                if(offsetSpeed>0)
                {
                    _rm+=offsetSpeed;
                }
                else
                {
                    _lm-=offsetSpeed;
                }
            }
            else
            {
               if(offsetSpeed>0)
                {
                    _lm+=offsetSpeed;
                }
                else
                {
                    _rm-=offsetSpeed;
                } 
            }
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsRunWalls(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsRunWalls(_rm)));
        }
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("lm: %d, rm: %d\r\n", getLeftMotorSpeed(), getRightMotorSpeed());
            printf("deltaSensor: %f\r\n", deltaSensor);
            printf("sensorVal: %f\r\n", getSensorDistance(2));
            printf("offsetSpeed: %d\r\n\r\n", offsetSpeed);
        }
#endif
        
    }

    completeMacro();
}

int boundMotorCommandsRunWalls(int val)
{
    if(val>MAX_SPEED_RUN_WALLS)
    {
        val=MAX_SPEED_RUN_WALLS;
    }
    else if(val<MIN_SPEED_RUN_WALLS)
    {
        val=MIN_SPEED_RUN_WALLS;
    }
    return val;
}