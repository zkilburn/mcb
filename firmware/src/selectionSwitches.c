/* ************************************************************************** */
/** Descriptive File Name

  @Company
 Zac Kilburn

  @File Name
    selectionSwitches.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "selectionSwitches.h"
#include "I2CFunctions.h"
typedef enum{
    START_BIT_SS = 0,
    ADDR_BYTE_SS,
    COMMAND_BYTE_SS,
    RESTART_BIT_SS,
    ADDR_BYTE_READ_SS,
    CLOCK_BYTE_READ_SS,
    GRAB_BYTE_READ_SS,                      
    NACK_BYTE_READ_SS,
    STOP_BIT_SS
}SELECTION_SWITCHES_STATES_t;

typedef enum{
    SWITCH1 = 0,
    SWITCH2,
    SWITCH3,
    SWITCH4,
    SWITCH5,
    SWITCH6,
    SWITCH7,
    SWITCH8,
    NUMBER_OF_SWITCHES
}SELECTION_SWITCHES_t;

bool switch1State,switch2State,switch3State,switch4State,switch5State,switch6State,switch7State,switch8State;

uint8_t readSelectionSwitches(void);


void initSelectionSwitches(void)
{
    uint8_t selectionRead = readSelectionSwitches();
    switch1State = selectionRead & (1<<SWITCH1);
    switch2State = selectionRead & (1<<SWITCH2);
    switch3State = selectionRead & (1<<SWITCH3);
    switch4State = selectionRead & (1<<SWITCH4);
    switch5State = selectionRead & (1<<SWITCH5);
    switch6State = selectionRead & (1<<SWITCH6);
    switch7State = selectionRead & (1<<SWITCH7);
    switch8State = selectionRead & (1<<SWITCH8);
}

uint8_t readSelectionSwitches(void)
{
    uint8_t selectionRead=0;
     SELECTION_SWITCHES_STATES_t states=START_BIT_SS;
    bool writeIncomplete=true;
    //Make sure another thread is not using the I2C
    if(getMutex()==false)
    {
        //Take the mutex before use
        setMutex(true);
        while(writeIncomplete)
        {
            //Wait for idle bus
            while (! PLIB_I2C_BusIsIdle ( I2C_ID_1 ) );
            switch(states)
            {
                case START_BIT_SS:
                    I2cStart();
                    states++;
                    break;
                case ADDR_BYTE_SS:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,SELECTION_SWITCHES_ADDR_WRITE);
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    states++;
                    break;
                case COMMAND_BYTE_SS:            
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,SELECTION_SWITCHES_READ_REQUEST);  
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));  
                    states++;
                    break;
                case RESTART_BIT_SS:
                    PLIB_I2C_MasterStartRepeat(I2C_ID_1);
                    states++;
                    break;
                case ADDR_BYTE_READ_SS:
                    PLIB_I2C_TransmitterByteSend(I2C_ID_1,SELECTION_SWITCHES_ADDR_READ);
                    while(!PLIB_I2C_TransmitterByteHasCompleted(I2C_ID_1));
                    states++;                
                    break;
                case CLOCK_BYTE_READ_SS: 
                    
                    PLIB_I2C_MasterReceiverClock1Byte(I2C_ID_1); 
                    //while(!PLIB_I2C_ReceivedByteIsAvailable(I2C_ID_1)); 
                    if(WaitRxByteAvailable())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_SS;                                            
                    }
                    break;
                case GRAB_BYTE_READ_SS:                      
                    selectionRead=PLIB_I2C_ReceivedByteGet(I2C_ID_1);                     
                    states++;
                    break;
                case NACK_BYTE_READ_SS:
                    PLIB_I2C_ReceivedByteAcknowledge(I2C_ID_1,false);
                    //while(!PLIB_I2C_ReceiverByteAcknowledgeHasCompleted(I2C_ID_1));
                    if(WaitReceiverByteAck())                    
                    {                        
                        states++;                    
                    }                    
                    else                    
                    {        
                        resetI2CSystem();                                        
                        states=START_BIT_SS;                                            
                    }
                    break;
                case STOP_BIT_SS:
                    I2cStop();
                    states=START_BIT_SS;
                    writeIncomplete=false;
                    break;
            }
        }
        //Return the mutex when finished
        setMutex(false);
    }
    return selectionRead;
}


/* *****************************************************************************
 End of File
 */
