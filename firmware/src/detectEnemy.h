
#ifndef _DETECT_ENEMY_H    /* Guard against multiple inclusion */
#define _DETECT_ENEMY_H


#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "locationFunction/EnemyDetection/rtwtypes.h"
#include "locationFunction/EnemyDetection/enemyDetection_types.h"
#include "locationFunction/EnemyDetection/rt_nonfinite.h"
#include "locationFunction/EnemyDetection/enemyDetection.h"
#include "locationFunction/EnemyDetection/enemyDetection_terminate.h"
#include "locationFunction/EnemyDetection/enemyDetection_initialize.h"

void initEnemySolver(void);
int detectEnemy(void);
int locateEnemy(double * inputs);
void closeEnemySolver(void);
double getXPos(void);
double getYPos(void);

#endif /* _DETECT_ENEMY_H */
