/* ************************************************************************** */
/** Descriptive File Name

  @Company
 Zac Kilburn

  @File Name
    selectionSwitches.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SELECTION_SWITCHES_H    /* Guard against multiple inclusion */
#define _SELECTION_SWITCHES_H
#include <stdint.h>
#include <stdbool.h>
#include "../../../../framework/driver/i2c/drv_i2c.h"
#define SELECTION_SWITCHES_ADDR_WRITE                (uint8_t) 0x72 // 0b011 1001 << 1
#define SELECTION_SWITCHES_ADDR_READ                 (uint8_t) 0x73 // 0b011 1001 << 1
#define SELECTION_SWITCHES_READ_REQUEST              0x00

#endif /* _SELECTION_SWITCHES_H */

/* *****************************************************************************
 End of File
 */
