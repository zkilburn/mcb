#include "runWalls.h"
#include "helperFunctions.h"
#include "IMU.h"
#include "communications.h"
#include "PID.h"
#include "motorControl.h"
#include "FastTransfer.h"
#include "driveAssistance.h"
#include "macros.h"
#include "RCInputs.h"
#include "LIDARSensors.h"
#include "MillisTimer.h"
#include "doTurn.h"

#define RUN_WALL_SPEED 25

#define MIN_SPEED_FLEE -30
#define MAX_SPEED_FLEE 30

timers_t debugTHIS;
timers_t decisionTiming, sampleTiming;

int boundMotorCommandsFlee(int val);


void runWallsFlee(void)
{
     //Prep data values
    float angleOffset=0;
    
    //Prepare a timer for making control decisions and sampling
    setTimerInterval(&decisionTiming,50);
    setTimerInterval(&sampleTiming,5);
    setTimerInterval(&debugTHIS,100);
    resetXAngle();      //Relative turning (zero before we turn)
    
    setGyroRelativeAngle();
    
    //Do method until forwards sensor is at wall, left sensor is at wall or told to stop
    while(getSensorDistance(0)>25 && getSensorDistance(2)>25 && (getMacroCommand()!=0)) //&& !weaponUpToSpeed())
    {
        //Update the gyroscope values
        updateIMU();
        
        //Update the communications systems (Motors/Sensors)
        updateCommunications();
        
#ifndef IGNORE_MACRO_KILL
        //Update the RC input to cancel
        updateRCInputs();
#endif
      
        //Allow the PID to output a control value
        if(timerDone(&decisionTiming))
        {            
            int _lm =  RUN_WALL_SPEED;
            int _rm =  RUN_WALL_SPEED;
            
            //Using ONE sensor facing the wall for orientation (BAD)
//            if(getSensorDistance(2)<50)
//            {
//                if(angleOffset<10)
//                {
//                    angleOffset++;
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
//                }
//            }
//            else if(getSensorDistance(2)>100)
//            {
//                if(angleOffset>-10)
//                {
//                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
//                    angleOffset--;
//                }
//            }
//            else
//            {          
////                angleOffset=0;
////                setGyroRelativeAngle();
//            }
            
            //Using the two 45 degree sensors for wall orientation
            if(getSensorDistance(1)<getSensorDistance(3)-25)
            {
                if(angleOffset<10)
                {
                    angleOffset++;
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()+1);
                }
            }
            else if(getSensorDistance(1)>getSensorDistance(3)+25)
            {
                if(angleOffset>-10)
                {
                    setGyroRelativeAngleValue(getGyroRelativeAngleValue()-1);
                    angleOffset--;
                }
            }
            else
            {          
//                angleOffset=0;
//                setGyroRelativeAngle();
            }
               
             if(getSensorDistance(0)<75)
            {
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_L_ADDRESS);  
                ToSend(SPEED_SETTING_DATA_ADDRESS,0);
                sendData(MDB_R_ADDRESS);  
                
                //Reset the offset for getting yourself straight
                angleOffset=0;
                //Turn 90 degrees at the corner
                doTurn(90);
                //Set the relative angle
                setGyroRelativeAngle();
            }    
            //Correct the motor values using the IMU
            motorGyroFeedbackStraight(&_lm, &_rm);
            
            //Send to the motor system to be sent by the communications library
            setLeftMotorSpeed(boundMotorCommands(boundMotorCommandsFlee(_lm)));
            setRightMotorSpeed(boundMotorCommands(boundMotorCommandsFlee(_rm)));
        }   
        
#ifdef DEBUG_PID_UART
        if(timerDone(&debugTHIS))
        {
            printf("Doing stuff\r\n");
        }
#endif
        
    }

    //CLEANUP WHEN WE ARE DONE
    setLeftMotorSpeed(0);
    setRightMotorSpeed(0);  
    
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_L_ADDRESS);  
    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
    sendData(MDB_R_ADDRESS);  
}


int boundMotorCommandsFlee(int val)
{
    if(val>MAX_SPEED_FLEE)
    {
        val=MAX_SPEED_FLEE;
    }
    else if(val<MIN_SPEED_FLEE)
    {
        val=MIN_SPEED_FLEE;
    }
    return val;
}