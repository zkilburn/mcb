/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include "simulinkDebugger.h"
#include "printfUART.h"
#include <stdio.h>
#include "uartHandler.h"

unsigned char parameters[12]= {69,69,24,8,2,100,100,100,0,1,1,0};
timers_t debugTimer;
void sendDataSimulink(void)
{
    static bool firstTime=true;
    if(firstTime)
    {
        setTimerInterval(&debugTimer,20);
        firstTime=false;
    }
    
    if(timerDone(&debugTimer))
    {
        int i=0;
        for(i=0;i<12;i++)
        {
            writeByteUART1(parameters[i]); 
        }
    }

    
}

/* *****************************************************************************
 End of File
 */
