/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: findSensorOne_types.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 17-Mar-2018 14:11:30
 */

#ifndef FINDSENSORONE_TYPES_H
#define FINDSENSORONE_TYPES_H

/* Include Files */
#include "rtwtypes.h"
#endif

/*
 * File trailer for findSensorOne_types.h
 *
 * [EOF]
 */
