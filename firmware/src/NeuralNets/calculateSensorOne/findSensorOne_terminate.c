/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: findSensorOne_terminate.c
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 17-Mar-2018 14:11:30
 */

/* Include Files */
#include "rt_nonfinite.h"
#include "findSensorOne.h"
#include "findSensorOne_terminate.h"

/* Function Definitions */

/*
 * Arguments    : void
 * Return Type  : void
 */
void findSensorOne_terminate(void)
{
  /* (no terminate code required) */
}

/*
 * File trailer for findSensorOne_terminate.c
 *
 * [EOF]
 */
