/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */
#include <stdio.h>
#include "rt_nonfinite.h"
#include "findSensorOne.h"
#include "findSensorOne_terminate.h"
#include "findSensorOne_initialize.h"
#include "solveSensorOne.h"


void initSensorOneSolver(void)
{
    /* Initialize the application.
    You do not need to do this more than one time. */
    findSensorOne_initialize();
}
double solveSensorOne(double * distances)
{
//    printf("B4: %d,", millis());
//    
//    printf("After: %d \r\n",millis());
//    printf("Using data %3.0f,%3.0f,%3.0f,%3.0f,%3.0f,%3.0f,%3.0f\r\n",distances[0],distances[1],distances[2],distances[3],distances[4],distances[5],distances[6]);
    return findSensorOne(distances);
}

void closeSensorOneSolver(void)
{
    /* Terminate the application.
    You do not need to do this more than one time. */
    findSensorOne_terminate();
}



/* *****************************************************************************
 End of File
 */
