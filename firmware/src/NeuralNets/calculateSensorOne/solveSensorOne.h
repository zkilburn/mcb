/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.h

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#ifndef _SOLVE_SENSOR_ONE_H    /* Guard against multiple inclusion */
#define _SOLVE_SENSOR_ONE_H

#include <math.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>
#include "rtwtypes.h"
#include "findSensorOne_types.h"

void initSensorOneSolver(void);
double solveSensorOne(double * distances);
void closeSensorOneSolver(void);


#endif /* _SOLVE_SENSOR_ONE_H */

/* *****************************************************************************
 End of File
 */
