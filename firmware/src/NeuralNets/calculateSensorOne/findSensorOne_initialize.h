/*
 * Academic License - for use in teaching, academic research, and meeting
 * course requirements at degree granting institutions only.  Not for
 * government, commercial, or other organizational use.
 * File: findSensorOne_initialize.h
 *
 * MATLAB Coder version            : 4.0
 * C/C++ source code generated on  : 17-Mar-2018 14:11:30
 */

#ifndef FINDSENSORONE_INITIALIZE_H
#define FINDSENSORONE_INITIALIZE_H

/* Include Files */
#include <stddef.h>
#include <stdlib.h>
#include "rtwtypes.h"
#include "findSensorOne_types.h"

/* Function Declarations */
extern void findSensorOne_initialize(void);

#endif

/*
 * File trailer for findSensorOne_initialize.h
 *
 * [EOF]
 */
