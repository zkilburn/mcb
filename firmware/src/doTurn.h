
#ifndef _DO_TURN_H    /* Guard against multiple inclusion */
#define _DO_TURN_H
#include <stdbool.h>
void doTurn(double angle);
bool isAtAngleStable(double target, double currentAngle, double acceptableAngleRange);

#endif /* _DO_TURN_H */
