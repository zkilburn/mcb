
#ifndef _RUN_WALLS_H    /* Guard against multiple inclusion */
#define _RUN_WALLS_H

#include <stdint.h>

void runWalls(void);
void runWallsFindEnemy(uint8_t weaponSpeed);

#endif /* _RUN_WALLS_H */

