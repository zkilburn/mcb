#include "RCInputs.h"
#include "demonstrationSetup.h"
#include "macros.h"
#include "MillisTimer.h"
#include <stdio.h>
#include "helperFunctions.h"
#include "motorControl.h"

//#define PRINT_RAW_INPUT_1
//#define PRINT_RAW_INPUT_2
//#define PRINT_RAW_INPUT_3
//#define PRINT_RAW_INPUT_4
//#define PRINT_RAW_INPUT_5
//#define PRINT_RAW_INPUT_6

uint8_t operationalMode=NORMAL_MODE;

uint16_t IC1CaptureEvents[IC_FIFO_LENGTH],IC3CaptureEvents[IC_FIFO_LENGTH];
DRV_HANDLE IC1Handle, IC3Handle, TIMERHandle;

timers_t RCTimeout;

int channel1Value=-253, channel2Value, channel3Value, channel4Value, channel5Value=-255, channel6Value=-255;
int lastRB11State, lastRB9State, lastRB12State, lastRB13State;

//Manual Input Capture
unsigned int channel2Start, channel2Raw;
unsigned int channel3Start, channel3Raw;
unsigned int channel4Start, channel4Raw;
unsigned int channel6Start, channel6Raw;

bool channel5DigitalRead,channel1DigitalRead;

bool lockSystemOutputs=false;

int processChannel2(void);
int processChannel3(void);
int processChannel4(void);
int processChannel6(void);

timers_t ICTimer;

void initRCInputs(void)
{
}
bool updateRCInputs(void)
{
    static bool firstTime=true;
    if(firstTime)
    {
        setTimerInterval(&RCTimeout,100);
        firstTime=false;        
        setTimerInterval(&ICTimer,3);
    }

    if(timerDone(&ICTimer))
    {
        //printf("Capture\r\n");
        inputCapture1PWMCalculator();
        inputCapture3PWMCalculator();       
        processChannel2();        
        processChannel3();  
        processChannel4();  
        processChannel6();
//        if(isAboutInt(channel1Value,-253,2) 
//                && isAboutInt(channel2Value,0,2) 
//                && isAboutInt(channel3Value,0,2) 
//                && isAboutInt(channel4Value,0,2)
//                && isAboutInt(channel5Value,-255,2)
//                && isAboutInt(channel6Value,-255,2))//isAboutInt(channel1Value,-78,2) && isAboutInt(channel2Value,0,1) && isAboutInt(channel3Value,-254,2) && isAboutInt(channel5Value,0,1))
//        {   
        if(timerDone(&RCTimeout))
        {
            //printf("Controller Off\r\n");
            lockSystemOutputs=true;
            
            channel1Value=-254;
            channel2Value=0;
            channel4Value=0;
            channel5Value=-255;
            channel6Value=-255;
            channel2Raw=0;
            channel4Raw=0;
            channel6Raw=0;
            setLeftMotorSpeed(0);
            setRightMotorSpeed(0);
            setWeaponMotorSpeed(0);
            setMacroCommand(0);
            return false;
        }
        else
        {
            //FLAPS
            if(channel5Value>DIGITAL_HIGH_INDICATION && !channel5DigitalRead)
            {        
                channel5DigitalRead = true;
                //WHEN THE PIN CHANGES TO HIGH
                //writeDigitSevenSeg(1,false);
                setMacroCommand(AUTONOMOUS); 
            }
            else if(channel5Value<DIGITAL_LOW_INDICATION && channel5DigitalRead)
            {
                //WHEN THE PIN CHANGES TO HIGH
                channel5DigitalRead = false;
                //writeDigitSevenSeg(0,false);
                setMacroCommand(MACRO_STOP);
            }
    
            //GEAR
            if(channel1Value>DIGITAL_HIGH_INDICATION && !channel1DigitalRead)
            {        
                //WHEN THE PIN CHANGES TO HIGH
                channel1DigitalRead = true;
                //writeDigitSevenSeg(3,false);        
                setMacroCommand(AUTONOMOUS2); //Turning macro
            }
            else if(channel1Value<DIGITAL_LOW_INDICATION && channel1DigitalRead)
            {        
                //WHEN THE PIN CHANGES TO HIGH
                channel1DigitalRead = false;
                //writeDigitSevenSeg(2,false);            
                setMacroCommand(MACRO_STOP);
            }
            return true;
        }
    }
    else
    {
        return false;
    }
}
//RB14 is IC1 (RC_PWM_CH1) 
int inputCapture1PWMCalculator(void)
{
    int i=0;
    while(!DRV_IC0_BufferIsEmpty())
    {
        IC1CaptureEvents[i]=DRV_IC0_Capture16BitDataRead();
        i++;
    }
    int k=0;
    for(k=1;k<i;k++)
    {
        int difference = (IC1CaptureEvents[k]-IC1CaptureEvents[k-1]);
        //printf("%d\n",difference);
        if(isWithinInt(difference,MIN_LENGTH_CHANNEL1,MAX_LENGTH_CHANNEL1))
        {
            channel1Value=difference;
            
            #ifdef PRINT_RAW_INPUT_1
            printf("Ch1: %d\r\n",channel1Value);
            #endif
            channel1Value-=CHANNEL1_OFFSET;
            channel1Value*=CHANNEL1_SCALE;
            if(isAboutInt(channel1Value,0,CHANNEL1_DEADBAND) )
            {
                channel1Value=0;
            }
            else
            {

            }
            if(channel1Value>255)
            {
                channel1Value=255;
            }
            if(channel1Value<-255)
            {
                channel1Value=-255;
            }
            //printf("Ch1: %d\r\n",channel1Value);
            return channel1Value;
        }
    }
    return channel1Value;
}

//RB10 is IC3(RC_PWM_CH5)    
int inputCapture3PWMCalculator(void)
{
    int i=0;
    while(!DRV_IC1_BufferIsEmpty())
    {
        IC3CaptureEvents[i]=DRV_IC1_Capture16BitDataRead();
        i++;
    }
    int k=0;
    for(k=1;k<i;k++)
    {
        int difference = (IC3CaptureEvents[k]-IC3CaptureEvents[k-1]);
        //printf("%d\n",difference);
        if(isWithinInt(difference,MIN_LENGTH_CHANNEL5,MAX_LENGTH_CHANNEL5))
        {
            channel5Value=difference;
            #ifdef PRINT_RAW_INPUT_5
            printf("Ch5: %d\r\n",channel5Value);
            #endif
            channel5Value-=CHANNEL5_OFFSET;
            channel5Value*=CHANNEL5_SCALE;
            if(isAboutInt(channel5Value,0,CHANNEL5_DEADBAND) )
            {
                channel5Value=0;
            }
            else
            {
              
            }
            if(channel5Value>255)
            {
                channel5Value=255;
            }
            if(channel5Value<-255)
            {
                channel5Value=-255;
            }
            //printf("Ch3: %d\r\n",channel3Value);
            return channel5Value;
        }
    }
    return channel5Value;
}



//24-RB11 is PORTB ChangeNotification (PWM Input)(RC_PWM_CH4)
//22-RB9  is PORTB ChangeNotification (PWM Input)(RC_PWM_CH6) 
//27-RB12 is PORTB ChangeNotification (PWM Input)(RC_PWM_CH3)
//28-RB13 is PORTB ChangeNotification (PWM Input)(RC_PWM_CH2)

void pinChangeNotificationCallback(void)
{
    if(getApplicationState()==APP_STATE_SERVICE_TASKS)
    {       
        //CHANNEL 4
        if(PORTBbits.RB11 != lastRB11State)
        {
            if(PORTBbits.RB11)
            {                
                //WHEN THE PIN CHANGES TO HIGH (Mark Timer Value)
                channel4Start = PLIB_TMR_Counter16BitGet(TMR_ID_2);
            }
            else
            {
                //WHEN THE PIN CHANGES TO LOW (Calculate Timer Duty)
                channel4Raw = PLIB_TMR_Counter16BitGet(TMR_ID_2)-channel4Start;                  
            }        
            lastRB11State = PORTBbits.RB11;
        }
        //CHANNEL 6
        if(PORTBbits.RB9 != lastRB9State)
        {
            if(PORTBbits.RB9)
            {                
                //WHEN THE PIN CHANGES TO HIGH (Mark Timer Value)
                channel6Start = PLIB_TMR_Counter16BitGet(TMR_ID_2);
            }
            else
            {
                //WHEN THE PIN CHANGES TO LOW (Calculate Timer Duty)
                channel6Raw = PLIB_TMR_Counter16BitGet(TMR_ID_2)-channel6Start;                          
            }
            lastRB9State = PORTBbits.RB9;
        }
       
        //CHANNEL 3
        if(PORTBbits.RB12 != lastRB12State)
        {
            if(PORTBbits.RB12)
            {
                //WHEN THE PIN CHANGES TO HIGH (Mark Timer Value)
                channel3Start = PLIB_TMR_Counter16BitGet(TMR_ID_2);
            }
            else
            {
                //WHEN THE PIN CHANGES TO LOW (Calculate Timer Duty)
                channel3Raw = PLIB_TMR_Counter16BitGet(TMR_ID_2)-channel3Start;               
            }
            lastRB12State = PORTBbits.RB12;
        }
        
        //CHANNEL 2
        if(PORTBbits.RB13 != lastRB13State)
        {
            resetTimer(&RCTimeout);
            if(PORTBbits.RB13)
            {
                //WHEN THE PIN CHANGES TO HIGH (Mark Timer Value)
                channel2Start = PLIB_TMR_Counter16BitGet(TMR_ID_2);                
            }
            else
            {
                //WHEN THE PIN CHANGES TO LOW (Calculate Timer Duty)                 
                channel2Raw = PLIB_TMR_Counter16BitGet(TMR_ID_2)-channel2Start;                
            }
            lastRB13State = PORTBbits.RB13;
        }
    }
}

int processChannel2(void)
{   
    printf("Ch2: %d\n",channel2Raw);
    if(isWithinInt(channel2Raw,MIN_LENGTH_CHANNEL2,MAX_LENGTH_CHANNEL2))
    {
        channel2Value=channel2Raw;
        #ifdef PRINT_RAW_INPUT_2
        printf("Ch2: %d\r\n",channel2Raw);
        #endif
        channel2Value-=CHANNEL2_OFFSET;
        channel2Value*=CHANNEL2_SCALE;
        if(isAboutInt(channel2Value,0,CHANNEL2_DEADBAND) )
        {
            channel2Value=0;
        }
        else
        {
            //channel2Value=channel2Raw;
        }
        
        if(channel2Value>255)
        {
            channel2Value=255;
        }
        if(channel2Value<-255)
        {
            channel2Value=-255;
        }
        //printf("Ch2: %d\r\n",channel2Value);
        return channel2Value;
    }
    return channel2Value;
}

int processChannel3(void)
{   
    //printf("Ch3: %d\r\n",channel3Raw);
    if(isWithinInt(channel3Raw,MIN_LENGTH_CHANNEL3,MAX_LENGTH_CHANNEL3))
    {
        channel3Value = channel3Raw;
        #ifdef PRINT_RAW_INPUT_3
        printf("Ch3: %d\r\n",channel3Raw);
        #endif
        channel3Value-=CHANNEL3_OFFSET;
        channel3Value*=CHANNEL3_SCALE;
        if(isAboutInt(channel3Value,0,CHANNEL3_DEADBAND) )
        {
            channel3Value=0;
        }
        else
        {
            
        }
        
        if(channel3Value>255)
        {
            channel3Value=255;
        }
        if(channel3Value<-255)
        {
            channel3Value=-255;
        }
        //printf("Ch3: %d\r\n",channel3Value);
        return channel3Value;
    }
    return channel3Value;
}


int processChannel4(void)
{   
    //printf("Ch4: %d\r\n",channel4Raw);
    if(isWithinInt(channel4Raw,MIN_LENGTH_CHANNEL4,MAX_LENGTH_CHANNEL4))
    {
        channel4Value = channel4Raw;
        #ifdef PRINT_RAW_INPUT_4
        printf("Ch4: %d\r\n",channel4Raw);
        #endif
        channel4Value-=CHANNEL4_OFFSET;
        channel4Value*=CHANNEL4_SCALE;
        if(isAboutInt(channel4Value,0,CHANNEL4_DEADBAND) )
        {
            channel4Value=0;
        }
        else
        {
            
        }
        
        if(channel4Value>255)
        {
            channel4Value=255;
        }
        if(channel4Value<-255)
        {
            channel4Value=-255;
        }
        //printf("Ch4: %d\r\n",channel4Value);
        return channel4Value;
    }
    return channel4Value;
}


int processChannel6(void)
{   
    //printf("Ch6: %d\r\n",channel6Raw);
    if(isWithinInt(channel6Raw,MIN_LENGTH_CHANNEL6,MAX_LENGTH_CHANNEL6))
    {
        channel6Value = channel6Raw;
        #ifdef PRINT_RAW_INPUT_6
        printf("Ch6: %d\r\n",channel6Raw);
        #endif
        channel6Value-=CHANNEL6_OFFSET;
        channel6Value*=CHANNEL6_SCALE;
        if(isAboutInt(channel6Value,0,CHANNEL6_DEADBAND) )
        {
            channel6Value=0;
        }
        else
        {
            
        }
        
        if(channel6Value>255)
        {
            channel6Value=255;
        }
        if(channel6Value<-255)
        {
            channel6Value=-255;
        }
        //printf("Ch6: %d\r\n",channel6Value);
        return channel6Value;
    }
    return channel6Value;
}

uint8_t getOperationalMode(void)
{
    return operationalMode;
}

void setupIC1Handle(DRV_HANDLE handle)
{
    IC1Handle = handle;
}

void setupIC3Handle(DRV_HANDLE handle)
{
    IC3Handle = handle;
}

void setupTimerHandleRC()//DRV_HANDLE handle)
{
    //TIMERHandle = handle;
}

int getChannel1(void)
{
    return channel1Value;
}

int getChannel2(void)
{
    return channel2Value;
}

int getChannel3(void)
{
    return channel3Value;
}

int getChannel4(void)
{
    return channel4Value;
}

int getChannel5(void)
{
    return channel5Value;
}
int getChannel6(void)
{
    return channel6Value;
}