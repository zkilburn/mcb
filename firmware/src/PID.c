#include "PID.h"
#include "MillisTimer.h"

double target;
double kp,ki,kd;
double proportional, derivative, integral;
double errorSum, error, lastError, derror;
double output;
double dt;
unsigned long lastMillisPID;


void resetPIDController(double _target, double _kp, double _ki, double _kd)
{
    //Set a new target
    target=_target;
    
    //Accept the tuning settings
    kp=_kp;
    ki=_ki;
    kd=_kd;
    
    //Wipe state variables
    error       =0;
    derror      =0;
    lastError   =0;
    errorSum    =0;
    proportional=0;
    derivative  =0;
    integral    =0;
    output      =0;
    
    //Record current milliseconds
    lastMillisPID = millis();
}

//Update the state of the PID controller
void updatePIDOutput(double sample)
{
    //Calculate the time between updates (seconds)
    dt              = (millis()-lastMillisPID) / 1000.0;
    
    //Move the error from last time to the old error
    lastError       = error;
    
    //Update the error now
    error           = target - sample;
    
    //Calculate the change in error
    derror          = error - lastError;
    
    //Calculate the sum of the error
    errorSum        += error;
    
    //Update the state control vars
    proportional    = error       * kp;
    derivative      = derror/dt   * kd;
    integral        = errorSum*dt * ki;
    
    //Calculate the output 
    output          = proportional + integral + derivative;
 
    lastMillisPID   = millis();
}

//Used for the PID controller
double getPIDOutput(void)
{
    return output;
}

//------------DEBUGGING GET VARS--------------
double getProportionalComponent(void)
{
    return proportional;
}
double getDerivativeComponent(void)
{
    return derivative;
}
double getIntegralComponent(void)
{
    return integral;
}
double getErrorComponent(void)
{
    return error;
}