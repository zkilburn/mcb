/* ************************************************************************** */
/** Drive Assistance Systems

  @Company
    Zac Kilburn

  @File Name
    driveAssistance.c

  @Summary
    An attempt to provide gyro control feedback to manual control.

  @Description
    The functions will modify the output to the motors based on
   gyroscope feedback and ensure straight driving when the robot
   is not being told to turn.
 
   May also try to have the robot turn with an angular rate
 */
/* ************************************************************************** */

#include "IMU.h"

#define GYRO_COMPENSATION               0.5
#define GYRO_COMPENSATION_AUTO          0.5
#define GYRO_COMPENSATION_ZERO_INPUT    1
#define TURNING_RATE_MAX    5000

float gyroRelativeAngle=0;

void setGyroRelativeAngle(void)
{
    gyroRelativeAngle = getXAxisAngle();
}

void setGyroRelativeAngleValue(float val)
{
    gyroRelativeAngle = val;
}

float getGyroRelativeAngleValue(void)
{
    return gyroRelativeAngle;
}

void motorGyroFeedbackStraightAuto(int *lm, int *rm)
{
    //Calculate the error of the angle versus the relative 0 angle
    float angleError =  getXAxisAngle()-gyroRelativeAngle ;
    
    //Calculate total throttle input
    int totalMag = (*lm+*rm) /2;
    //Convert to a percentage
    float percentageThrottle = totalMag/255.0;
//    if(!isInverted())
//    {
        //Correct the left and right motor for the gyro error based on percentage throttle
        *lm+=(angleError*GYRO_COMPENSATION_AUTO *(1.0-percentageThrottle));
        *rm-=(angleError*GYRO_COMPENSATION_AUTO*(1.0-percentageThrottle)); 
//    }
//    else
//    {
//         //Correct the left and right motor for the gyro error based on percentage throttle
//        *lm-=(angleError*GYRO_COMPENSATION *(1.0-percentageThrottle));
//        *rm+=(angleError*GYRO_COMPENSATION*(1.0-percentageThrottle)); 
//    }
}

void motorGyroFeedbackStraight(int *lm, int *rm)
{
    //Calculate the error of the angle versus the relative 0 angle
    float angleError =  getXAxisAngle()-gyroRelativeAngle;
    int originalL = *lm;
    int originalR = *rm;
    //Calculate total throttle input
    int totalMag = (*lm+*rm) /2;
    //Convert to a percentage
    float percentageThrottle = totalMag/255.0;

    //This attempts to only add a value to a motor, so that moving forward
    //is the priority, while also correcting the angle
    
    if(abs(angleError)>5)
    {
        //if the command is positive
        if(*lm>0 && *rm>0)
        {
            //if the error is positive
            if(angleError<0)
            {                
                *rm-=(int)(angleError*GYRO_COMPENSATION *(1.0-percentageThrottle));
            }
            else
            {            
                *lm+=(int)(angleError*GYRO_COMPENSATION*(1.0-percentageThrottle)); 
            }
        }
        //if the command is negative
        else if(*lm<0 && *rm<0)
        {
            //if the error is positive
            if(angleError>0)
            {                
                *rm-=(int)(angleError*GYRO_COMPENSATION *(1.0-percentageThrottle));
            }
            else
            {            
                *lm+=(int)(angleError*GYRO_COMPENSATION*(1.0-percentageThrottle)); 
            }
        }
        //if the inputs are zero - allows for the robot to stablize when inputs are zero
        else
        {
            //Correct the left and right motor for the gyro error based on percentage throttle        
//            if(abs(angleError)>2)
//            {
//                //Correct the left and right motor for the gyro error based on percentage throttle
//                *lm+=(int)(angleError*(GYRO_COMPENSATION_ZERO_INPUT));
//                *rm-=(int)(angleError*(GYRO_COMPENSATION_ZERO_INPUT)); 
//            }
        }
    }
//    if(originalL>0)
//    {
//        if(*lm<5)
//        {
//            *lm=5;
//        }
//    }
//    else
//    {
//        if(*lm>-5)
//        {
//            *lm=5;
//        }
//    }
//    if(originalR>0)
//    {
//        if(*rm<5)
//        {
//            *rm=5;
//        }
//    }
//    else
//    {
//        if(*rm>-5)
//        {
//            *rm=5;
//        }
//    }
}

void motorGyroFeedbackTurning(int lm, int rm)
{
    int16_t turningRate = getXDPS();
    if(abs(turningRate > TURNING_RATE_MAX))
    {
        if(turningRate > 0)         //POSITIVE TURNING RATE
        {
            if(lm>rm)       //INSTRUCTIONS ARE CW
            {
                
            }
            else            //INSTRUCTIONS ARE CCW
            {
                
            }
        }
        else                        //NEGATIVE TURNING RATE
        {            
            if(lm>rm)       //INSTRUCTIONS ARE CW
            {
                
            }
            else            //INSTRUCTIONS ARE CCW
            {
                
            }
        }
    }    
}

/* *****************************************************************************
 End of File
 */
