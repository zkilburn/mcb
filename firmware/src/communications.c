// *****************************************************************************
// *****************************************************************************
// Communications.c
// -- Handles Communications between MCB and other boards
// *****************************************************************************
// *****************************************************************************


#include "communications.h"
#include "LIDARSensors.h"
#include "FastTransfer.h"
#include "uartHandler.h"
#include "MillisTimer.h"
#include "motorControl.h"
#include "detectEnemy.h"

//#define DISABLE_WMCB
//#define DISABLE_SENSORS
//#define IGNORE_MDB_RESPONSE
//#define DISABLE_MOTORS  
#define NUMBER_OF_SENSORS       8
#define MIN_WEAPON_SPEED        50
#define WEIGHT_OLD              90.0
#define WEIGHT_NEW              10.0

int receiveArray[4];
//timer_t sendTimer;
timers_t resendTimer;
timers_t recievedResendTimer;
timers_t receiveTimer;
timers_t sendMotorCommand,debugSevenTimer;
timers_t detectEnemyTimer;

unsigned char slaveAddress  = 0;
bool receivedResponse       = true;
uint16_t numberOfRetries    = 0;
int enemyLocation=0;

void retrySystem(void);
void populateSlaveDataFT(unsigned char slaveNum);
bool receiveDataFT(unsigned char slaveNum);
void incrementSlaveCounter(void);

bool weaponUpToSpeed(void)
{
//    if(weaponSpeed>MIN_WEAPON_SPEED)
//    {
//        return true;
//    }
//    else
//    {
//        return false;
//    }
    return true;
}

void setupCommunications(void)
{
    setTimerInterval(&debugSevenTimer,100);     //Timer to control debugging display
    setTimerInterval(&resendTimer,30);          //Resend timer during comms dropout
    setTimerInterval(&recievedResendTimer,3);   //How quickly to resend after receiving
    setTimerInterval(&sendMotorCommand,5);      //When open loop comms to MDBs
    setTimerInterval(&detectEnemyTimer,10);      //When open loop comms to MDBs
    begin(receiveArray, sizeof(receiveArray), MCU_ADDRESS, false, writeByteUART0, readByteUART2, rxBytesAvailable2, peekByteUART2);
    //stopAllMotors();
}

int getEnemyLocation(void)
{
    return enemyLocation;
}

void updateCommunications(void)
{
    if(timerDone(&detectEnemyTimer))
    {
        enemyLocation=detectEnemy();
    }
    WDTCONbits.WDTCLRKEY=0x5743;
    if(!receivedResponse)
    {
        //int recPacket=0;
        while(receiveData())
        {
            //printf("Heard packet\r\n");
//            recPacket++;
//            if(recPacket>1)
//            {
//                printf("RecPacket: %d\r\n", recPacket);
//            }
            resetTimer(&recievedResendTimer);
            resetTimer(&resendTimer);
//            if(timerDone(&debugSevenTimer))
//                writeDigitSevenSeg(2,true);
            if(receiveDataFT(slaveAddress))
            {
                //printf("Got it\r\n");
                //break;
            }
        }
//        if(receiveData())
//        {                     
//            //printf("Rec\r\n");
//            resetTimer(&recievedResendTimer);
//            if(timerDone(&debugSevenTimer))
//                writeDigitSevenSeg(2,true);
//            receiveDataFT(slaveAddress);
//        }
//        else
//        {            
////            if(timerDone(&debugSevenTimer))
////                writeDigitSevenSeg(1,true);
//        }
    }
    
   
    
    //If its time to send a request for data/data out
    if(receivedResponse && timerDone(&recievedResendTimer))
    {
        //Send the data
        //wipeBuffer2();
        populateSlaveDataFT(addressList[slaveAddress]);
        sendData(addressList[slaveAddress]);
        
        //printf("sendingOut %d\r\n", slaveAddress);
        //Reset the resend timer for retries
        resetTimer(&resendTimer); 
        
        //writeDigitSevenSeg(slaveAddress,true);
        //Mark that we are waiting for a response
        receivedResponse=false;                

        //Clear the retry counter
        numberOfRetries     = 0;
    }
#ifdef IGNORE_MDB_RESPONSE
//    else if(timerDone(&sendMotorCommand))
//    {
//        static uint8_t motorCounter=0;
//        motorCounter++;
//        if(motorCounter>1)
//        {
//            motorCounter=0;
//        }
//        populateSlaveDataFT(addressList[motorCounter]);
//        sendData(addressList[motorCounter]);  
//    }
#endif
    
    //Retry slaves if not responding
    retrySystem();   
}


bool receiveDataFT(unsigned char slaveNum)
{
    //printf("Response from %d\r\n", addressList[slaveNum]);
    //printf("%d\r\n", receiveArray[0]);
//    if((receiveArray[0]==addressList[slaveNum]))//||(addressList[slaveNum] == MDB_L_ADDRESS)||(addressList[slaveNum] == MDB_R_ADDRESS))
//    {
        switch(receiveArray[0])//addressList[slaveNum])
        {
            case MDB_L_ADDRESS:                       
                //printf("MDB_L\r\n");
                break;
            case MDB_R_ADDRESS:                       
                //printf("MDB_R\r\n");
                break;
            case WMCB_ADDRESS:                
                //printf("WMCB\r\n");
                break;
            case PSB_1_ADDRESS:
                setSensorDistance(0,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(0)*WEIGHT_NEW)/100.0);
                break;
            case PSB_2_ADDRESS:
                setSensorDistance(1,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(1)*WEIGHT_NEW)/100.0);
                break;
            case PSB_3_ADDRESS:
                setSensorDistance(2,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(2)*WEIGHT_NEW)/100.0);
                break;
            case PSB_4_ADDRESS:
                setSensorDistance(3,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(3)*WEIGHT_NEW)/100.0);
                break;
            case PSB_5_ADDRESS:
                setSensorDistance(4,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(4)*WEIGHT_NEW)/100.0);
                break;
            case PSB_6_ADDRESS:
                setSensorDistance(5,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(5)*WEIGHT_NEW)/100.0);
                break;
            case PSB_7_ADDRESS:
                setSensorDistance(6,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(6)*WEIGHT_NEW)/100.0);
                break;
            case PSB_8_ADDRESS:
                setSensorDistance(7,(receiveArray[1]*WEIGHT_OLD + getSensorDistance(7)*WEIGHT_NEW)/100.0);
                break;
        }
     
        //mark that we have heard back
        receivedResponse = true;
        
        //Since we have heard back, lets move onto the next slave        
        incrementSlaveCounter();
        return true;
//    }
//    else
//    {
//        
//        resetTimer(&resendTimer); //TESTING ONLY
//        receivedResponse = false;
//    }
//    return false;
}

void populateSlaveDataFT(unsigned char slaveNum)
{
     switch(slaveNum)
    {
        case MDB_L_ADDRESS:
            ToSend(SPEED_SETTING_DATA_ADDRESS,getLeftMotorSpeed());
            break;
        case MDB_R_ADDRESS:
            ToSend(SPEED_SETTING_DATA_ADDRESS,getRightMotorSpeed());
            break;
         case WMCB_ADDRESS:           
            //printf("WMCB send\r\n");          
            ToSend(SPEED_SETTING_DATA_WEAPON_ADDRESS,getWeaponMotorSpeed());
            break;
        case PSB_1_ADDRESS:
            ToSend(0,0);
            //printf("Sending to PSB\r\n");
            break;
        case PSB_2_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_3_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_4_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_5_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_6_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_7_ADDRESS:
            ToSend(0,0);

            break;
        case PSB_8_ADDRESS:
            ToSend(0,0);

            break;
    }
}

void retrySystem(void)
{
     //If you havent received a response
    if(!receivedResponse )
    {
        //And it has been some time
        if(timerDone(&resendTimer))
        {
            //printf("missed: %d\r\n",addressList[slaveAddress]);
            //while(readByteUART2()!=0);
            //and you havent retried too many times
            if(numberOfRetries < MAX_RETRY_PER_ADDRESS)
            {
                //Send again
                populateSlaveDataFT(addressList[slaveAddress]);
                sendData(addressList[slaveAddress]);   
                numberOfRetries++;
            }
            else    //You tried too much
            {             
                numberOfRetries=0;
                incrementSlaveCounter();
                //Allow the send routine to send data
                receivedResponse = true;   
            }
        }
    }   
}

bool testBoardComms(unsigned char slaveNum)
{
    static bool testReceivedData = true, firstpass=true;
    static timers_t timeout;
        
    //First time through, init timer
    if(firstpass)
    {
        firstpass=false;
        setTimerInterval(&timeout,150);
    }
    
    //If you receive data
    if(receiveData())
    {
        writeDigitSevenSeg(15,true);
        switch(slaveNum)
        {
            case MDB_L_ADDRESS:     
                printf("MDBL\r\n");           
                break;
            case MDB_R_ADDRESS:
                printf("MDBR\r\n");                
                break;
            case WMCB_ADDRESS:
                printf("WMCB\r\n");
                break;
            case PSB_1_ADDRESS:
                setSensorDistance(0,receiveArray[1]);
                break;
            case PSB_2_ADDRESS:
                setSensorDistance(1,receiveArray[1]);
                break;
            case PSB_3_ADDRESS:
                setSensorDistance(2,receiveArray[1]);
                break;
            case PSB_4_ADDRESS:
                setSensorDistance(3,receiveArray[1]);
                break;
            case PSB_5_ADDRESS:
                setSensorDistance(4,receiveArray[1]);
                break;
            case PSB_6_ADDRESS:
                setSensorDistance(5,receiveArray[1]);
                break;
            case PSB_7_ADDRESS:
                setSensorDistance(6,receiveArray[1]);
                break;
            case PSB_8_ADDRESS:
                setSensorDistance(7,receiveArray[1]);
                break;
        }
        
        if(slaveNum>=PSB_1_ADDRESS && slaveNum <= PSB_8_ADDRESS)
        {
            //printf("%d\r\n",receiveArray[0]);
//            printf("Sensor #%d: ",receiveArray[0]);
//            int i;
//            for(i=0;i<8;i++)
//            {
//                printf("%3.3f",getSensorDistance(i));
//                printf(" ");
//            }
//            printf("\r\r\n");
        }
        
        testReceivedData=true;
        resetTimer(&timeout);
        return true;
    }
    
    
    if(testReceivedData)
    {
        switch(slaveNum)
        {
            case MDB_L_ADDRESS: 
                //testLeftMotorControl();
                ToSend(SPEED_SETTING_DATA_ADDRESS,getLeftMotorSpeed());
                //printf("Sending left motor command: %d to MDB\r\n", getLeftMotorSpeed());
                break;
            case MDB_R_ADDRESS:
                //testRightMotorControl();
                ToSend(SPEED_SETTING_DATA_ADDRESS,getRightMotorSpeed());
                //printf("Sending right motor command: %d to MDB\r\n", getRightMotorSpeed());
                break;
             case WMCB_ADDRESS:                 
                testWeaponMotorControl();
                ToSend(1,getWeaponMotorSpeed());
                printf("Sending weapon motor command: %d to WMCB\r\n",getWeaponMotorSpeed());
                break;
            case PSB_1_ADDRESS:
            case PSB_2_ADDRESS:
            case PSB_3_ADDRESS:
            case PSB_4_ADDRESS:
            case PSB_5_ADDRESS:
            case PSB_6_ADDRESS:
            case PSB_7_ADDRESS:
            case PSB_8_ADDRESS:
                ToSend(0,1);                
                //printf("Sending PSB %d data\r\n", slaveNum);
                break;
        }
        sendData(slaveNum);
        testReceivedData=false;
        resetTimer(&timeout);
        return false;
    }
    if(!testReceivedData && timerDone(&timeout))
    {
        printf("%d\r\n",slaveNum);
        testReceivedData=true;
        return true;
    }
    return false;
}

void incrementSlaveCounter(void)
{
       //Move on
                slaveAddress++;
                
            //IF WE SKIP WMCB
            #ifdef DISABLE_MOTORS
                if(slaveAddress==MDB_L_ADDRESS)
                {
                    slaveAddress+=2; 
                }

            #endif
            #if defined(DISABLE_WMCB)
                if(slaveAddress==WMCB_ADDRESS_INDEX)
                {
                    slaveAddress++; 
                }
            #endif

                
            //Rollover conditions
            #if defined( DISABLE_SENSORS) && defined(DISABLE_WMCB)
                //If sensors and weapon disabled, talk to just the MDBs
                if(slaveAddress>MDB_R_ADDRESS_INDEX)
                { 
            #elif defined (DISABLE_SENSORS)
                 //If we are talking to slaves, increment until we reach the last slave
                if(slaveAddress>(WMCB_ADDRESS_INDEX))
                {
            #else
                if(slaveAddress>(2+NUMBER_OF_SENSORS))
                {
            #endif
                    
            //Restart conditions
            #if defined(IGNORE_MDB_RESPONSE) && defined(DISABLE_WMCB)
                  slaveAddress=PSB_1_ADDRESS_INDEX;//3 is currently using only the first sensor board ()
                }
            #elif defined(IGNORE_MDB_RESPONSE)

                  slaveAddress=WMCB_ADDRESS_INDEX;//3 is currently using only the first sensor board ()
                }
                
            #else   
                    slaveAddress=0;//3 is currently using only the first sensor board ()
                }
            #endif
}
/*******************************************************************************
 End of File
 */