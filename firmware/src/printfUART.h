/* 
 * File:   printfUART.h
 * Author: Zac
 *
 * Created on January 25, 2018, 6:39 PM
 */

#ifndef PRINTFUART_H
#define	PRINTFUART_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "app.h"

void setupPrintf(DRV_HANDLE uartHandle);  
void writeChar(unsigned char c);
unsigned char peekChar(void);
unsigned char readChar(void);
int charAvailable(void);

#ifdef	__cplusplus
}
#endif

#endif	/* PRINTFUART_H */

