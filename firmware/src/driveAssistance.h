/* ************************************************************************** */
/** Drive Assistance Systems

  @Company
    Zac Kilburn

  @File Name
    driveAssistance.h

  @Summary
    An attempt to provide gyro control feedback to manual control.

  @Description
    The functions will modify the output to the motors based on
   gyroscope feedback and ensure straight driving when the robot
   is not being told to turn.
 
   May also try to have the robot turn with an angular rate
 */
/* ************************************************************************** */

#ifndef _DRIVE_ASSISTANCE_H    /* Guard against multiple inclusion */
#define _DRIVE_ASSISTANCE_H


void setGyroRelativeAngle(void);
void motorGyroFeedbackStraight(int *lm, int *rm);
void motorGyroFeedbackTurning(int lm, int rm);

void setGyroRelativeAngleValue(float val);
float getGyroRelativeAngleValue(void);

#endif /* _DRIVE_ASSISTANCE_H */

/* *****************************************************************************
 End of File
 */
