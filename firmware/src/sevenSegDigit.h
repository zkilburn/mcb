/* 
 * File:   sevenSegDigit.h
 * Author: Zac
 *
 * Created on January 22, 2018, 8:33 PM
 */

#ifndef SEVENSEGDIGIT_H
#define	SEVENSEGDIGIT_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "app.h"
#include "system_definitions.h"
#include <stdbool.h>
    
#define LED_DIGIT_ADDR_WRITE                (uint8_t) 0x70 // 0b011 1000
#define LED_DIGIT_WRITE_CONFIG              0x03
#define LED_DIGIT_WRITE_DATA                0x01
#define LED_DIGIT_ENABLE_OUTPUT             0x00
    
#define SEG_A                               0b11011111
#define SEG_B                               0b11101111
#define SEG_C                               0b11111011
#define SEG_D                               0b11111101
#define SEG_E                               0b11111110
#define SEG_F                               0b10111111
#define SEG_G                               0b01111111
#define SEG_DP                              0b11110111
    
#define DIGIT0                              (SEG_A & SEG_B & SEG_C & SEG_D & SEG_E & SEG_F) 
#define DIGIT1                              SEG_B & SEG_C
#define DIGIT2                              SEG_A & SEG_B & SEG_G & SEG_E & SEG_D
#define DIGIT3                              SEG_A & SEG_B & SEG_G & SEG_C & SEG_D
#define DIGIT4                              SEG_F & SEG_G & SEG_B & SEG_C
#define DIGIT5                              SEG_A & SEG_F & SEG_G & SEG_C & SEG_D
#define DIGIT6                              SEG_A & SEG_F & SEG_G & SEG_C & SEG_D & SEG_E
#define DIGIT7                              SEG_A & SEG_B & SEG_C
#define DIGIT8                              SEG_A & SEG_B & SEG_C & SEG_D & SEG_E & SEG_F & SEG_G
#define DIGIT9                              SEG_A & SEG_B & SEG_C & SEG_D & SEG_F & SEG_G
#define DIGITA                              SEG_A & SEG_B & SEG_C & SEG_E & SEG_F & SEG_G
#define DIGITB                              SEG_C & SEG_D & SEG_E & SEG_F & SEG_G
#define DIGITC                              SEG_A & SEG_D & SEG_E & SEG_F 
#define DIGITD                              SEG_B & SEG_C & SEG_D & SEG_E & SEG_G
#define DIGITE                              SEG_A & SEG_D & SEG_E & SEG_F & SEG_G
#define DIGITF                              SEG_A & SEG_E & SEG_F & SEG_G

    
#define MAX_DIGITS_SEVEN_SEG                15
    
void initSevenSeg(void);
void initDigit(void);
void writeDigitSevenSeg(char num, bool decimalPoint);
void writeDigitSevenSegNoDec(char num);
void blinkDecimal(unsigned long ms);
void decimalBlink(void);
void testSevenSegment(void);

#ifdef	__cplusplus
}
#endif

#endif	/* SEVENSEGDIGIT_H */

