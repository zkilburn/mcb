/* ************************************************************************** */
/** Descriptive File Name

  @Company
    Company Name

  @File Name
    filename.c

  @Summary
    Brief description of the file.

  @Description
    Describe the purpose of this file.
 */
/* ************************************************************************** */

#include "macros.h"
#include "demonstrationSetup.h"
#include "AutonomousMode.h"
#include "driveAssistance.h"
#include "IMU.h"
#include "runWalls.h"
#include "MillisTimer.h"
#include "runBackAndForthFindEnemyBoth.h"
#include "spinWeaponUp.h"
#include "FastTransfer.h"
#include "motorControl.h"
#include "doTurn.h"


uint8_t macroCommand=0;

void updateMacros(void)
{
    if(macroCommand!=0)
    {
        MACRO_LIGHT = 1;
        switch(macroCommand)
        {            
            case TURN:
               // writeDigitSevenSeg(1,false);
                doTurn(180);
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;
                break;
            case APPROACH:
                //writeDigitSevenSeg(1,false);
                doApproach(50);
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;
                break;
            case PERPENDICULAR:
                 //writeDigitSevenSeg(1,false);
                findPerpendicular(2);
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;
                break;
            case WALLS:
                 //writeDigitSevenSeg(1,false);
                runWalls();
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;
                
                break;
            case BAF:
                 //writeDigitSevenSeg(1,false);
                runBackAndForth();
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;
                
                break;
            case BAF_ENEMY:
                 //writeDigitSevenSeg(1,false);
                runBackAndForthFindEnemyBoth();
                //writeDigitSevenSeg(0,false);  
                macroCommand=0;                
                break;
            case AUTONOMOUS:
                runAutonomous();                
                macroCommand=0;
                break;
            case AUTONOMOUS2:
                runAutonomous2();                
                macroCommand=0;
                break;
            case SPIN_UP:                
                //writeDigitSevenSeg(1,false);
                spinWeaponUp(25);
                //writeDigitSevenSeg(0,false);
                macroCommand=0;
                break;
            case SPIN_AND_MOVE:
                //writeDigitSevenSeg(1,false);
                testMovementSpinning();
                //writeDigitSevenSeg(0,false);
                macroCommand=0;
                break;
            case WALLS_ENEMY:
                //writeDigitSevenSeg(1,false);
                runWallsFindEnemy(25);
                //writeDigitSevenSeg(0,false);
                macroCommand=0;
                
                break;
        }
        
        
        MACRO_LIGHT = 0;
    }
}

void setMacroCommand(uint8_t mc)
{
    macroCommand = mc;
}

uint8_t getMacroCommand(void)
{
    return macroCommand;
}

bool checkMacroExists(void)
{
    return macroCommand!=0;
}

void completeMacro(void)
{
    resetXAngle();
    setGyroRelativeAngle();
      //CLEANUP WHEN WE ARE DONE
    setLeftMotorSpeed(0);
    setRightMotorSpeed(0); 
    //setWeaponMotorSpeed(0);
    
    
//    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
//    sendData(MDB_L_ADDRESS);  
//    ToSend(SPEED_SETTING_DATA_ADDRESS,0);
//    sendData(MDB_R_ADDRESS);  
    
}

void checkWDT(void)
{
    
    WDTCONbits.WDTCLRKEY=0x5743;
}

/* *****************************************************************************
 End of File
 */
