/* 
 * File:   motorControl.h
 * Author: Zac
 *
 * Created on February 3, 2018, 5:22 PM
 */

#ifndef MOTORCONTROL_H
#define	MOTORCONTROL_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "app.h"
    
    
#define MAX_POS_MOTOR_COMMAND       255
#define STOPPED_MOTOR_COMMAND       0
#define MAX_NEG_MOTOR_COMMAND       -255
    
#define MAX_WEAPON_COMMAND       100
#define MIN_WEAPON_COMMAND       0
    
void mixJoysticks(int vert, int horizon);
int boundMotorCommands(int command);

void stopAllMotors(void);

void setLeftMotorSpeed(int lm);
int getLeftMotorSpeed(void);
void testLeftMotorControl(void);

void setRightMotorSpeed(int rm);
int getRightMotorSpeed(void);
void testRightMotorControl(void);

void setWeaponMotorSpeed(int wm);
int getWeaponMotorSpeed(void);
void testWeaponMotorControl(void);


#ifdef	__cplusplus
}
#endif

#endif	/* MOTORCONTROL_H */

