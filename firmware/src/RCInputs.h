/* 
 * File:   RCInputs.h
 * Author: Zac
 *
 * Created on February 2, 2018, 4:22 PM
 */

#ifndef RCINPUTS_H
#define	RCINPUTS_H

#ifdef	__cplusplus
extern "C" {
#endif
  
#include "app.h"
#include <stdint.h>

//UNCOMMENT THIS TO SEE A RAW IC OUTPUT ON THE UART
//#define PRINT_RAW_INPUT
    
#define DIGITAL_HIGH_INDICATION 150
#define DIGITAL_LOW_INDICATION  -150
    
#define NORMAL_MODE         0
#define BABY_CAKES_MODE     1
    
#define IC_FIFO_LENGTH 4
//------------CHANNEL 1 SETTINGS-------------
#define MIN_LENGTH_CHANNEL1 3250 //3450
#define MAX_LENGTH_CHANNEL1 6100 //6400
    
#define CHANNEL1_DEADBAND   20
#define CHANNEL1_OFFSET     4669    //4990
#define CHANNEL1_SCALE      0.193    //0.22    
//------------CHANNEL 2 SETTINGS-------------
#define MIN_LENGTH_CHANNEL2 2700 //3300
#define MAX_LENGTH_CHANNEL2 6650 //6100
    
#define CHANNEL2_DEADBAND   20
#define CHANNEL2_OFFSET     4693    //4720
#define CHANNEL2_SCALE      0.136    //0.193    
//------------CHANNEL 3 SETTINGS-------------
#define MIN_LENGTH_CHANNEL3 3250  //2800
#define MAX_LENGTH_CHANNEL3 6100  //6100
    
#define CHANNEL3_DEADBAND   20
#define CHANNEL3_OFFSET     4652     //4736
#define CHANNEL3_SCALE      0.193     //0.1619    
//------------CHANNEL 4 SETTINGS-------------
#define MIN_LENGTH_CHANNEL4 2700
#define MAX_LENGTH_CHANNEL4 6650
    
#define CHANNEL4_DEADBAND   20
#define CHANNEL4_OFFSET     4680
#define CHANNEL4_SCALE      0.136   
//------------CHANNEL 5 SETTINGS-------------
#define MIN_LENGTH_CHANNEL5 3250
#define MAX_LENGTH_CHANNEL5 6100
    
#define CHANNEL5_DEADBAND   20
#define CHANNEL5_OFFSET     4670      //4736
#define CHANNEL5_SCALE      0.194      //0.25    
//------------CHANNEL 6 SETTINGS-------------
#define MIN_LENGTH_CHANNEL6 3250
#define MAX_LENGTH_CHANNEL6 6100
    
#define CHANNEL6_DEADBAND   100
#define CHANNEL6_OFFSET     4692      //4736
#define CHANNEL6_SCALE      0.194
    
    //Returns true if valid RC signals are detected, false if control seems off
bool updateRCInputs(void);

//RB14 is IC1 (RC_PWM_CH1) 
void setupIC1Handle(DRV_HANDLE handle);
int inputCapture1PWMCalculator(void);
//RB13 (RC_PWM_CH2)
//RB12 (RC_PWM_CH3)
//RB11 is PORTB ChangeNotification (Digital Input)(RC_PWM_CH4)
//RB9  is PORTB ChangeNotification (Digital Input)(RC_PWM_CH6) 
void pinChangeNotificationCallback(void);
//RB10 is IC3(RC_PWM_CH5)    
void setupIC3Handle();//DRV_HANDLE handle);
int inputCapture3PWMCalculator(void);

uint8_t getOperationalMode(void);
    
int getChannel1(void); 
int getChannel2(void);
int getChannel3(void);
int getChannel4(void);
int getChannel5(void);
int getChannel6(void);



#ifdef	__cplusplus
}
#endif

#endif	/* RCINPUTS_H */

