
#ifndef _HOLD_ROTATION_WEAPON_STOPPED_H    /* Guard against multiple inclusion */
#define _HOLD_ROTATION_WEAPON_STOPPED_H

#include <stdbool.h>


void setupHoldPosition(void);
void holdPosition(void);

#endif /* _HOLD_ROTATION_WEAPON_STOPPED_H */

