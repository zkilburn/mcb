#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-default.mk)" "nbproject/Makefile-local-default.mk"
include nbproject/Makefile-local-default.mk
endif
endif

# Environment
MKDIR=gnumkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=default
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=-mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c ../src/runWallsFlee.c ../src/enemyAssumedToBeInFront.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c ../src/driveAssistance.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c ../src/IMU.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c ../src/selectionSwitches.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c ../src/MicrosTimer.c ../src/TimerlessDelay.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c ../src/simulinkDebugger.c ../src/app.c ../src/main.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c ../src/LIDARSensors.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/spi/src/dynamic/drv_spi.c ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usart/src/dynamic/drv_usart.c ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c ../../../../framework/system/int/src/sys_int_pic32.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/_ext/1868040755/runWalls.o ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o ${OBJECTDIR}/_ext/1868040755/doTurn.o ${OBJECTDIR}/_ext/1360937237/driveAssistance.o ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o ${OBJECTDIR}/_ext/1868040755/macros.o ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o ${OBJECTDIR}/_ext/1104696802/findSensorOne.o ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o ${OBJECTDIR}/_ext/1104696802/rtGetInf.o ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o ${OBJECTDIR}/_ext/1360937237/IMU.o ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o ${OBJECTDIR}/_ext/1158113717/AngleFunction.o ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o ${OBJECTDIR}/_ext/1158113717/rtGetInf.o ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o ${OBJECTDIR}/_ext/1612208989/enemyDetection.o ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o ${OBJECTDIR}/_ext/1612208989/repmat.o ${OBJECTDIR}/_ext/1612208989/rtGetInf.o ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o ${OBJECTDIR}/_ext/1868040755/detectEnemy.o ${OBJECTDIR}/_ext/628060847/locationFunction.o ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o ${OBJECTDIR}/_ext/628060847/repmat.o ${OBJECTDIR}/_ext/628060847/rtGetInf.o ${OBJECTDIR}/_ext/628060847/rtGetNaN.o ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o ${OBJECTDIR}/_ext/628060847/solveLocation.o ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o ${OBJECTDIR}/_ext/875444448/drv_ic_static.o ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o ${OBJECTDIR}/_ext/977502197/drv_spi_api.o ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ${OBJECTDIR}/_ext/1868040755/MillisTimer.o ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o ${OBJECTDIR}/_ext/1868040755/uartHandler.o ${OBJECTDIR}/_ext/1868040755/printfUART.o ${OBJECTDIR}/_ext/1868040755/FastTransfer.o ${OBJECTDIR}/_ext/1868040755/communications.o ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1868040755/RCInputs.o ${OBJECTDIR}/_ext/1868040755/helperFunctions.o ${OBJECTDIR}/_ext/1868040755/motorControl.o ${OBJECTDIR}/_ext/1868040755/PID.o ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/568870469/drv_spi.o ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/260586732/drv_usart.o ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o
POSSIBLE_DEPFILES=${OBJECTDIR}/_ext/1868040755/runWalls.o.d ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d ${OBJECTDIR}/_ext/1868040755/doTurn.o.d ${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d ${OBJECTDIR}/_ext/1868040755/macros.o.d ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d ${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d ${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d ${OBJECTDIR}/_ext/1360937237/IMU.o.d ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d ${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d ${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d ${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d ${OBJECTDIR}/_ext/1612208989/repmat.o.d ${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d ${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d ${OBJECTDIR}/_ext/628060847/locationFunction.o.d ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d ${OBJECTDIR}/_ext/628060847/repmat.o.d ${OBJECTDIR}/_ext/628060847/rtGetInf.o.d ${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d ${OBJECTDIR}/_ext/628060847/solveLocation.o.d ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d ${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d ${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d ${OBJECTDIR}/_ext/1688732426/system_init.o.d ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d ${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d ${OBJECTDIR}/_ext/1868040755/uartHandler.o.d ${OBJECTDIR}/_ext/1868040755/printfUART.o.d ${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d ${OBJECTDIR}/_ext/1868040755/communications.o.d ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d ${OBJECTDIR}/_ext/1360937237/app.o.d ${OBJECTDIR}/_ext/1360937237/main.o.d ${OBJECTDIR}/_ext/1868040755/RCInputs.o.d ${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d ${OBJECTDIR}/_ext/1868040755/motorControl.o.d ${OBJECTDIR}/_ext/1868040755/PID.o.d ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d ${OBJECTDIR}/_ext/568870469/drv_spi.o.d ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d ${OBJECTDIR}/_ext/260586732/drv_usart.o.d ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d

# Object Files
OBJECTFILES=${OBJECTDIR}/_ext/1868040755/runWalls.o ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o ${OBJECTDIR}/_ext/1868040755/doTurn.o ${OBJECTDIR}/_ext/1360937237/driveAssistance.o ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o ${OBJECTDIR}/_ext/1868040755/macros.o ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o ${OBJECTDIR}/_ext/1104696802/findSensorOne.o ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o ${OBJECTDIR}/_ext/1104696802/rtGetInf.o ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o ${OBJECTDIR}/_ext/1360937237/IMU.o ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o ${OBJECTDIR}/_ext/1158113717/AngleFunction.o ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o ${OBJECTDIR}/_ext/1158113717/rtGetInf.o ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o ${OBJECTDIR}/_ext/1612208989/enemyDetection.o ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o ${OBJECTDIR}/_ext/1612208989/repmat.o ${OBJECTDIR}/_ext/1612208989/rtGetInf.o ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o ${OBJECTDIR}/_ext/1868040755/detectEnemy.o ${OBJECTDIR}/_ext/628060847/locationFunction.o ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o ${OBJECTDIR}/_ext/628060847/repmat.o ${OBJECTDIR}/_ext/628060847/rtGetInf.o ${OBJECTDIR}/_ext/628060847/rtGetNaN.o ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o ${OBJECTDIR}/_ext/628060847/solveLocation.o ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o ${OBJECTDIR}/_ext/875444448/drv_ic_static.o ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o ${OBJECTDIR}/_ext/977502197/drv_spi_api.o ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ${OBJECTDIR}/_ext/1688732426/system_init.o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ${OBJECTDIR}/_ext/1868040755/MillisTimer.o ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o ${OBJECTDIR}/_ext/1868040755/uartHandler.o ${OBJECTDIR}/_ext/1868040755/printfUART.o ${OBJECTDIR}/_ext/1868040755/FastTransfer.o ${OBJECTDIR}/_ext/1868040755/communications.o ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o ${OBJECTDIR}/_ext/1360937237/app.o ${OBJECTDIR}/_ext/1360937237/main.o ${OBJECTDIR}/_ext/1868040755/RCInputs.o ${OBJECTDIR}/_ext/1868040755/helperFunctions.o ${OBJECTDIR}/_ext/1868040755/motorControl.o ${OBJECTDIR}/_ext/1868040755/PID.o ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ${OBJECTDIR}/_ext/568870469/drv_spi.o ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ${OBJECTDIR}/_ext/260586732/drv_usart.o ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o

# Source Files
SOURCEFILES=C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c ../src/runWallsFlee.c ../src/enemyAssumedToBeInFront.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c ../src/driveAssistance.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c ../src/IMU.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c ../src/selectionSwitches.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S ../src/system_config/default/framework/system/ports/src/sys_ports_static.c ../src/system_config/default/system_init.c ../src/system_config/default/system_interrupt.c ../src/system_config/default/system_exceptions.c ../src/system_config/default/system_tasks.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c ../src/MicrosTimer.c ../src/TimerlessDelay.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c ../src/simulinkDebugger.c ../src/app.c ../src/main.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c ../src/LIDARSensors.c ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c ../../../../framework/driver/spi/src/dynamic/drv_spi.c ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c ../../../../framework/driver/usart/src/dynamic/drv_usart.c ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c ../../../../framework/system/int/src/sys_int_pic32.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-default.mk dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=32MZ2048EFG064
MP_LINKER_FILE_OPTION=
# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assembleWithPreprocess
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -D__DEBUG -DPICkit3PlatformTool=1 -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--defsym=__ICD2RAM=1,--defsym=__MPLAB_DEBUG=1,--gdwarf-2,--defsym=__DEBUG=1,--defsym=PICkit3PlatformTool=1
	
else
${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.ok ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.err 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d" "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d" -t $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC} $(MP_EXTRA_AS_PRE)  -c -mprocessor=$(MP_PROCESSOR_OPTION)  -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.d"  -o ${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_cache_pic32mz.S  -DXPRJ_default=$(CND_CONF)  -legacy-libc  -Wa,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_AS_POST),-MD="${OBJECTDIR}/_ext/340578644/sys_devcon_cache_pic32mz.o.asm.d",--gdwarf-2
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/_ext/1868040755/runWalls.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runWalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runWalls.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/runWalls.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/runWalls.o.d" -o ${OBJECTDIR}/_ext/1868040755/runWalls.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d" -o ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d" -o ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d" -o ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/runWallsFlee.o: ../src/runWallsFlee.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d" -o ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o ../src/runWallsFlee.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o: ../src/enemyAssumedToBeInFront.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d" -o ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o ../src/enemyAssumedToBeInFront.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/doTurn.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/doTurn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/doTurn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/doTurn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/doTurn.o.d" -o ${OBJECTDIR}/_ext/1868040755/doTurn.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/driveAssistance.o: ../src/driveAssistance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/driveAssistance.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d" -o ${OBJECTDIR}/_ext/1360937237/driveAssistance.o ../src/driveAssistance.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d" -o ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/macros.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/macros.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/macros.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/macros.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/macros.o.d" -o ${OBJECTDIR}/_ext/1868040755/macros.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/AutonomousMode.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d" -o ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1104696802/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/solveSensorOne.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d" -o ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/I2CFunctions.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d" -o ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/IMU.o: ../src/IMU.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/IMU.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/IMU.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/IMU.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/IMU.o.d" -o ${OBJECTDIR}/_ext/1360937237/IMU.o ../src/IMU.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d" -o ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/selectionSwitches.o: ../src/selectionSwitches.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d" -o ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o ../src/selectionSwitches.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1158113717/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/repmat.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/repmat.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/repmat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/repmat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/repmat.o.d" -o ${OBJECTDIR}/_ext/1612208989/repmat.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1612208989/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/detectEnemy.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/detectEnemy.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d" -o ${OBJECTDIR}/_ext/1868040755/detectEnemy.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/repmat.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/repmat.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/repmat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/repmat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/repmat.o.d" -o ${OBJECTDIR}/_ext/628060847/repmat.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/628060847/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/628060847/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/solveLocation.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/solveLocation.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/solveLocation.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/solveLocation.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/solveLocation.o.d" -o ${OBJECTDIR}/_ext/628060847/solveLocation.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o: ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/875444448" 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d" -o ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/875444448/drv_ic_static.o: ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/875444448" 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d" -o ${OBJECTDIR}/_ext/875444448/drv_ic_static.o ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_api.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_api.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_api.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/MillisTimer.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/MillisTimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d" -o ${OBJECTDIR}/_ext/1868040755/MillisTimer.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/MicrosTimer.o: ../src/MicrosTimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d" -o ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o ../src/MicrosTimer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o: ../src/TimerlessDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d" -o ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o ../src/TimerlessDelay.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/uartHandler.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/uartHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/uartHandler.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/uartHandler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/uartHandler.o.d" -o ${OBJECTDIR}/_ext/1868040755/uartHandler.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/printfUART.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/printfUART.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/printfUART.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/printfUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/printfUART.o.d" -o ${OBJECTDIR}/_ext/1868040755/printfUART.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/FastTransfer.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/FastTransfer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d" -o ${OBJECTDIR}/_ext/1868040755/FastTransfer.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/communications.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/communications.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/communications.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/communications.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/communications.o.d" -o ${OBJECTDIR}/_ext/1868040755/communications.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o: ../src/simulinkDebugger.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d" -o ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o ../src/simulinkDebugger.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/RCInputs.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/RCInputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/RCInputs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/RCInputs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/RCInputs.o.d" -o ${OBJECTDIR}/_ext/1868040755/RCInputs.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/helperFunctions.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/helperFunctions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d" -o ${OBJECTDIR}/_ext/1868040755/helperFunctions.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/motorControl.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/motorControl.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/motorControl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/motorControl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/motorControl.o.d" -o ${OBJECTDIR}/_ext/1868040755/motorControl.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/PID.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/PID.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/PID.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/PID.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/PID.o.d" -o ${OBJECTDIR}/_ext/1868040755/PID.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/LIDARSensors.o: ../src/LIDARSensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d" -o ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o ../src/LIDARSensors.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/568870469/drv_spi.o: ../../../../framework/driver/spi/src/dynamic/drv_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/568870469" 
	@${RM} ${OBJECTDIR}/_ext/568870469/drv_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/568870469/drv_spi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/568870469/drv_spi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/568870469/drv_spi.o.d" -o ${OBJECTDIR}/_ext/568870469/drv_spi.o ../../../../framework/driver/spi/src/dynamic/drv_spi.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o: ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/465164171" 
	@${RM} ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d" -o ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/260586732/drv_usart.o: ../../../../framework/driver/usart/src/dynamic/drv_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart.o ../../../../framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o: ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE) -g -D__DEBUG -DPICkit3PlatformTool=1  -fframe-base-loclist  -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
else
${OBJECTDIR}/_ext/1868040755/runWalls.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runWalls.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runWalls.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/runWalls.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/runWalls.o.d" -o ${OBJECTDIR}/_ext/1868040755/runWalls.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runWalls.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o.d" -o ${OBJECTDIR}/_ext/1868040755/runBackAndForthFindEnemyBoth.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/runBackAndForthFindEnemyBoth.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o.d" -o ${OBJECTDIR}/_ext/1868040755/holdRotationWeaponStopped.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/holdRotationWeaponStopped.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o.d" -o ${OBJECTDIR}/_ext/1868040755/spinWeaponUp.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/spinWeaponUp.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/runWallsFlee.o: ../src/runWallsFlee.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/runWallsFlee.o.d" -o ${OBJECTDIR}/_ext/1360937237/runWallsFlee.o ../src/runWallsFlee.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o: ../src/enemyAssumedToBeInFront.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o.d" -o ${OBJECTDIR}/_ext/1360937237/enemyAssumedToBeInFront.o ../src/enemyAssumedToBeInFront.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/doTurn.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/doTurn.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/doTurn.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/doTurn.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/doTurn.o.d" -o ${OBJECTDIR}/_ext/1868040755/doTurn.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/doTurn.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/driveAssistance.o: ../src/driveAssistance.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/driveAssistance.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/driveAssistance.o.d" -o ${OBJECTDIR}/_ext/1360937237/driveAssistance.o ../src/driveAssistance.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o.d" -o ${OBJECTDIR}/_ext/1868040755/demonstrationSetup.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/demonstrationSetup.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/macros.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/macros.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/macros.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/macros.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/macros.o.d" -o ${OBJECTDIR}/_ext/1868040755/macros.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/macros.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/AutonomousMode.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/AutonomousMode.o.d" -o ${OBJECTDIR}/_ext/1868040755/AutonomousMode.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/AutonomousMode.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o.d" -o ${OBJECTDIR}/_ext/1104696802/findSensorOne_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/findSensorOne_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1104696802/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1104696802/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1104696802/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1104696802/solveSensorOne.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1104696802" 
	@${RM} ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d 
	@${RM} ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1104696802/solveSensorOne.o.d" -o ${OBJECTDIR}/_ext/1104696802/solveSensorOne.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/NeuralNets/calculateSensorOne/solveSensorOne.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/I2CFunctions.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/I2CFunctions.o.d" -o ${OBJECTDIR}/_ext/1868040755/I2CFunctions.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/I2CFunctions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/IMU.o: ../src/IMU.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/IMU.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/IMU.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/IMU.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/IMU.o.d" -o ${OBJECTDIR}/_ext/1360937237/IMU.o ../src/IMU.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o.d" -o ${OBJECTDIR}/_ext/1868040755/sevenSegDigit.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/sevenSegDigit.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/selectionSwitches.o: ../src/selectionSwitches.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/selectionSwitches.o.d" -o ${OBJECTDIR}/_ext/1360937237/selectionSwitches.o ../src/selectionSwitches.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o.d" -o ${OBJECTDIR}/_ext/1158113717/AngleFunction_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/AngleFunction_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1158113717/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1158113717/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1158113717" 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1158113717/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/AngleFunction/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o.d" -o ${OBJECTDIR}/_ext/1612208989/enemyDetection_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/enemyDetection_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/repmat.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/repmat.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/repmat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/repmat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/repmat.o.d" -o ${OBJECTDIR}/_ext/1612208989/repmat.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/repmat.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/1612208989/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/1612208989/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1612208989" 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/1612208989/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/EnemyDetection/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/detectEnemy.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/detectEnemy.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/detectEnemy.o.d" -o ${OBJECTDIR}/_ext/1868040755/detectEnemy.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/detectEnemy.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction_initialize.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_initialize.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o.d" -o ${OBJECTDIR}/_ext/628060847/locationFunction_terminate.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/locationFunction_terminate.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/repmat.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/repmat.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/repmat.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/repmat.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/repmat.o.d" -o ${OBJECTDIR}/_ext/628060847/repmat.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/repmat.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rtGetInf.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetInf.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetInf.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rtGetInf.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rtGetInf.o.d" -o ${OBJECTDIR}/_ext/628060847/rtGetInf.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetInf.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rtGetNaN.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rtGetNaN.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rtGetNaN.o.d" -o ${OBJECTDIR}/_ext/628060847/rtGetNaN.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rtGetNaN.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/rt_nonfinite.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/rt_nonfinite.o.d" -o ${OBJECTDIR}/_ext/628060847/rt_nonfinite.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/rt_nonfinite.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/628060847/solveLocation.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/628060847" 
	@${RM} ${OBJECTDIR}/_ext/628060847/solveLocation.o.d 
	@${RM} ${OBJECTDIR}/_ext/628060847/solveLocation.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/628060847/solveLocation.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/628060847/solveLocation.o.d" -o ${OBJECTDIR}/_ext/628060847/solveLocation.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/locationFunction/solveLocation.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o: ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/875444448" 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o.d" -o ${OBJECTDIR}/_ext/875444448/drv_ic_mapping.o ../src/system_config/default/framework/driver/ic/src/drv_ic_mapping.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/875444448/drv_ic_static.o: ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/875444448" 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/875444448/drv_ic_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/875444448/drv_ic_static.o.d" -o ${OBJECTDIR}/_ext/875444448/drv_ic_static.o ../src/system_config/default/framework/driver/ic/src/drv_ic_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_tasks.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_api.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_api.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_api.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_api.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_api.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o: ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/977502197" 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o.d" -o ${OBJECTDIR}/_ext/977502197/drv_spi_master_ebm_tasks.o ../src/system_config/default/framework/driver/spi/dynamic/drv_spi_master_ebm_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o: ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/639803181" 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o.d" -o ${OBJECTDIR}/_ext/639803181/sys_clk_pic32mz.o ../src/system_config/default/framework/system/clk/src/sys_clk_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/340578644/sys_devcon.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon.o ../src/system_config/default/framework/system/devcon/src/sys_devcon.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o: ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/340578644" 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d 
	@${RM} ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o.d" -o ${OBJECTDIR}/_ext/340578644/sys_devcon_pic32mz.o ../src/system_config/default/framework/system/devcon/src/sys_devcon_pic32mz.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/822048611/sys_ports_static.o: ../src/system_config/default/framework/system/ports/src/sys_ports_static.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/822048611" 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d 
	@${RM} ${OBJECTDIR}/_ext/822048611/sys_ports_static.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/822048611/sys_ports_static.o.d" -o ${OBJECTDIR}/_ext/822048611/sys_ports_static.o ../src/system_config/default/framework/system/ports/src/sys_ports_static.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_init.o: ../src/system_config/default/system_init.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_init.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_init.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_init.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_init.o ../src/system_config/default/system_init.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_interrupt.o: ../src/system_config/default/system_interrupt.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_interrupt.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_interrupt.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_interrupt.o ../src/system_config/default/system_interrupt.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_exceptions.o: ../src/system_config/default/system_exceptions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_exceptions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_exceptions.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_exceptions.o ../src/system_config/default/system_exceptions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1688732426/system_tasks.o: ../src/system_config/default/system_tasks.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1688732426" 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o.d 
	@${RM} ${OBJECTDIR}/_ext/1688732426/system_tasks.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1688732426/system_tasks.o.d" -o ${OBJECTDIR}/_ext/1688732426/system_tasks.o ../src/system_config/default/system_tasks.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/MillisTimer.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/MillisTimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/MillisTimer.o.d" -o ${OBJECTDIR}/_ext/1868040755/MillisTimer.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/MillisTimer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/MicrosTimer.o: ../src/MicrosTimer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/MicrosTimer.o.d" -o ${OBJECTDIR}/_ext/1360937237/MicrosTimer.o ../src/MicrosTimer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o: ../src/TimerlessDelay.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o.d" -o ${OBJECTDIR}/_ext/1360937237/TimerlessDelay.o ../src/TimerlessDelay.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/uartHandler.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/uartHandler.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/uartHandler.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/uartHandler.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/uartHandler.o.d" -o ${OBJECTDIR}/_ext/1868040755/uartHandler.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/uartHandler.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/printfUART.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/printfUART.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/printfUART.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/printfUART.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/printfUART.o.d" -o ${OBJECTDIR}/_ext/1868040755/printfUART.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/printfUART.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/FastTransfer.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/FastTransfer.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/FastTransfer.o.d" -o ${OBJECTDIR}/_ext/1868040755/FastTransfer.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/FastTransfer.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/communications.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/communications.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/communications.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/communications.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/communications.o.d" -o ${OBJECTDIR}/_ext/1868040755/communications.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/communications.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o: ../src/simulinkDebugger.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o.d" -o ${OBJECTDIR}/_ext/1360937237/simulinkDebugger.o ../src/simulinkDebugger.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/app.o: ../src/app.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/app.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/app.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/app.o.d" -o ${OBJECTDIR}/_ext/1360937237/app.o ../src/app.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/main.o: ../src/main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/main.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/main.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/main.o.d" -o ${OBJECTDIR}/_ext/1360937237/main.o ../src/main.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/RCInputs.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/RCInputs.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/RCInputs.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/RCInputs.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/RCInputs.o.d" -o ${OBJECTDIR}/_ext/1868040755/RCInputs.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/RCInputs.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/helperFunctions.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/helperFunctions.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/helperFunctions.o.d" -o ${OBJECTDIR}/_ext/1868040755/helperFunctions.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/helperFunctions.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/motorControl.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/motorControl.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/motorControl.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/motorControl.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/motorControl.o.d" -o ${OBJECTDIR}/_ext/1868040755/motorControl.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/motorControl.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1868040755/PID.o: C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1868040755" 
	@${RM} ${OBJECTDIR}/_ext/1868040755/PID.o.d 
	@${RM} ${OBJECTDIR}/_ext/1868040755/PID.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1868040755/PID.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1868040755/PID.o.d" -o ${OBJECTDIR}/_ext/1868040755/PID.o C:/microchip/harmony/v2_05_01/apps/MCB/firmware/src/PID.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/1360937237/LIDARSensors.o: ../src/LIDARSensors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/1360937237" 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d 
	@${RM} ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/1360937237/LIDARSensors.o.d" -o ${OBJECTDIR}/_ext/1360937237/LIDARSensors.o ../src/LIDARSensors.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/280795049/drv_i2c.o: ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/280795049" 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o.d 
	@${RM} ${OBJECTDIR}/_ext/280795049/drv_i2c.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/280795049/drv_i2c.o.d" -o ${OBJECTDIR}/_ext/280795049/drv_i2c.o ../../../../framework/driver/i2c/src/dynamic/drv_i2c.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/568870469/drv_spi.o: ../../../../framework/driver/spi/src/dynamic/drv_spi.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/568870469" 
	@${RM} ${OBJECTDIR}/_ext/568870469/drv_spi.o.d 
	@${RM} ${OBJECTDIR}/_ext/568870469/drv_spi.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/568870469/drv_spi.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/568870469/drv_spi.o.d" -o ${OBJECTDIR}/_ext/568870469/drv_spi.o ../../../../framework/driver/spi/src/dynamic/drv_spi.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o: ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/465164171" 
	@${RM} ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d 
	@${RM} ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o.d" -o ${OBJECTDIR}/_ext/465164171/drv_spi_sys_queue_fifo.o ../../../../framework/driver/spi/src/drv_spi_sys_queue_fifo.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/185269848/drv_tmr.o: ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/185269848" 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o.d 
	@${RM} ${OBJECTDIR}/_ext/185269848/drv_tmr.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/185269848/drv_tmr.o.d" -o ${OBJECTDIR}/_ext/185269848/drv_tmr.o ../../../../framework/driver/tmr/src/dynamic/drv_tmr.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/260586732/drv_usart.o: ../../../../framework/driver/usart/src/dynamic/drv_usart.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart.o ../../../../framework/driver/usart/src/dynamic/drv_usart.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o: ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/260586732" 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d 
	@${RM} ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o.d" -o ${OBJECTDIR}/_ext/260586732/drv_usart_byte_model.o ../../../../framework/driver/usart/src/dynamic/drv_usart_byte_model.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
${OBJECTDIR}/_ext/122796885/sys_int_pic32.o: ../../../../framework/system/int/src/sys_int_pic32.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/_ext/122796885" 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d 
	@${RM} ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o 
	@${FIXDEPS} "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" $(SILENT) -rsi ${MP_CC_DIR}../  -c ${MP_CC}  $(MP_EXTRA_CC_PRE)  -g -x c -c -mprocessor=$(MP_PROCESSOR_OPTION)  -ffunction-sections -O1 -I"../src" -I"../src/system_config/default" -I"../src/default" -I"../../../../framework" -I"../src/system_config/default/framework" -Wall -MMD -MF "${OBJECTDIR}/_ext/122796885/sys_int_pic32.o.d" -o ${OBJECTDIR}/_ext/122796885/sys_int_pic32.o ../../../../framework/system/int/src/sys_int_pic32.c    -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD) 
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: compileCPP
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ2048EFG064_peripherals.a  
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) -g -mdebugger -DPICkit3PlatformTool=1 -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MZ2048EFG064_peripherals.a      -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)   -mreserve=data@0x0:0x37F   -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=__MPLAB_DEBUG=1,--defsym=__DEBUG=1,-D=__DEBUG_D,--defsym=PICkit3PlatformTool=1,--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk  ../../../../bin/framework/peripheral/PIC32MZ2048EFG064_peripherals.a 
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE)  -mprocessor=$(MP_PROCESSOR_OPTION)  -o dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} ${OBJECTFILES_QUOTED_IF_SPACED}    ..\..\..\..\bin\framework\peripheral\PIC32MZ2048EFG064_peripherals.a      -DXPRJ_default=$(CND_CONF)  -legacy-libc  $(COMPARISON_BUILD)  -Wl,--defsym=__MPLAB_BUILD=1$(MP_EXTRA_LD_POST)$(MP_LINKER_FILE_OPTION),--defsym=_min_heap_size=0,--gc-sections,--no-code-in-dinit,--no-dinit-in-serial-mem,-Map="${DISTDIR}/${PROJECTNAME}.${IMAGE_TYPE}.map",--memorysummary,dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml
	${MP_CC_DIR}\\xc32-bin2hex dist/${CND_CONF}/${IMAGE_TYPE}/MCB.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX} 
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/default
	${RM} -r dist/default

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
